import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private tr: ToastrService) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (sessionStorage.getItem('token')) {
        return true;
      } else {
        return true;
        // this.tr.warning('', 'You have to signin');
            //  this.router.navigateByUrl('/');
            //     sessionStorage.clear();
                // setTimeout(() => {
                //     window.location.reload();
                // }, 100);
      }

  }
}
