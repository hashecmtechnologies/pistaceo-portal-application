import { Component, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { WorkService } from '../../../service/work.service';
import { UserService } from '../../../service/user.service';
import { OrgWorkItms } from '../../../models/orgworkitems.model';
import { AdministrationService } from '../../../service/Administration.service';
import { SubLevelOrg } from '../../../models/sublevelorg.model';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { SchemaService } from '../../../service/schema.service';
import { AdminLayoutComponent } from '../layout/admin-layout/admin-layout.component';
import { ContentService } from '../../../service/content.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, AfterViewInit {
  public services = [];
  constructor(private breadcrumbService: BreadcrumbService, private ws: WorkService,
    private us: UserService, private changeDetectorRef: ChangeDetectorRef, private router: Router,
    private translate: TranslateService, private as: AdministrationService,
    private ss: SchemaService, private al: AdminLayoutComponent, private cs: ContentService) {
    this.breadcrumbService.setItems([
      { label: this.translate.instant('New Ticket') }
    ]);
  }

  ngOnInit() {
    this.ss.getWorkTypes().subscribe(data => { this.dynamicItems(data); });
    setTimeout(() => {
      this.changeDetectorRef.detectChanges();
      this.al.selected = 'home';
    }, 100);
  }

  dynamicItems(data) {
    this.services = [];
    if (data._body) {
      this.services = JSON.parse(data._body);
      console.log(this.services);
      this.services.forEach(element => {
        if (element.name === 'Ticket') {
          this.openCreate(element);
        }
      });
    }
  }
  ngAfterViewInit() {
  }
  openCreate(service) {
    this.router.navigate(['/create'], { queryParams: { 'id': service.initActivityType, 'typeId': service.id, 'multtiFromSupport': service.multiFormSupport, 'subjectProps': service.subjectProps } });

  }

}
