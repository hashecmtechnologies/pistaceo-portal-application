import { Component, OnInit, ChangeDetectorRef, ViewChild, ElementRef, OnChanges, AfterViewInit, Input } from '@angular/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { WorkService } from '../../../service/work.service';
import { DocumentService } from '../../../service/document.service';
import { SchemaService } from '../../../service/schema.service';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { saveAs } from 'file-saver';
import { Location } from '@angular/common';
import { Editor } from 'primeng/primeng';
import { ToastrService } from 'ngx-toastr';
import { IntegrationService } from '../../../service/integration.service';
import { ContentService } from '../../../service/content.service';
import { DomSanitizer } from '@angular/platform-browser';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { BreadcrumbComponent } from '../layout/breadcrumb/breadcrumb.component';

@Component(
    { selector: 'app-activity', templateUrl: './activity.component.html', styleUrls: ['./activity.component.css'] }
)
export class ActivityComponent implements OnInit, OnChanges {

    @Input() readworkitem;
    @Input() typeId;
    @ViewChild('quillDisable')
    editor: Editor;
    @ViewChild('checkInFileUploder') checkInFileUploder: ElementRef;
    // @ViewChild('ng2FileUplode') ng2FileUplode: ElementRef;
    public notify = false;
    public assignedTo;
    public formDocid;
    public routeToItem;
    public routeToItemRoutes;
    public showassignedTo;
    public showRoleMembers;
    public activityform: FormGroup;
    public activityinfo;
    public activityhistory = [];
    public docId;
    public recallButton;
    public showDocuments;
    public activitytype;
    public showToDo = false;
    public showDocOrForms;
    public showRichtextComments = false;
    public duplicateActivity;
    public multipleSelectDropdown;
    public hideRoute;
    public multiRoleSelect = false;
    public tempAttachments = [];
    public tempelate;
    public imageToShow = '';
    public previewImage;
    public previewOpacity = false;
    public totalPages;
    public curPage;
    public invalidFormat;
    public routeTodata = [];
    public responsesDropDown = [];
    public tempFileobject = new FileUploader({ url: '' });
    public globelFileUploder = new FileUploader({});
    public updatePrimaryDocument = new FileUploader({});
    public showDocumentsTick = false;
    public formData;
    public websocketNotOpen = false;
    public showUpdateScanned;
    public formObjectSubmit;
    public formObject;
    public createformtabshow = false;
    public createformtitle: any;
    public activeIndexNumber = 0;
    public reloadDocComp = false;
    public readonlyformtabshow = false;
    public generic = false;
    public documentsSelected = [];
    public listOfDcumentAnnotation = [];
    public showRecall = false;
    public quillSettings = {
        theme: 'bubble'
    };
    public pdfUrl: any;
    public printPdf = false;
    public documentFormTemplate = false;
    public tempDoc;
    public responseSelect;
    public tempelateRender: FormGroup;
    public templateName = '';
    public responseSelectProps;
    public printPreviewTab = false;
    public annotationCount = 0;
    public recall;
    public documentId;
    public docPropsShow = false;
    public recentDocumentList = [];
    public systemDate;
    public documentClass;
    public documentClassProp;
    public documentAdd: FormGroup;
    public docClassSymName;
    public docClassName;
    public attachmentfileName = null;
    public attachmentfile;
    public selectedClassItem;
    public docProperty;
    public selectedComments;
    public editSubject = false;
    public docPropertyType;
    public formDocumentId;
    public activityId;
    public changePage;
    public annotationPreviewTab = false;
    public annotationType;
    public AnnotationCurrentPage;
    public annotationDocId;
    public annotationDocumentType;
    public timestamp;
    public ddmmyy = false;
    public documnetAnnoationType = 'docs';
    public showBackButton = false;
    public documentProperty;
    public docProps = [];
    public propertyTabShow = false;
    public datatableId: any;
    public activityScreen = 'activityScreen';
    public onlyTabAnnotations = false;
    public selectedLanguage: any;
    public typeFormat: any;
    public isReqFormDocument = false;
    public pageNor = 1;
    public attachmentsShow = [];
    constructor(
        private breadcrumbService: BreadcrumbService,
        public translate: TranslateService,
        private ss: SchemaService,
        public us: UserService,
        private fb: FormBuilder,
        private ws: WorkService,
        private cs: ContentService,
        private tr: ToastrService,
        private ds: DocumentService,
        private changeDetectorRef: ChangeDetectorRef,
        private _sanitizer: DomSanitizer,
        private location: Location,
        private ts: ToastrService,
        private integrationservice: IntegrationService,
        private route: ActivatedRoute,
        private router: Router,
        private toastr: ToastrService,
        public changeDetectRef: ChangeDetectorRef,
        public ngxSmartModalService: NgxSmartModalService,
        private spinnerService: Ng4LoadingSpinnerService
    ) {
        // this.breadcrumbService.isBack = true;
        if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
            this.ddmmyy = true;
        } else {
            this.ddmmyy = false;
        }
        this.ddmmyy = true;
        this.documentAdd = this.fb.group({});
        if (window.location.href.includes('draft')) {
            this.breadcrumbService.setItems([
                {
                    label: this.translate.instant('Draft')
                }
            ]);
            this.docPropertyType = 'draft';
        } else if (window.location.href.includes('inbox')) {
            this.breadcrumbService.setItems([
                {
                    label: this.translate.instant('Inbox')
                }
            ]);
            this.docPropertyType = 'inbox';
        } else if (window.location.href.includes('sent')) {
            this.breadcrumbService.setItems([
                {
                    label: this.translate.instant('Sent')
                }
            ]);
            this.docPropertyType = 'sent';
        } else if (window.location.href.includes('archive')) {
            this.breadcrumbService.setItems([
                {
                    label: this.translate.instant('Archive')
                }
            ]);
            this.docPropertyType = 'archive';
        }
        this.activitytype = [];
        this.activityinfo = [];
        this.activityform = this.fb.group({
            refno: [null],
            createdon: [null],
            instruction: [null],
            subject: [null],
            comments: [null],
            quillcomments: [null],
            activity: [null],
            role: [null],
            response: [null],
            responseDropdown: [null],
            roleDropdown: [null],
            activityDropdown: [null],
            activityDropdownId: [null],
            responseDropdownId: [null],
            roleDropdownId: [null],
            deadline: ['']
        });
    }

    ngOnInit() {
        this.selectedLanguage = localStorage.getItem('Default Language');
        this.tempAttachments = [];
        // if (!window.location.href.includes('search')) {
        this.showBackButton = true;
        this.spinnerService.show();
        this.route.queryParams.subscribe(params => {
            this.typeId = params['workId'];
            this.formDocid = params['formdocid'];
            this.readworkitem = params['readworkitem'];
            this.routeToItem = [];
            this.routeToItemRoutes = [];
            this.showassignedTo = params['workAssignedToName'];
            this.assignedTo = params['workAssignedTo'];
            this.showRoleMembers = '';
            this.recall = params['recall'];
            this.typeFormat = params['typeFormat'];
            if (this.readworkitem === 'readonly') {
                this.showRecall = true;

            }
            if (this.recall !== undefined) {
                this.showRecall = false;
            } else {
                this.showRecall = true;
            }
            // this.showDocumentsTick = false;
        });

        if (this.readworkitem === 'readonly') {
            this.showRecall = true;
            this.activityform.controls.subject.disable();
            if (this.us.getCurrentUser().isAdmin === 'Y' && (this.breadcrumbService.getItem() === 'Search' || this.breadcrumbService.getItem() === 'Advanced Search')) {
                this.showRecall = false;
                this.notify = true;
            } else {
                this.notify = false;
            }
        }
        this.tempelateRender = this.fb.group({});

        this.ws.getActivityForAction(this.typeId).subscribe(data => this.getactivityinfo(data), error => this.spinnerService.hide());
        this.ws.getAcitvityHistory(this.typeId).subscribe(data => this.getHistory(data), error => this.spinnerService.hide());
        // }
    }

    ngOnChanges() {
        // if (this.readworkitem === 'readonly') {
        this.spinnerService.show();

        if (this.readworkitem === 'readonly') {
            this.showRecall = true;
            this.activityform.controls.subject.disable();
            if (this.us.getCurrentUser().isAdmin === 'Y' && (this.breadcrumbService.getItem() === 'Search' || this.breadcrumbService.getItem() === 'Advanced Search')) {
                this.showRecall = false;
                this.notify = true;
            } else {
                this.notify = false;
            }
        }
        if (this.recall !== undefined) {
            this.showRecall = false;
        }
        this.tempelateRender = this.fb.group({});

        this.ws.getActivityForAction(this.typeId).subscribe(data => this.getactivityinfo(data), error => this.spinnerService.hide());
        this.ws.getAcitvityHistory(this.typeId).subscribe(data => this.getHistory(data), error => this.spinnerService.hide());
        // }
    }

    annotationsCount(data) {
        const resData = JSON.parse(data._body);
        this.annotationCount = resData.length;
    }

    getRoleMembers(data) {
        const value = JSON.parse(data._body);
        if (value.length > 0) {
            for (let i = 0; i < value.length; i++) {
                if (i === 0) {
                    this.showRoleMembers = value[i].fulName;
                } else {
                    this.showRoleMembers = this.showRoleMembers + ' , ' + value[i].fulName;
                }
            }
        }

    }

    getHistory(data) {
        this.activityhistory = JSON.parse(data._body);
    }

    getactivityinfo(data) {
        if (data._body !== undefined) {
            this.activityinfo = JSON.parse(data._body);
            this.tempAttachments = [];
            this.showassignedTo = this.activityinfo.assignedToName;
            this.attachmentsShow = [];
            this.us.getRoleMembers(this.activityinfo.assignedTo).subscribe(dataRes => this.getRoleMembers(dataRes));
            if (this.activityinfo.primaryDoc !== undefined) {
                this.docId = this.activityinfo.primaryDoc;
                this.pageNor = 1;
                const date = new Date();
                this.timestamp = date.getSeconds();
            }
            if (this.activityinfo.attachments && this.activityinfo.attachments.length !== 0) {
                for (let index = 0; index < this.activityinfo.attachments.length; index++) {
                    // if (this.activityinfo.attachments[index].docType.type !== 'FORM') {
                    this
                        .tempAttachments
                        .push(this.activityinfo.attachments[index]);
                    // }
                    if (index === 0) {
                        const objectValue = { name: this.activityinfo.attachments[index].docType.name, arrayValue: [] };
                        this.attachmentsShow.push(objectValue);
                        objectValue.arrayValue.push(this.activityinfo.attachments[index]);
                    } else {
                        let flag = 0;
                        let posI = 0;
                        for (let i = 0; i < this.attachmentsShow.length; i++) {
                            if (this.activityinfo.attachments[index].docType.name === this.attachmentsShow[i].name) {
                                flag = 1;
                                posI = i;
                                break;
                            } else {
                                flag = 0;
                            }
                        }
                        if (flag === 1) {
                            this.attachmentsShow[posI].arrayValue.push(this.activityinfo.attachments[index]);

                        } else {
                            const objectValue = { name: this.activityinfo.attachments[index].docType.name, arrayValue: [] };
                            this.attachmentsShow.push(objectValue);
                            objectValue.arrayValue.push(this.activityinfo.attachments[index]);
                        }
                    }
                }
                this.showDocuments = true;
                // for (let index = 0; index < this.activityinfo.attachments.length; index++) {
                //     if (this.converToUppercase(this.activityinfo.attachments[index].docType.type) === 'FORM') {
                //         // this.showDocumentsTick = true;
                //     }
                // }
            } else {
                this.showDocuments = false;
            }
            if (this.activityinfo.status === 'READ' || this.activityinfo.status === 'READ') {
                this.recallButton = true;
            }

            this.activityform.patchValue({ comments: this.activityinfo.comments });
            this.cs.getAnnotations(this.docId).subscribe(datares => this.annotationsCount(datares), error => this.spinnerService.hide());
            this.getworktypefromid(this.activityinfo.type);

            // this
            //     .ss
            //     .getWorkTypeFromID(this.activityinfo.typeId)
            //     .subscribe(resdata => this.getworktypefromid(resdata), error => this.spinnerService.hide());
        }
    }

    getworktypefromid(resdata) {
        this.spinnerService.hide();
        this.activitytype = resdata;
        this.duplicateActivity = resdata;
        this.responsesDropDown = [];
        if (this.activitytype.responses !== undefined) {
            for (let index = 0; index < this.activitytype.responses.length; index++) {
                if (this.responsesDropDown.length === 0) {
                    this.responsesDropDown.push({
                        label: this.activitytype.responses[index].name,
                        value: this.activitytype.responses[index].name
                    });
                } else {
                    let flag = 0;
                    for (let d = 0; d < this.responsesDropDown.length; d++) {
                        if (this.responsesDropDown[d].label !== this.activitytype.responses[index].name) {
                            flag = 1;
                        } else {
                            flag = 0;
                            break;
                        }
                    }
                    if (flag === 1) {
                        this.responsesDropDown.push({
                            label: this.activitytype.responses[index].name,
                            value: this.activitytype.responses[index].name
                        });
                    }
                }
            }
        }
        if (this.activitytype.docTypes !== undefined) {
            this.formDocumentRequired();
        } else {
            // this.showDocumentsTick = true;
        }
        // if (this.activitytype.ui.id === 0) {
        //     this.editSubject = false;
        // } else {
        //     this.editSubject = true;
        // }
        this.editSubject = false;
        this.activityform.patchValue(
            {
                refno: this.activityinfo.refNo,
                createdon: this.activityinfo.createdOn,
                instruction: this.activityinfo.instructions,
                subject: this.activityinfo.subject,
                deadline: this.activityinfo.deadline

            }
        );
        if (this.readworkitem !== 'readworkitem') {
            if (this.activitytype.docTypes !== undefined) {
                if (this.activitytype.docTypes.length > 0) {
                    this.showToDo = true;
                } else {
                    this.showToDo = false;
                }

                if (this.activitytype.docTypes.id) {
                    this.showDocOrForms = true;
                } else {
                    this.showDocOrForms = false;
                }
            }
        }

        if (this.readworkitem === 'readworkitem') {
            this.showRichtextComments = false;
            if (this.activitytype.docTypes !== undefined) {
                for (let index = 0; index < this.activitytype.docTypes.length; index++) {
                    if (this.activitytype.docTypes[index].type === 'DOCUMENT') {
                        // let fileUp = this.activitytype.docTypes[index].id + 'file';
                        this.activitytype.docTypes[index].file = new FileUploader({});
                    }
                }
            }
            // this.activityform.patchValue({   responseDropdown:
            // this.activitytype.responses['0'].name,   responseDropdownId:
            // this.activitytype.responses['0'].id,   activityDropdown:
            // this.activitytype.responses['0'].routeToName,   activityDropdownId:
            // this.activitytype.responses['0'].routeToId,   comments :
            // this.activitytype.responses['0'].comment });
            let comments = '';
            if (this.activitytype.responses.length > 0 && this.activitytype.responses['0'].comment) {
                comments = this.activitytype.responses['0'].comment;
            } else {
                comments = '';
            }
            this
                .activityform
                .patchValue({
                    comments: comments
                });
            if (this.activitytype.responses.length > 0 && (this.activitytype.responses['0'].name === 'Archive' || this.activitytype.responses['0'].name === 'Compose Reply')) {
                this.hideRoute = true;
                if (this.activitytype.responses['0'].name === 'Compose Reply') {
                    this.showRichtextComments = true;
                    this.activityform.patchValue({
                        responseDropdown: this.activitytype.responses['0'].name,
                        responseDropdownId: this.activitytype.responses['0'].id,
                        activityDropdown: this.activitytype.responses['0'].routeToName,
                        activityDropdownId: this.activitytype.responses['0'].routeToId
                    });
                    this.activityform.patchValue({
                        comments: this.activitytype.responses['0'].comment
                    });
                } else {
                    this.showRichtextComments = false;
                    this.activityform.patchValue({
                        responseDropdown: this.activitytype.responses['0'].name,
                        responseDropdownId: this.activitytype.responses['0'].id,
                        activityDropdown: this.activitytype.responses['0'].routeToName,
                        activityDropdownId: this.activitytype.responses['0'].routeToId
                    });
                    this.activityform.patchValue({
                        comments: this.activitytype.responses['0'].comment
                    });
                }
            } else {
                if (this.activitytype.responses.length > 0) {
                    this.hideRoute = false;
                    this.activityform.patchValue({
                        responseDropdown: this.activitytype.responses['0'].name,
                        responseDropdownId: this.activitytype.responses['0'].id,
                        activityDropdown: this.activitytype.responses['0'].routeToName,
                        activityDropdownId: this.activitytype.responses['0'].routeToId
                    });
                    this.activityform.patchValue({
                        comments: comments
                    });
                }
            }
            this.responseSelect = this.activitytype.responses['0'];

            if (this.activitytype.responses.length > 0 && this.activitytype.responses['0'].roleId !== -5) {
                this.multiRoleSelect = false;
                this.routeToItem = [];
                this.routeToItemRoutes = [];
                for (let b = 0; b < this.activitytype.responses.length; b++) {
                    if (this.activitytype.responses['0'].id === this.activitytype.responses[b].id) {
                        let rout;
                        let routes;
                        if (this.activitytype.responses[b].roleName) {
                            rout = {
                                name: this.activitytype.responses[b].roleName,
                                value: this.activitytype.responses[b].roleId
                            };
                            routes = {
                                name: this.activitytype.responses[b].roleName,
                                value: this.activitytype.responses[b].roleId,
                                routeToId: this.activitytype.responses[b].routeToId,
                                routeToName: this.activitytype.responses[b].routeToName
                            };
                        } else {
                            rout = {
                                name: 'None'
                            };
                            routes = {
                                name: 'None'
                            };

                        }
                        this.routeToItem.push(rout);
                        this.routeToItemRoutes.push(routes);
                    }
                }
                this.activityform.controls.roleDropdown.disable();
            } else {
                this.multiRoleSelect = false;
                this.activityform.controls.roleDropdown.enable();
                this.routeToItem = [];
                this.routeToItemRoutes = [];
            }
            if (this.activitytype.responses.length > 0 && this.activitytype.responses['0'].roleId === -6) {
                this.multiRoleSelect = false;
                this.activityform.controls.roleDropdown.enable();
                this.routeToItem = [];
                this.routeToItemRoutes = [];
                this.responseSelect = this.activitytype.responses['0'];
            }
        }
        if (this.editSubject) {
            this.activityform.controls.subject.enable();
        }
        this.activityform.patchValue({ deadline: this.activityinfo.deadline });
        this.spinnerService.hide();
    }

    activiveFormSubmit(nextscreen) {
        this.spinnerService.show();
        let comments;
        comments = this.activityform.controls.comments.value;
        if ((this.isReqFormDocument === true && this.showDocumentsTick !== true) && this.responseSelect.validate === 1) {  // NEED TO MAKE CHANGE HERE
            this.ts.error('Please fill the form');
            this.spinnerService.hide();
            this.annotationPreviewTab = false;
            this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = 0;
            return;
        } else {
            // if ((this.responseSelect.roleId === -5 || this.responseSelect.roleId === -6) && (this.activityform.controls.deadline.value === '' || !this.activityform.controls.deadline.value)) {
            //     this.tr.error('', 'Deadline is required');
            //     this.annotationPreviewTab = false;
            //     this.changeDetectorRef.detectChanges();
            //     this.spinnerService.hide();
            //     this.activeIndexNumber = 0;
            //     return;
            // }
            if ((comments === '' || comments === undefined || comments === null)) {
                this.ts.error('Comment is required');
                this.annotationPreviewTab = false;
                this.changeDetectorRef.detectChanges();
                this.spinnerService.hide();
                this.activeIndexNumber = 0;
                return;
            } else {
                let flag = 0;
                let count = 0;
                for (let index = 0; index < this.activitytype.docTypes.length; index++) {
                    if (this.activitytype.docTypes[index].type === 'DOCUMENT') {
                        if (this.activitytype.docTypes[index].req === 1) {
                            count++;
                            for (let pos = 0; pos < this.tempAttachments.length; pos++) {
                                if (this.activitytype.docTypes[index].name === this.tempAttachments[pos].docType.name) {
                                    flag = 1;
                                    break;
                                } else {
                                    flag = 0;
                                }
                            }
                        }
                    }
                }
                if (count === 0) {
                    flag = 1;
                }
                if (flag === 0 && this.responseSelect.validate === 1) {
                    this.ts.error('', 'Need to complete all the mandatory to do items');
                    this.spinnerService.hide();
                    this.annotationPreviewTab = false;
                    this.changeDetectorRef.detectChanges();
                    this.activeIndexNumber = 0;
                    return;
                } else {
                    const activitySubmit = {
                        id: this.typeId,
                        typeId: this.activitytype.id,
                        modifiedBy: this.us.getCurrentUser().EmpNo,
                        modifierRole: this.us.getCurrentUser().roles['0'].id,
                        finishedBy: this.us.getCurrentUser().EmpNo,
                        finishedByName: this.us.getCurrentUser().fulName,
                        finisherRole: this.us.getCurrentUser().roles['0'].id,
                        comments: comments,
                        deadline: this.activityform.controls.deadline.value,
                        attachments: this.tempAttachments,
                        responseId: this.activityform.controls.responseDropdownId.value,
                        routes: [],
                        respPurpose: this.responseSelect.purpose
                    };
                    // if (this.responseSelect.finishForm !== undefined &&
                    // this.responseSelect.finishForm !== null) {   activitySubmit.respPurpose =
                    // this.responseSelect.purpose; }
                    if (this.multiRoleSelect) {
                        // this.spinnerService.hide();
                        if (this.routeToItem.name !== undefined) {
                            const routes = {
                                activityType: this.activityform.controls.activityDropdownId.value,
                                activityTypeName: this.activityform.controls.activityDropdown.value,
                                roleId: this.routeToItem.value,
                                roleName: this.routeToItem.name
                            };
                            activitySubmit.routes.push(routes);
                            if (this.responseSelect.finishForm !== undefined && this.responseSelect.finishForm !== null) {
                                this.spinnerService.hide();
                                this.templateName = this.responseSelect.finishForm.name;
                                this.responseSelectProps = this.responseSelect.finishForm.properties;
                                this.tempelateRender = this.fb.group({});
                                for (let index = 0; index < this.responseSelectProps.length; index++) {
                                    if (this.responseSelectProps[index].req === 'TRUE') {
                                        const control: FormControl = new FormControl(null, Validators.required);
                                        this.tempelateRender.addControl(this.responseSelectProps[index].name, control);
                                    } else {
                                        const control: FormControl = new FormControl(null);
                                        this.tempelateRender.addControl(this.responseSelectProps[index].name, control);
                                    }
                                }
                                for (let index = 0; index < this.responseSelectProps.length; index++) {
                                    if (this.responseSelectProps[index].uitype.uitype === 'TEXT') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'LOOKUP') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].uitype.lookups[0].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'DATE') {
                                        this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].lookups[0].value);
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'NUMBER') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'TEXTAREA') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'CHECKBOX') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else { }
                                }
                                this.spinnerService.hide();
                                this.ngxSmartModalService.getModal('tempelateRenderModel').open();
                                this.spinnerService.hide();
                            } else {
                                if (this.editSubject) {
                                    this.ws.updateSubject(this.activityinfo.id, encodeURIComponent(this.activityform.controls.subject.value)).subscribe();
                                }
                                this.activityform.disable();

                                this.ws.saveActivity(activitySubmit).subscribe(data => this.ws.finishActivity(activitySubmit).subscribe(res => {
                                    this.finishActivity(res, nextscreen);
                                }, errr => {
                                    this.activityform.enable(); this.spinnerService.hide();
                                }),
                                    err => {
                                        this.activityform.enable();
                                    }
                                );
                            }
                        } else {
                            this.ts.error('', 'Please select a role');
                            this.spinnerService.hide();
                            this.annotationPreviewTab = false;
                            this.changeDetectorRef.detectChanges();
                            this.activeIndexNumber = 0;
                            return;
                        }
                    } else {
                        if (this.routeToItem.length === 0) {
                            this.ts.error('', 'Please select a role');
                            this.spinnerService.hide();
                            this.annotationPreviewTab = false;
                            this.changeDetectorRef.detectChanges();
                            this.activeIndexNumber = 0;
                            return;
                        } else {
                            if (this.responseSelect.finishForm !== null && this.responseSelect.finishForm !== undefined) {
                                this.templateName = this.responseSelect.finishForm.label;
                                this.responseSelectProps = this.responseSelect.finishForm.properties;
                                this.tempelateRender = this.fb.group({});
                                for (let index = 0; index < this.responseSelectProps.length; index++) {
                                    if (this.responseSelectProps[index].req === 'TRUE') {
                                        const control: FormControl = new FormControl(null, Validators.required);
                                        this
                                            .tempelateRender
                                            .addControl(this.responseSelectProps[index].name, control);
                                    } else {
                                        const control: FormControl = new FormControl(null);
                                        this
                                            .tempelateRender
                                            .addControl(this.responseSelectProps[index].name, control);
                                    }
                                }
                                for (let index = 0; index < this.responseSelectProps.length; index++) {
                                    if (this.responseSelectProps[index].uitype.uitype === 'TEXT') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'LOOKUP') {
                                        this.responseSelectProps[index].lookupOptions = [];
                                        for (const options of this.responseSelectProps[index].uitype.lookups) {
                                            const lookupValue = {
                                                label: options.name,
                                                value: options.value
                                            };
                                            this.responseSelectProps[index].lookupOptions.push(lookupValue);
                                        }
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[
                                                this.responseSelectProps[index].name]
                                                .patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[
                                                this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].uitype.lookups[0].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'DATE') {
                                        this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].lookups[0].value);
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'NUMBER') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[
                                                this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'TEXTAREA') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else if (this.responseSelectProps[index].uitype.uitype === 'CHECKBOX') {
                                        if (this.responseSelectProps[index].value.length !== 0) {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value[0].value);
                                        } else {
                                            this.tempelateRender.controls[this.responseSelectProps[index].name].patchValue(this.responseSelectProps[index].value);
                                        }
                                    } else { }
                                }
                                this.ngxSmartModalService.getModal('tempelateRenderModel').open();
                                this.spinnerService.hide();
                            } else {
                                this.spinnerService.show();
                                const activitySubmit = {
                                    id: this.typeId,
                                    typeId: this.activitytype.id,
                                    modifiedBy: this.us.getCurrentUser().EmpNo,
                                    modifierRole: this.us.getCurrentUser().roles['0'].id,
                                    finishedBy: this.us.getCurrentUser().EmpNo,
                                    finishedByName: this.us.getCurrentUser().fulName,
                                    finisherRole: this.us.getCurrentUser().roles['0'].id,
                                    comments: comments,
                                    deadline: this.activityform.controls.deadline.value,
                                    attachments: this.tempAttachments,
                                    responseId: this.activityform.controls.responseDropdownId.value,
                                    routes: [],

                                    respPurpose: this.responseSelect.purpose
                                };
                                for (let roue = 0; roue < this.routeToItem.length; roue++) {
                                    if (this.routeToItemRoutes.length > 0) {
                                        const routes = {
                                            activityType: this.routeToItemRoutes[roue].routeToId,
                                            activityTypeName: this.routeToItemRoutes[roue].routeToName,
                                            roleId: this.routeToItem[roue].value,
                                            roleName: this.routeToItem[roue].name
                                        };
                                        activitySubmit.routes.push(routes);
                                    } else {
                                        const routes = {
                                            activityType: this.activityform.controls.activityDropdownId.value,
                                            activityTypeName: this.activityform.controls.activityDropdown.value,
                                            roleId: this.routeToItem[roue].value,
                                            roleName: this.routeToItem[roue].name
                                        };

                                        activitySubmit.routes.push(routes);
                                    }
                                }

                                if (this.editSubject) {
                                    this.ws.updateSubject(this.activityinfo.id, encodeURIComponent(this.activityform.controls.subject.value)).subscribe();
                                }
                                this.activityform.disable();
                                this.ws.saveActivity(activitySubmit).subscribe(data => this.ws.finishActivity(activitySubmit).subscribe(res => this.finishActivity(res, nextscreen), err => {
                                    this.activityform.enable();
                                    this.spinnerService.hide();
                                }),
                                    error => {
                                        this.spinnerService.hide();
                                    }
                                );
                            }
                        }
                    }
                }
            }

        }
    }

    formDocumentRequired() {
        let flag = 0;
        for (let index = 0; index < this.activitytype.docTypes.length; index++) {
            if (this.activitytype.docTypes[index].type === 'FORM' && this.activitytype.docTypes[index].req === 1) {
                for (let d = 0; d < this.activityinfo.attachments.length; d++) {
                    if (this.activityinfo.attachments[d].docType.type === 'FORM') {
                        this.ds.getFormJSONDocumentForActivity(this.activityinfo.attachments[d].docId, this.activityinfo.id).subscribe(data => this.showDocumentsTickChecked(data), error => this.spinnerService.hide());
                        // if (this.tempDoc) {

                        // } else {
                        //     this.tempDoc =
                        // }
                    }
                }
                flag = 1;
                break;
            } else {
                flag = 0;
            }
        }
        if (flag === 1) {
            // this.showDocumentsTick = false;
            this.isReqFormDocument = true;
            console.log(this.showDocumentsTick);
        } else {
            this.isReqFormDocument = false;
            // this.showDocumentsTick = true;

        }
    }
    showDocumentsTickChecked(data) {
        let flagDoc = 1;
        const value = JSON.parse(data._body);
        if (value) {
            for (let a = 0; a < value.sections.length; a++) {
                if (value.sections[a].visible === 'TRUE') {
                    if (value.sections[a].rOnly === 'TRUE') {
                        flagDoc = 1;
                    } else {
                        flagDoc = 0;
                        break;
                    }
                }
            }
        }
        if (flagDoc === 0) {
            // this.showDocumentsTick = false;
            console.log(this.showDocumentsTick);
        } else {
            // this.showDocumentsTick = true;
        }
    }
    documentForm(item) {
        this.spinnerService.show();
        this.tempelate = item;
        this.createformtitle = item.name;
        let flag = 0;
        for (let index = 0; index < this.activityinfo.attachments.length; index++) {
            if (this.activityinfo.attachments[index].docType.id === item.id) {
                flag = 0;
                if (this.tempDoc) {
                    if (this.tempDoc.activityId === 0) {
                        this.tempDoc = this
                            .activityinfo
                            .attachments[index];
                    } else {
                        this.tempDoc.activityId = this.activityId;
                    }
                    // this.datatableId = this.tempDoc.docId;
                } else {
                    this.tempDoc = this
                        .activityinfo
                        .attachments[index];
                }
                break;
            } else {
                flag = 1;
            }
        }
        if (flag === 0) {
            this.documentFormTemplate = false;
            this.ds.getFormJSONDocumentForActivity(this.tempDoc.docId, this.activityinfo.id).subscribe(data => this.getJSONFromDocument(data), error => this.spinnerService.hide());
        } else {
            this.documentFormTemplate = true;
            this.ds.getFormJSONDocumentForActivity(this.tempelate.template, this.activityinfo.id).subscribe(data => this.getJSONFromDocument(data), error => this.spinnerService.hide());
        }
    }

    formView(type, docId, format, name, document) {
        if (this.converToUppercase(type) === 'FORM') {
            this.spinnerService.show();
            this.createformtitle = name;
            this.ds.getJSONFromDocument(docId).subscribe(data => this.getJSONFromDocumentReadonly(data, name));
        } else if (type === 'DOCUMENT') {
            this.docPropsShow = true;
            this.documentId = docId;
            this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = 3;
            this.documentPropertyTab(document, 'document', name);
        }
        this.documentProperty = docId;
    }

    responseSelectChange() {
        this.showRichtextComments = false;
        let responseSelect;
        for (let index = 0; index < this.activitytype.responses.length; index++) {
            if (this.activitytype.responses[index].name === this.activityform.controls.responseDropdown.value) {
                responseSelect = this.activitytype.responses[index];
                this.responseSelect = this.activitytype.responses[index];

            }
        }

        if (responseSelect.name === 'Archive' || responseSelect.name === 'Compose Reply') {
            this.hideRoute = true;
            if (responseSelect.name === 'Compose Reply') {
                this.showRichtextComments = true;
                this.activityform.patchValue({
                    responseDropdownId: responseSelect.id,
                    activityDropdownId: responseSelect.routeToId,
                    activityDropdown: responseSelect.routeToName,
                    roleDropdownId: responseSelect.roleId,
                    roleDropdown: responseSelect.roleName,
                    comments: responseSelect.comment
                });
            } else {
                this.showRichtextComments = false;
                this.activityform.patchValue({
                    responseDropdownId: responseSelect.id,
                    activityDropdownId: responseSelect.routeToId,
                    activityDropdown: responseSelect.routeToName,
                    roleDropdownId: responseSelect.roleId,
                    roleDropdown: responseSelect.roleName,
                    comments: responseSelect.comment
                });
            }
        } else {
            this.hideRoute = false;
            this.activityform.patchValue({
                responseDropdownId: responseSelect.id,
                activityDropdownId: responseSelect.routeToId,
                activityDropdown: responseSelect.routeToName,
                roleDropdownId: responseSelect.roleId,
                roleDropdown: responseSelect.roleName,
                comments: responseSelect.comment
            });
        }

        if (responseSelect.roleId !== -5) {
            this.multiRoleSelect = false;
            this.routeToItem = [];
            this.routeToItemRoutes = [];
            if (responseSelect.name === 'Archive') {
                const response = {
                    value: responseSelect.roleId,
                    name: 'None'
                };
                this.routeToItem.push(response);
            } else if (responseSelect.name !== 'Archive') {
                // this.routeToItem.push = {
                //     'value': responseSelect.roleId,
                //     'name': responseSelect.roleName
                // };
                for (let d = 0; d < this.activitytype.responses.length; d++) {
                    if (responseSelect.name === this.activitytype.responses[d].name) {
                        let response;
                        let routes;
                        if (this.activitytype.responses[d].roleName) {
                            response = {
                                value: this.activitytype.responses[d].roleId,
                                name: this.activitytype.responses[d].roleName
                            };
                            routes = {
                                name: this.activitytype.responses[d].roleName,
                                value: this.activitytype.responses[d].roleId,
                                routeToId: this.activitytype.responses[d].routeToId,
                                routeToName: this.activitytype.responses[d].routeToName
                            };
                        } else {
                            response = {
                                name: 'None'
                            };
                            routes = {
                                name: 'None'
                            };
                            // response.name = 'None';
                            // routes.name = 'None';
                        }
                        this.routeToItem.push(response);
                        this.routeToItemRoutes.push(routes);
                    }
                }
            }
            this.activityform.controls.roleDropdown.disable();
        } else {
            this.multiRoleSelect = false;
            this.activityform.controls.roleDropdown.enable();
            this.routeToItem = [];
            this.routeToItemRoutes = [];
        }
        if (responseSelect.roleId === -6) {
            this.multiRoleSelect = false;
            this.activityform.controls.roleDropdown.enable();
            this.routeToItem = [];
            this.routeToItemRoutes = [];
        }
    }

    getJSONFromDocument(data) {
        this.formObject = data._body;
        this.createformtabshow = true;
        this.changeDetectorRef.detectChanges();
        // this.activeIndexNumber = 3;
        this.activeIndexNumber = this.docProps.length + 3;
        this.spinnerService.hide();
    }

    getJSONFromDocumentReadonly(data, name) {
        this.formObject = data._body;
        this.readonlyformtabshow = true;
        this.changeDetectorRef.detectChanges();
        // this.activeIndexNumber = 3;
        // this.activeIndexNumber = this.docProps.length + 3;
        this.spinnerService.hide();
        this.documentPropertyTab(this.formObject, 'form', name);
    }



    finishActivity(data, nextScreen) {
        localStorage.getItem('Default Annotation');
        const route = JSON.parse(data._body);
        const value = nextScreen.toLowerCase();
        this.spinnerService.hide();
        if (route > 0) {
            this.ws.getWorkDraftActivity(route).subscribe(datares => this.openDraft(datares), error => this.spinnerService.hide());
            // this.router.navigateByUrl('inbox/readworkitem/' + route);
            this.spinnerService.hide();
        } else {
            this.spinnerService.hide();
            this.ngxSmartModalService.getModal('tempelateRenderModel').close();
            this.router.navigateByUrl('/' + value);
            this.toastr.success('', 'Completed Successfully');
        }
    }

    openDraft(data) {
        this.toastr.success('', 'Document Routed');
        this.spinnerService.hide();
        const value = JSON.parse(data._body);
        this.ngxSmartModalService.getModal('tempelateRenderModel').close();
        this.router.navigate(['draft/activity'], {
            queryParams: {
                'readworkitem': 'readworkitem',
                workId: value
            }
        });
    }

    downloadDocument(docId) {
        this.ds.downloadAnnotatedDocument(docId).subscribe(data => this.downloadCurrentDocument(data));
    }

    downloadSignedDocument(docId) {
        this.ds.downloadSignedDocument(docId).subscribe(data => this.downloadCurrentDocument(data));
    }
    downloadSimpleDocument(docId) {
        this.ds.downloadDocument(docId).subscribe(data => this.downloadCurrentDocument(data));
    }
    downloadCurrentDocument(data) {
        let filename = '';
        const disposition = data.headers.get('Content-Disposition');
        const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
            filename = matches[1].replace(/['"]/g, '');
        }
        saveAs(data._body, filename);
    }

    recallActivity() {
        this.ws.recallActivity(this.activityinfo.id).subscribe(data => this.getrecallActivity(data));
    }

    getrecallActivity(data) {
        this.router.navigateByUrl('/sent');
    }

    backButtonClicked() {
        const back = this.breadcrumbService.getItem();
        // console.log(back);
        if (this.breadcrumbService.getItem() !== 'Advanced Search') {
            if (this.router.url.includes('inbox')) {
                this.router.navigate(['inbox']);
            } else if (this.router.url.includes('sent')) {
                this.router.navigate(['sent']);
            } else if (this.router.url.includes('draft')) {
                this.router.navigate(['draft']);
            } else if (this.router.url.includes('register')) {
                this.router.navigate(['register']);
            } else if (this.router.url.includes('archive')) {
                this.router.navigate(['archive']);
            } else {
            }
        } else {
            window.history.back();
        }
    }

    tempObject(obj, name) {
        //   console.log('ok');
        this.tempFileobject = obj;
        const date = new Date();
        this.systemDate = date;
        for (let index = 0; index < this.activitytype.docTypes.length; index++) {
            if (this.activitytype.docTypes[index].type === 'DOCUMENT' && this.activitytype.docTypes[index].name === name) {
                localStorage.setItem('itemid', this.duplicateActivity.docTypes[index].id);
                localStorage.setItem('itemName', this.duplicateActivity.docTypes[index].name);
                this.tempFileobject = this.activitytype.docTypes[index].file;
            }
        }
    }
    getRecent() {
        this.cs.getRecent(1).subscribe(data => this.getRecentDocuments(data));
    }

    getRecentDocuments(data) {
        if (data._body !== '') {
            this.recentDocumentList = JSON.parse(data._body);
            this.generic = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            this.activeIndexNumber = this.docProps.length + 3;

        }
    }

    inputfileChanged(id, docClass) {
        for (let i = 0; i < this.globelFileUploder.queue.length; i++) {
            // if (this.globelFileUploder.queue[i]._file.type === 'application/octet-stream'
            // || this.globelFileUploder.queue[i]._file.type === 'application/java-archive'
            // || this.globelFileUploder.queue[i]._file.type === 'application/x-zip-compressed'
            // || this.globelFileUploder.queue[i]._file.type === 'application/x-msdownload') {
            //     //   this.fileNameList.push(this.globelFileUploder.queue[i]._file.name);
            //     this
            //         .ts
            //         .error(
            //             '',
            //             this.globelFileUploder.queue[i]._file.name + ': Not able to attach please select only pdf, doc and image files'
            //         );
            //     this
            //         .globelFileUploder
            //         .removeFromQueue(this.globelFileUploder.queue[i]);
            //     i--;
            // } else {
            //     this.addDocument(id, docClass);
            // }
        }
        this.addDocument(id, docClass);
    }

    addDocument(fileUploader, docClass) {
        this.spinnerService.show();
        let count = 0;
        for (let i = 0; i < this.globelFileUploder.queue.length; i++) {
            const adddocument = [];
            this.spinnerService.show();
            const docInfo = {
                docclass: docClass,
                props: [
                    {
                        'name': 'Document Title',
                        'symName': 'DocumentTitle',
                        'dtype': 'STRING',
                        'mvalues': [
                            this.globelFileUploder.queue[i]._file.name
                        ],
                        'mtype': 'N',
                        'len': 255,
                        'rOnly': 'false',
                        'hidden': 'false',
                        'req': 'false'
                    }
                ],
                accessPolicies: []
            };
            this.formData = new FormData();
            this.formData.append('DocInfo', JSON.stringify(docInfo));
            this.formData.append('file' + i, this.globelFileUploder.queue[i]._file);
            this.ds.addDocument(this.formData).subscribe(data => {
                adddocument.push({
                    docid: data,
                    docTitle: this.globelFileUploder.queue[i]._file.name,
                    format: this.globelFileUploder.queue[i]._file.type
                });
                this.saveAttachemtObjects(adddocument, ++count);
                // console.log(data);
                this.spinnerService.hide();
            }, error => this.spinnerService.hide());
        }
    }

    saveAttachemtObjects(data, count) {
        this.showDocOrForms = true;
        const attachments = {
            docId: data[0].docid._body,
            docTitle: data[0].docTitle,
            format: data[0].format,
            docType: {
                id: localStorage.getItem('itemid'),
                name: localStorage.getItem('itemName')
            }
        };
        this.spinnerService.hide();
        this.ts.success('', 'Document Added');
        this.tempAttachments.push(attachments);
        // this.ws.getActivityForAction(this.typeId).subscribe(dataRes => this.getactivityinfo(dataRes), error => { });

        const date: number = new Date().getTime();
        // contents must be an array of strings, each representing a line in the new
        // file
        const file = new File([''], data[0].docTitle, {
            type: data[0].format,
            lastModified: date
        });
        const fileItem = new FileItem(this.tempFileobject, file, {});
        // (Visual Only) adds the new fileItem to the upload queue
        this.tempFileobject.queue.push(fileItem);
        if (count === this.globelFileUploder.queue.length) {
            this.globelFileUploder.clearQueue();
            this.saveActivity('event');
        }
        // this.spinnerService.hide();
        // this.saveActivity('event');
    }

    removeAttachments(fileobj) {
        for (let index = 0; index < this.tempAttachments.length; index++) {
            if (fileobj.docId === this.tempAttachments[index].docId) {
                this.ws.removeWorkAttachment(this.tempAttachments[index].id).subscribe(data => {
                    this.tempAttachments.splice(index, 1);
                    this.ts.info('', 'Document removed');
                }, error => { });
                break;
            }
        }
        for (let index = 0; index < this.activitytype.docTypes.length; index++) {
            if (this.activitytype.docTypes[index].id === +fileobj.docType.id) {
                this.activitytype.docTypes[index].file.clearQueue();
                break;
            }
        }
        const doc = <HTMLInputElement>document.getElementById(fileobj.docType.id);
        doc.value = '';
        if (this.tempAttachments.length > 0) {
            this.showDocOrForms = true;
        } else {
            this.showDocOrForms = false;
        }
        this.saveActivity('event');
    }

    // activityselectSingle(event) {

    // }

    //  Scane code
    showClassPropScan(item) {
        this.selectedClassItem = item;
        this.cs.getDocumentClasses().subscribe(data => this.documentClassesScan(data, item));
    }

    documentClassesScan(data, item) {
        if (data._body !== '') {
            this.documentClass = JSON.parse(data._body);
            for (let index = 0; index < this.documentClass.length; index++) {
                if (this.documentClass[index].symName === item.docClass) {
                    this.docProperty = this.documentClass[index];
                    this.documentClassProp = this.documentClass[index].props;
                    this.docClassSymName = this.documentClass[index].value;
                    this.docClassName = this.documentClass[index].name;
                }
            }
            for (let index = 0; index < this.documentClassProp.length; index++) {
                if (this.documentClassProp[index].req === 'true' && this.documentClassProp[index].symName !== 'documenttitle') {
                    const control: FormControl = new FormControl(null, Validators.required);
                    this.documentAdd.addControl(this.documentClassProp[index].symName, control);
                } else {
                    const control: FormControl = new FormControl(null);
                    this.documentAdd.addControl(this.documentClassProp[index].symName, control);
                } if (this.documentClassProp[index].lookup !== undefined && this.documentClassProp[index].symName !== 'documenttitle') {
                    const control: FormControl = new FormControl(null);
                    this.documentAdd.addControl(this.documentClassProp[index].lookups[0].value, control);
                }
            }
        }
        this.attachmentfileName = null;
        this.documentAdd.reset();
        this.ngxSmartModalService.getModal('documentAddScanWithProps').open();
    }

    scanImage(id, docClass) {
        const scannedPdfName = this.activityinfo.refNo;
        const socket = new WebSocket('ws://localhost:8181');
        socket.onopen = () => socket.send('1100');
        socket.onmessage = (e) => this.onMessage(e, 'notupdate', docClass);
        socket.onerror = (e) => this.onWebsocketError(e);
    }

    onMessage(e, isupdate, docClass) {
        this.spinnerService.show();
        this.updatePrimaryDocument.queue.splice(0, 1);
        this.websocketNotOpen = false;
        const storedFiles = [];
        let count = 0;
        if (typeof e.data === 'string') {
            // IF Received Data is String
            // console.log('string');
        } else if (e.data instanceof ArrayBuffer) {
            // IF Received Data is ArrayBuffer
            // console.log('ArrayBuffer');
        } else if (e.data instanceof Blob && count <= 1) {
            const f = e.data;
            count++;
            const reader = new FileReader();
            reader.onload = (f: FileReaderEvent) => {
                const pdfdata = f.target.result;
                const byteCharacters = atob(
                    pdfdata.replace('data:application/octet-stream;base64,', '')
                );
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                const byteArray = new Uint8Array(byteNumbers);
                const blob = new Blob([byteArray], { type: 'application/pdf' });
                // saveAs(blob, this.activityinfo.refNo);
                let string: String;
                string = this.activityinfo.refNo;

                const docInfo = {
                    docclass: docClass,
                    props: [
                        {
                            'name': 'Document Title',
                            'symName': 'DocumentTitle',
                            'dtype': 'STRING',
                            'mvalues': [
                                string
                                    .replace('/', '-')
                                    .concat('.pdf')
                            ],
                            'mtype': 'N',
                            'len': 255,
                            'rOnly': 'false',
                            'hidden': 'false',
                            'req': 'false'
                        }
                    ],
                    accessPolicies: []
                };
                const formData = new FormData();
                formData.append('DocInfo', JSON.stringify(docInfo));
                // formData.append('file', scannedPdfName);
                formData.append('document', blob, this.activityinfo.refNo);
                if (isupdate !== 'update') {
                    this.ds.addDocument(formData).subscribe(data => this.scannedDocument(data), error => this.spinnerService.hide());
                } else {
                    const file = new File(
                        [blob],
                        string.replace('/', '-').concat('.pdf'),
                        { type: 'application/pdf' }
                    );
                    this.updatePrimaryDocument.queue = [];

                    const fileItem = new FileItem(this.updatePrimaryDocument, file, {});
                    this.updatePrimaryDocument.queue.push(fileItem);
                    this.showUpdateScanned = true;
                }
            };
            reader.readAsDataURL(f);
        }
    }
    scannedDocument(data) {
        this.showDocOrForms = true;
        const str = 'Scanned-';
        const attachments = {
            docId: data._body,
            docTitle: str.concat(this.activityform.controls.refno.value),
            format: 'application/pdf',
            docType: {
                id: localStorage.getItem('itemid'),
                name: localStorage.getItem('itemName')
            }
        };
        this.ts.success('', 'Document added');
        this.tempAttachments.push(attachments);
        this.uploadFile(str.concat(this.activityform.controls.refno.value));
        // this.ws.getActivityForAction(this.typeId).subscribe(dataRes => this.getactivityinfo(dataRes), error => { });
        this.spinnerService.hide();
    }

    uploadFile(desiredFilename: string) {
        const date: number = new Date().getTime();
        // contents must be an array of strings, each representing a line in the new
        // file
        const file = new File([''], desiredFilename, {
            type: 'text/plain',
            lastModified: date
        });
        const fileItem = new FileItem(this.tempFileobject, file, {});
        // (Visual Only) adds the new fileItem to the upload queue
        this.tempFileobject.queue.push(fileItem);
        // this.saveActivity('event');
    }

    onWebsocketError(e) {
        if (this.websocketNotOpen === false && e !== '') {
            this.websocketNotOpen = true;
            const socket = new WebSocket('ws://localhost:8181');
            socket.close();
            this.ngxSmartModalService.getModal('hashTwainModel').open();
        }
    }

    annotationClicked() {
        if (this.readworkitem === 'readworkitem') {
            this.router.navigate([
                window.location.hash.split('/')[1] + '/documentview'
            ], {
                    queryParams: {
                        'documentType': 'annotate',
                        'docId': this.docId,
                        'currentPage': 1,
                        'annotationType': 'COMMENT'
                    }
                });
        } else {
            this.router.navigate([window.location.hash.split('/')[1] + '/documentview'], { queryParams: { 'documentType': 'readonly', 'docId': this.docId, 'currentPage': 1, 'annotationType': 'IMAGE' } });
        }
    }

    formDynamicSubmit(event) {
        this.formObjectSubmit = event;
        const date = new Date();
        const filename = this.createformtitle.concat(date.getTime().toString()) + '.JSON';
        const docInfo = {
            id: this.tempelate.template,
            docclass: 'ProductivitiDocument',
            props: [
                {
                    'name': 'Document Title',
                    'symName': 'DocumentTitle',
                    'dtype': 'STRING',
                    'mvalues': [
                        filename
                    ],
                    'mtype': 'N',
                    'len': 255,
                    'rOnly': 'false',
                    'hidden': 'false',
                    'req': 'false'
                }
            ],
            accessPolicies: []
        };
        const formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));

        for (const docClass of this.formObjectSubmit) {
            docClass.sections.forEach(element => {
                if (this.converToUppercase(element.type) === 'FORM') {
                    for (const cols of element.columns) {
                        for (const props of cols.properties) {
                            delete props.dbValue;
                            delete props.dbDummyValue;
                            delete props.lookupOptions;

                        }
                    }
                } else if (this.converToUppercase(element.type) === 'TABLE') {
                    for (const row of element.rows) {
                        for (const item of row.items) {
                            delete item.rOnly;
                            delete item.req;
                            delete item.label;
                            delete item.type;
                            delete item.length;
                            delete item.lookup;
                            delete item.lookups;
                            delete item.dblookup;
                        }
                    }
                }
            });
        }

        formData.append('file', new Blob([JSON.stringify(this.formObjectSubmit)], { type: 'application/json' }));
        if (this.documentFormTemplate === true) {
            // this
            //     .ds
            //     .addJSONDocument(this.formObjectSubmit.pop())
            //     .subscribe(data => {
            //         this.addDocumentAttachement(data);
            //     });
            this.ds.saveFormDocument(this.formObjectSubmit.pop()).subscribe(data => {
                this.addDocumentAttachement(data);
            });
        } else {
            // this
            //     .ds
            //     .checkOut(this.tempDoc.docId)
            //     .subscribe(data => {
            //         this.checkinfile(data, formData);
            //     });
            let flag = 0;
            if (this.formObjectSubmit[0].datatable.length > 0) {
                for (let index = 0; index < this.formObjectSubmit[0].datatable.length; index++) {
                    if (this.formObjectSubmit[0].datatable[index].key === 'DOCID') {
                        this.formObjectSubmit[0].datatable[index].value = this.tempDoc.docId;
                        flag = 1;
                        break;
                    } else {
                        flag = 0;
                    }
                }
            } else {
                flag = 0;
            }
            if (flag === 0) {
                //  this.formObjectSubmit[0].datatable.push({'DOCID': this.tempDoc.docId});
                this.formObjectSubmit[0].datatable.push({ 'key': 'DOCID', 'value': this.tempDoc.docId });
            }
            this.ds.saveFormDocument(this.formObjectSubmit.pop()).subscribe(datares => { this.chechedin(datares); });
            // console.log(this.formObjectSubmit);
            // this.ds.saveFormDocument(this.formObjectSubmit.pop()).subscribe(data => {
            //     this.addDocumentAttachement(data);
            // });
        }
        this.createformtabshow = false;
        this.activeIndexNumber = 0;
        this.changeDetectorRef.detectChanges();
    }

    addDocumentAttachement(data) {
        let flag = 0;
        const document = {
            docId: data._body,
            docTitle: this.createformtitle,
            docType: this.tempelate,
            format: 'application/json'
        };
        this.activityinfo.attachments.push(document);
        for (let index = 0; index < this.tempAttachments.length; index++) {
            if (this.tempAttachments[index].docId === document.docId) {
                this.tempAttachments.splice(index, 1);
                flag = 0;
                break;
            }
        }
        this.tempAttachments.push(document);
    }

    checkinfile(data, response) {
        this.createformtabshow = false;
        this.activeIndexNumber = 0;
        // this.showDocumentsTick = true;
        const date = new Date();
        const docInfo = {
            id: data._body,
            docclass: 'ProductivitiDocument',
            props: [
                {
                    'name': 'Document Title',
                    'symName': 'DocumentTitle',
                    'dtype': 'STRING',
                    'mvalues': [
                        this
                            .createformtitle
                            .concat(date.getTime().toString()) + '.JSON'
                    ],
                    'mtype': 'N',
                    'len': 255,
                    'rOnly': 'false',
                    'hidden': 'false',
                    'req': 'false'
                }
            ],
            accessPolicies: []
        };

        for (const docClass of this.formObjectSubmit) {
            docClass.sections.forEach(element => {
                if (this.converToUppercase(element.type) === 'FORM') {
                    for (const cols of element.columns) {
                        for (const props of cols.properties) {
                            delete props.dbValue;
                            delete props.dbDummyValue;
                            delete props.lookupOptions;
                        }
                    }
                } else if (this.converToUppercase(element.type) === 'TABLE') {
                    for (const row of element.rows) {
                        for (const item of row.items) {
                            delete item.rOnly;
                            delete item.req;
                            delete item.label;
                            delete item.type;
                            delete item.length;
                            delete item.lookup;
                            delete item.lookups;
                            delete item.dblookup;
                        }
                    }
                }
            });
        }
        const formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        formData.append(
            'file',
            new Blob([JSON.stringify(this.formObjectSubmit.pop())], { type: 'application/json' }),
            (this.createformtitle.concat(date.getTime().toString() + '.JSON'))
        );
        this.ds.checkIn(formData).subscribe(datares => {
            this.chechedin(datares);
        });
    }

    chechedin(data) {
        this.datatableId = data._body;
        this.ts.success('Saved Successfully');
        // this.showDocumentsTick = true;
        this.tempDoc.docId = data._body;
        for (let i = 0; i < this.tempAttachments.length; i++) {
            if (this.tempAttachments[i].docType.id === this.tempDoc.id) {
                this.tempAttachments[i].docId = data._body;
                this.tempDoc.docId = data._body;
                break;
            }
        }
        this.saveActivity('event');
        this.ws.updatePrimaryDocument(this.activityinfo.workId, this.activityinfo.id).subscribe(datares => this.updatePrimaryDoc(datares));

        //  this is no the ideal way of doing but it's an imidate fix
    }

    updatePrimaryDoc(data) {
        this.activityinfo.primaryDoc = '';
        this.ws.getActivityInfo(this.typeId).subscribe(datares => this.getactivityinfoDoc(datares));
        this.createformtabshow = false;

    }

    getactivityinfoDoc(datares) {
        const activityInfo = JSON.parse(datares._body);
        this.activityId = activityInfo.id;
        this.ws.updateAttachmentActivity(this.tempDoc.id, this.activityId).subscribe(datas => { this.updateActivityFormId(datas); }, error => { });
        this.changeDetectRef.detectChanges();
        this.activityinfo.primaryDoc = activityInfo.primaryDoc;

    }
    updateActivityFormId(data) {
        // this.tempDoc.activityId = this.activityId;
        this.showDocumentsTick = true;
    }
    onTabChange(event) {
        if (event.index !== 3) {
            this.createformtabshow = false;
            this.docPropsShow = false;
            this.readonlyformtabshow = false;
            this.printPreviewTab = false;
            this.generic = false;
            this.annotationPreviewTab = false;
            this.activeIndexNumber = event.index;
            this.changeDetectorRef.detectChanges();
        }
        this.annotationPreviewTab = false;
        this.activeIndexNumber = event.index;
        this.changeDetectorRef.detectChanges();
        if (event.index === 0) {
            // const date = new Date();
            // this.timestamp = date.getSeconds();
            this.ngOnChanges();

        }
    }

    cancelForm(val) {
        this.createformtabshow = false;
        this.activeIndexNumber = 0;
        this.changeDetectorRef.detectChanges();
    }

    updateScanImage(update) {
        const scannedPdfName = this.activityinfo.refNo;
        const socket = new WebSocket('ws://localhost:8181');
        socket.onopen = () => socket.send('1100');
        socket.onmessage = (e) => this.onMessage(e, update, 'ProductivitiDocument');
    }

    clickedFile() {
        if (this.updatePrimaryDocument.queue.length >= 2) {
            this.updatePrimaryDocument.queue.splice(0, 1);
        }
        this.showUpdateScanned = true;
    }


    documentSearchList(event) {
        this.documentsSelected = event;
        for (let i = 0; i < this.documentsSelected.length; i++) {
            // if (this.documentsSelected[i].format !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' &&
            // this.documentsSelected[i].format !== 'application/pdf' &&
            // this.documentsSelected[i].format !== 'application/msword' &&
            // this.documentsSelected[i].format !== 'image/jpeg' &&
            // this.documentsSelected[i].format !== 'image/png') {
            //     this
            //         .ts
            //         .error(
            //             '',
            //             this.documentsSelected[i].name + ': Not able to attach please select only pdf, ' +
            //                     'doc and image files'
            //         );
            //     this
            //         .documentsSelected
            //         .splice(i, 1);
            // } else {}
        }
    }

    attachRepDoc() {
        this.showDocOrForms = true;
        for (let index = 0; index < this.documentsSelected.length; index++) {
            const attachments = {
                docId: this.documentsSelected[index].docId,
                docTitle: this.documentsSelected[index].name,
                format: this.documentsSelected[index].format,
                docType: {
                    id: localStorage.getItem('itemid'),
                    name: localStorage.getItem('itemName')
                }
            };
            this.ts.success('', 'Document added');
            this.tempAttachments.push(attachments);

        }
        // for (let index = 0; index < this.documentsSelected.length; index++) {
        //     this.uploadFile(this.documentsSelected[index].name);
        // }
        this.documentsSelected = [];
        this.generic = false;
        this.activeIndexNumber = 0;
        this.changeDetectRef.detectChanges();
        this.saveActivity('event');
    }

    listDocumentAnnotation() {
        this.cs.getAnnotations(this.docId).subscribe(data => this.getListOfAnnoattion(data));
    }

    getListOfAnnoattion(data) {
        const resData = JSON.parse(data._body);
        if (resData.length <= 0) {
            this.tr.info('', 'No annotation found on this document');
            this.annotationCount = 0;
            this.ngxSmartModalService.getModal('annotationListActivityModel').close();
        } else {
            this.listOfDcumentAnnotation = JSON.parse(data._body);
            this.annotationCount = this.listOfDcumentAnnotation.length;
            if (this.listOfDcumentAnnotation.length > 0) {
                this.ngxSmartModalService.getModal('annotationListActivityModel').open();

            }
        }

    }

    saveActivity(event) {
        const activitySubmit = {
            id: this.typeId,
            typeId: this.activitytype.id,
            modifiedBy: this.us.getCurrentUser().EmpNo,
            modifierRole: this.us.getCurrentUser().roles['0'].id,
            finishedBy: this.us.getCurrentUser().EmpNo,
            finishedByName: this.us.getCurrentUser().fulName,
            finisherRole: this.us.getCurrentUser().roles['0'].id,
            comments: this.activityform.controls.comments.value,
            deadline: this.activityform.controls.deadline.value,
            attachments: this.tempAttachments,
            responseId: this.activityform.controls.responseDropdownId.value,
            routes: [],
            respPurpose: this.responseSelect.purpose
        };

        const routes = {
            activityType: this.activityform.controls.activityDropdownId.value,
            activityTypeName: this.activityform.controls.activityDropdown.value,
            roleId: this.routeToItem.value,
            roleName: this.routeToItem.name
        };
        activitySubmit.routes.push(routes);
        this.ws.saveActivity(activitySubmit).subscribe(data => {
            if (event === 'event') {
                this.ws.getActivityForAction(this.typeId).subscribe(dataRes => this.getactivityinfo(dataRes), error => { });
            }
        });

        if (this.editSubject) {
            this.ws.updateSubject(this.activityinfo.id, encodeURIComponent(this.activityform.controls.subject.value)).subscribe();
        }
    }

    documnetAnnoation(pageNo, type, event) {
        this.annotationType = type;
        this.AnnotationCurrentPage = pageNo;
        if (this.router.url.includes('inbox')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('sent')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('draft')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('register')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('archive')) {
            this.annotationDocumentType = 'readonly';
        } else {
            this.annotationDocumentType = 'annotate';
        }
        this.annotationDocId = this.docId;
        if (this.documnetAnnoationType === 'docs') {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            this.activeIndexNumber = this.docProps.length + 3;
        } else {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 4;
            this.activeIndexNumber = this.docProps.length + 3;
        }
    }

    checkInSubmit(event) {
        this.ds.checkOut(this.activityinfo.primaryDoc).subscribe(data => this.documentCheckedOut(data));
    }

    documentCheckedOut(data) {
        const docInfo = {
            id: data._body,
            docclass: 'ProductivitiDocument',
            props: [
                {
                    'name': 'Document Title',
                    'symName': 'DocumentTitle',
                    'dtype': 'STRING',
                    'mvalues': [this.activityinfo.name],
                    'mtype': 'N',
                    'len': 255,
                    'rOnly': 'false',
                    'hidden': 'false',
                    'req': 'false'
                }
            ],
            accessPolicies: []
        };
        const formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        formData.append('file', this.updatePrimaryDocument.queue[0]._file);
        this.ds.checkIn(formData).subscribe(datares => this.getCheckIn(datares));

    }

    getCheckIn(datares) {
        this.tr.success('', 'Primary document updated');
        this.activityinfo.primaryDoc = datares._body;
        this.showUpdateScanned = false;
        // this.ngOnInit();
        this.ws.getActivityForAction(this.typeId).subscribe(data => this.getactivityinfo(data), error => this.spinnerService.hide());
        this.ws.getAcitvityHistory(this.typeId).subscribe(data => this.getHistory(data), error => this.spinnerService.hide());
    }

    printPrimaryDocument(primaryDoc) {
        this.ds.downloadPrintDocument(primaryDoc).subscribe(data => this.downloadPrintDocument(data), error => { this.tr.error('', 'You can not print this document'); });
    }
    docPrint(event) {
        console.log(event);
    }
    docAnnotation(event) {
        console.log(event);
    }
    downloadPrintDocument(data) {
        this.pdfUrl = data._body;
        const file3 = new Blob([data._body], { type: 'application/pdf' });
        this.pdfUrl = this._sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(file3));
        this.printPreviewTab = true;
        this.changeDetectorRef
            .detectChanges();
        // this.activeIndexNumber = 3;
        console.log(this.docProps.length);
        this.activeIndexNumber = this.docProps.length + 3;


    }

    checkboxChecked() {
    }

    activityComplete(event) {
        for (const inputField of [].slice.call(event.target)) {
            for (let index = 0; index < this.responseSelectProps.length; index++) {
                if (inputField.id === this.responseSelectProps[index].name) {
                    this.responseSelectProps[index].value = [
                        {
                            'value': inputField.value
                        }
                    ];
                }
                delete this.responseSelectProps[index].dbValue;
                delete this.responseSelectProps[index].dbDummyValue;
                delete this.responseSelectProps[index].lookupOptions;
            }
        }
        for (
            const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))
        ) {
            for (let index = 0; index < this.responseSelectProps.length; index++) {
                if (inputField.id === this.responseSelectProps[index].name) {
                    this.responseSelectProps[index].value = [
                        {
                            'value': inputField.children['0'].children['0'].value
                        }
                    ];
                }
            }
        }
        for (
            const inputField of [].slice.call(event.target.getElementsByTagName('p-editor'))
        ) {
            for (let index = 0; index < this.responseSelectProps.length; index++) {
                if (inputField.id === this.responseSelectProps[index].name) {
                    this.responseSelectProps[index].value = [
                        {
                            'value': inputField.children[0].children[1].children[0].innerHTML
                        }
                    ];
                }
            }
        }

        const finishForm = {
            name: this.responseSelect.finishForm.name,
            properties: this.responseSelectProps
        };
        const activitySubmit = {
            id: this.typeId,
            typeId: this.activitytype.id,
            modifiedBy: this.us.getCurrentUser().EmpNo,
            modifierRole: this.us.getCurrentUser().roles['0'].id,
            finishedBy: this.us.getCurrentUser().EmpNo,
            finishedByName: this.us.getCurrentUser().fulName,
            finisherRole: this.us.getCurrentUser().roles['0'].id,
            comments: this.activityform.controls.comments.value,
            deadline: this.activityform.controls.deadline.value,
            attachments: this.tempAttachments,
            responseId: this.activityform.controls.responseDropdownId.value,
            routes: [],
            finishForm: finishForm,
            respPurpose: this.responseSelect.purpose
        };
        const routes = {
            activityType: this.activityform.controls.activityDropdownId.value,
            activityTypeName: this.activityform.controls.activityDropdown.value,
            roleId: this.routeToItem.value,
            roleName: this.routeToItem.name
        };

        activitySubmit.routes.push(routes);
        this.ngxSmartModalService.getModal('tempelateRenderModel').close();
        this.spinnerService.show();
        this.ws.saveActivity(activitySubmit).subscribe(data => this.ws.finishActivity(activitySubmit).subscribe(res => this.finishActivity(res, this.activitytype.nextScreen.toLowerCase()), err => {
            this.activityform.enable();
            this.spinnerService.hide();
        }),
            error => {
                this.spinnerService.hide();
            }
        );
        if (this.editSubject) {
            this.ws.updateSubject(this.activityinfo.id, encodeURIComponent(this.activityform.controls.subject.value)).subscribe(
                data => this.updateSubject(data, activitySubmit)
            );
        }
    }

    updateSubject(data, activitySubmit) {

        this.activityform.disable();
        this.tempelateRender.disable();
        this.spinnerService.hide();
        this.ws.saveActivity(activitySubmit).subscribe(
            datares => this.ws.finishActivity(activitySubmit).subscribe(res => {
                this.finishActivity(res, this.activitytype.nextScreen);
            }, errr => {
                this.spinnerService.hide();
            }),
            err => {
                this.spinnerService.hide();
            }
        );
    }

    flagActivitiy() {
        this.ws.flagActivity(this.typeId, 1, this.us.getCurrentUser().EmpNo).subscribe(data => this.updateFlag(data));
    }

    removeflagActivitiy() {
        this.ws.flagActivity(this.typeId, 0, this.us.getCurrentUser().EmpNo).subscribe(data => this.updateFlag(data));
    }

    updateFlag(data) {
        // this.ngOnInit();
        this.ws.getActivityForAction(this.typeId).subscribe(data => this.getactivityinfo(data), error => this.spinnerService.hide());
        this.ws.getAcitvityHistory(this.typeId).subscribe(data => this.getHistory(data), error => this.spinnerService.hide());
    }

    documentAnnotationClicked(event, showComplete) {
        // this.documnetAnnoation(event.pagenumber, localStorage.getItem('Default Annotation'), event);
        this.annotationType = localStorage.getItem('Default Annotation');
        this.AnnotationCurrentPage = event.page + 1;
        this.annotationDocId = event.docId;
        this.activityScreen = 'document';
        if (this.router.url.includes('inbox')) {
            this.annotationDocumentType = 'annotate';
            this.activityScreen = showComplete;
        } else if (this.router.url.includes('sent')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('draft')) {
            this.annotationDocumentType = 'annotate';
            this.activityScreen = showComplete;
        } else if (this.router.url.includes('register')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('archive')) {
            this.annotationDocumentType = 'readonly';
        } else {
            this.annotationDocumentType = 'annotate';
        }
        if (this.documnetAnnoationType === 'docs') {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            // this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = this.docProps.length + 3;
        } else {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            this.activeIndexNumber = this.docProps.length + 3;
        }
    }


    showClassProp(item) {
        this.selectedClassItem = item;
        this.cs.getDocumentClasses().subscribe(data => this.documentClasses(data, item));
    }

    documentClasses(data, item) {
        if (data._body !== '') {
            this.documentClass = JSON.parse(data._body);
            for (let index = 0; index < this.documentClass.length; index++) {
                if (this.documentClass[index].symName === item.docClass) {
                    this.docProperty = this.documentClass[index];
                    this.documentClassProp = this.documentClass[index].props;
                    this.docClassSymName = this.documentClass[index].value;
                    this.docClassName = this.documentClass[index].name;
                }
            }
            for (let index = 0; index < this.documentClassProp.length; index++) {
                if (this.documentClassProp[index].lookups === undefined) {
                    if (this.documentClassProp[index].req === 'TRUE' || this.documentClassProp[index].req === 'true') {
                        let control: FormControl;
                        if (this.documentClassProp[index].mvalues[0] !== undefined) {
                            control = new FormControl(this.documentClassProp[index].mvalues[0], Validators.required);
                        } else {
                            control = new FormControl(null, Validators.required);
                        }
                        this.documentAdd.addControl(this.documentClassProp[index].symName, control);
                    } else {
                        let control: FormControl;
                        if (this.documentClassProp[index].mvalues[0] !== undefined) {
                            control = new FormControl(this.documentClassProp[index].mvalues[0]);
                        } else {
                            control = new FormControl(null);
                        }
                        this.documentAdd.addControl(this.documentClassProp[index].symName, control);
                    }
                } else if (this.documentClassProp[index].lookups !== undefined) {
                    let control: FormControl;
                    if (this.documentClassProp[index].mvalues[0] !== undefined) {
                        control = new FormControl(this.documentClassProp[index].mvalues[0].value);
                    } else {
                        control = new FormControl(this.documentClassProp[index].lookups[0].value);
                    }
                    this.documentAdd.addControl(this.documentClassProp[index].symName, control);
                } else {

                }
            }
        }
        this.checkInFileUploder.nativeElement.value = null;
        this.documentAdd.reset();
        this.ngxSmartModalService.getModal('documentAddWithProps').open();
    }

    fileChanegd(event) {
        this.attachmentfile = null;
        this.attachmentfile = event.target.files[0];
        this.attachmentfileName = event.target.files[0].name;
        for (let i = 0; i < this.documentClassProp.length; i++) {
            if (this.documentClassProp[i].symName === 'documenttitle') {
                this.documentAdd.controls[this.documentClassProp[i].symName].patchValue(event.target.files[0].name);
            }
        }
    }

    documentSubmit(event) {
        let count = 0;
        let adddocument;
        adddocument = [];
        const docInfo = {
            id: this.docProperty.id,
            creator: this.docProperty.creator,
            addOn: this.docProperty.addOn,
            modOn: this.docProperty.modOn,
            docclass: this.docProperty.symName,
            props: []
        };
        for (const inputField of [].slice.call(event.target)) {
            if (inputField.getAttribute('type') !== 'file') {
                for (const prop of this.docProperty.props) {
                    if (inputField.id !== undefined && inputField.id === prop.symName) {
                        const property = {
                            'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value], mtype: prop.mtype,
                            len: prop.len, rOnly: prop.rOnly, hidden: prop.hidden, req: prop.req, ltype: prop.ltype
                        };
                        docInfo.props.push(property);
                    }
                }
            }
        }

        for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
            if (inputField.getAttribute('type') !== 'file') {
                for (const datepick of this.docProperty.props) {
                    if (inputField.id !== undefined && inputField.id === datepick.symName) {
                        const property = {
                            'symName': datepick.symName, 'dtype': datepick.dtype,
                            'mvalues': [inputField.children['0'].children['0'].value],
                            len: datepick.len, rOnly: datepick.rOnly, hidden: datepick.hidden, req: datepick.req, ltype: datepick.ltype
                        };
                        docInfo.props.push(property);
                    }
                }
            }
        }

        const formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        formData.append('file', this.attachmentfile);
        this.ds.addDocument(formData).subscribe(data => {
            adddocument.push({
                docid: data,
                docTitle: this.attachmentfileName,
                format: this.attachmentfile.type
            });
            this.ngxSmartModalService.getModal('documentAddWithProps').close();
            this.ngxSmartModalService.getModal('documentAddScanWithProps').close();

            this.documentAdd.reset();
            this.saveAttachemtObjects(adddocument, ++count);
        }, error => { });
    }

    showHistoryModel(history) {
        this.selectedComments = history.details;
        if (this.selectedComments !== undefined) {
            // this.commentsModel = true;
            this.ngxSmartModalService.getModal('commentsModel').open();
        }
    }

    scanImageWithProp() {
        const socket = new WebSocket('ws://localhost:8181');
        socket.onopen = () => socket.send('1100');
        // console.log('opned');
        socket.onmessage = (e) => { this.onMessageWithProp(e); console.log(e); };
        socket.onerror = (e) => { this.onWebsocketError(e); console.log(e); };
    }

    onMessageWithProp(e) {
        // console.log(e.data);
        this.websocketNotOpen = false;
        const storedFiles = [];
        let count = 0;
        if (typeof e.data === 'string') {
            // IF Received Data is String
        } else if (e.data instanceof ArrayBuffer) {
            // IF Received Data is ArrayBuffer
        } else if (e.data instanceof Blob && count <= 1) {
            const f = e.data;
            count++;
            const reader = new FileReader();
            reader.onload = (f: FileReaderEvent) => {
                const pdfdata = f.target.result;
                const byteCharacters = atob(pdfdata.replace('data:application/octet-stream;base64,', ''));
                const byteNumbers = new Array(byteCharacters.length);
                for (let i = 0; i < byteCharacters.length; i++) {
                    byteNumbers[i] = byteCharacters.charCodeAt(i);
                }
                const byteArray = new Uint8Array(byteNumbers);
                const blob = new Blob([byteArray], { type: 'application/pdf' });
                let date;
                date = new Date();
                for (let i = 0; i < this.documentClassProp.length; i++) {
                    if (this.documentClassProp[i].symName === 'documenttitle') {
                        this.documentAdd.controls[this.documentClassProp[i].symName].patchValue('ScannedImage'.concat(date.getTime().toString()).concat('.pdf'));
                    }
                }
                this.attachmentfile = null;
                this.attachmentfile = blob;

                this.attachmentfileName = 'ScannedImage'.concat(date.getTime().toString()).concat('.pdf');
            };
            reader.readAsDataURL(f);
        }
    }

    deleteAnnoation(id) {
        this.cs.deleteAnnotation(id).subscribe(data => { this.deleteAnnotation(data); });
    }

    deleteAnnotation(data) {
        this
            .cs
            .getAnnotations(this.docId)
            .subscribe(datares => this.getListOfAnnoattion(datares));
        this.tr.success('', 'Annotation deleted');
    }

    downloadWithDocument(docId) {
        this.cs.downloadAnnotatedDocument(docId).subscribe(data => this.downloadCurrentDocument(data));
    }

    closeGeneric() {
        this.generic = false;
        this.activeIndexNumber = 0;
        this.changeDetectRef.detectChanges();
        this.documentsSelected = [];
    }

    converToUppercase(string) {
        if (string !== undefined) {
            return string.toUpperCase();
        }
    }


    viewFormDocument(docId) {
        console.log(docId);
        this.formDocumentId = docId;
        this.ngxSmartModalService.getModal('formDocumentModel').open();
    }

    focusElement(event) {
        // console.log(event);
        window.setTimeout(function () {
            document.getElementById(event.target.id).focus();
        }, 0);
    }

    changePageOfDocument(click) {
        const date = new Date();
        this.changePage = [click, date.getTime];
    }

    onSearch(evt) {
        const keyUp: String = evt.query;
        if (keyUp.length > 2) {
            if (this.responseSelect.roleId === -6) {
                this.us.searchSubordinateRoles(keyUp).subscribe(data => {
                    this.assinDBTypeAhead(data);
                }, error => { });
            } else {
                this
                    .us
                    .searchRoles(keyUp)
                    .subscribe(res => this.assinDBTypeAhead(res));
            }
        }
    }

    assinDBTypeAhead(res) {
        this.routeTodata = [];
        const result = JSON.parse(res._body);
        for (let index = 0; index < result.length; index++) {
            const element = {
                value: result[index].id,
                name: result[index].name
            };
            this.routeTodata.push(element);
        }
    }

    pageChangeEmitted(event) {
        this.spinnerService.show();
        // this.documnetAnnoation(event.pagenumber, localStorage.getItem('Default Annotation'), event);
        this.annotationType = localStorage.getItem('Default Annotation');
        this.AnnotationCurrentPage = event.page + 1;
        this.annotationDocId = event.docId;
        if (this.router.url.includes('inbox')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('sent')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('draft')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('register')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('archive')) {
            this.annotationDocumentType = 'readonly';
        } else {
            this.annotationDocumentType = 'annotate';
        }
        if (this.documnetAnnoationType === 'docs') {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            this.activeIndexNumber = this.docProps.length + 3;
        } else {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = 4;
        }

    }
    closeDocProps(doc) {
        this.docProps.splice(doc.index - 3, 1);
        this.activeIndexNumber = 1;
    }
    documentTypeChangeEmitted(event) {
        this.spinnerService.show();
        this.annotationType = event.annoatationType;
        this.AnnotationCurrentPage = event.pageno;
        this.annotationDocId = event.docId;
        if (this.router.url.includes('inbox')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('sent')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('draft')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('register')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('archive')) {
            this.annotationDocumentType = 'readonly';
        } else {
            this.annotationDocumentType = 'annotate';
        }
        if (this.documnetAnnoationType === 'docs') {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            this.activeIndexNumber = this.docProps.length + 3;
        } else {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = 4;
        }
    }
    documentPropertyTab(document, type, name) {
        // this.documentId = document.id;
        // console.log(document);
        if (localStorage.getItem('Default Document View') === 'properties') {
            this.onlyTabAnnotations = true;
        } else {
            if (this.router.url.includes('inbox')) {
                this.annotationDocumentType = 'annotate';
            } else if (this.router.url.includes('sent')) {
                this.annotationDocumentType = 'readonly';
            } else if (this.router.url.includes('draft')) {
                this.annotationDocumentType = 'annotate';
            } else if (this.router.url.includes('register')) {
                this.annotationDocumentType = 'readonly';
            } else if (this.router.url.includes('archive')) {
                this.annotationDocumentType = 'readonly';
            } else {
                this.annotationDocumentType = 'annotate';
            }
            this.onlyTabAnnotations = false;
        }

        let flag = 0;
        let activeIndex = 0;
        for (let index = 0; index < this.docProps.length; index++) {
            if (type === 'document' && this.docProps[index].id) {
                if (this.docProps[index].id === document.docId) {
                    flag = 1;
                    activeIndex = index + 1;
                    break;
                }
            } else if (type === 'form' && this.docProps[index].formObject) {
                if (this.docProps[index].formObject.id === document.id) {
                    flag = 1;
                    activeIndex = index + 1;
                    break;
                }
            }
        }
        if (this.docProps.length === 0) {
            this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = 1;
        }
        if (flag === 0) {
            if (type === 'form') {
                this.docProps.push({
                    id: undefined,
                    docTitle: name,
                    formObject: document,
                    title: name,
                    type: type,
                    icon: 'fa-info-circle'
                });
            } else if (type === 'document') {
                this.docProps.push({
                    id: document.docId,
                    docTitle: document.docTitle,
                    formObject: undefined,
                    title: name,
                    type: type,
                    icon: 'ui-icon-web-asset'
                });
            }
            this.propertyTabShow = true;
            this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = this.docProps.length + 2;
        } else {
            this.changeDetectorRef.detectChanges();
            this.activeIndexNumber = activeIndex + 2;
        }
        this.changeDetectorRef.detectChanges();
    }

    tabClosed(event) {
        // console.log(event);
    }

    setThisPrimary(item) {
        this.ws.setAsPrimaryDocument(this.typeId, item.docId).subscribe(data => this.tr.success('', 'Document set as primary document'));
    }

    documentAnnotationDocument(event, showComplete) {
        this.documnetAnnoationType = 'attachDocs';
        // this.documentTypeChangeEmitted(event);
        this.spinnerService.show();
        this.activityScreen = showComplete;
        this.annotationType = event.annoatationType;
        this.AnnotationCurrentPage = event.pagenumber;
        this.annotationDocId = event.docId;
        if (this.router.url.includes('inbox')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('sent')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('draft')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('register')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('archive')) {
            this.annotationDocumentType = 'readonly';
        } else {
            this.annotationDocumentType = 'annotate';
        }
        if (this.documnetAnnoationType === 'docs') {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            this.activeIndexNumber = this.docProps.length + 3;
        } else {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 4;
            this.activeIndexNumber = this.docProps.length + 3;
        }
    }
    parseInt(str) {
        const x = str;
        const y = +x; // y: number
        return y;
    }

    notification() {
        this.ws.sendPendingActivityNotifications(this.activityinfo.workId).subscribe(data => {
            this.tr.success('', 'Notification sent successfully');
        }, error => { });
    }
    showAnnotation(event) {
        this.annotationType = event.type;
        this.AnnotationCurrentPage = event.pageNo;
        if (this.router.url.includes('inbox')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('sent')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('draft')) {
            this.annotationDocumentType = 'annotate';
        } else if (this.router.url.includes('register')) {
            this.annotationDocumentType = 'readonly';
        } else if (this.router.url.includes('archive')) {
            this.annotationDocumentType = 'readonly';
        } else {
            this.annotationDocumentType = 'annotate';
        }
        this.annotationDocId = event.docId;
        if (this.documnetAnnoationType === 'docs') {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 3;
            this.activeIndexNumber = this.docProps.length + 3;
        } else {
            this.annotationPreviewTab = true;
            this.changeDetectorRef.detectChanges();
            // this.activeIndexNumber = 4;
            this.activeIndexNumber = this.docProps.length + 3;
        }
    }
    showPropsTab(event) {
        if (event === 'true') {
            this.onlyTabAnnotations = true;
        }
    }
    dbLookupDropdown(res, k) {
        this.responseSelectProps.properties[k].value = [];
        this.responseSelectProps.properties[k].value.push({
            name: res.label,
            value: res.value
        });
    }
    dateAndTime(event, isdd) {
        const d = new Date(Date.parse(event));
        let newDate;
        let date1;
        let date2;
        if (((d.getDate()).toString()).length < 2) {
            date1 = '0' + (d.getDate());
        } else {
            date1 = (d.getDate());
        }
        if (((d.getMonth() + 1).toString()).length < 2) {
            date2 = '0' + (d.getMonth() + 1);
        } else {
            date2 = (d.getMonth() + 1);
        }
        if (isdd === 'ddmmyy') {
            newDate = `${date1}/${date2}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
        } else {
            newDate = `${date2}/${date1}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
        }
        // const formControlName = this.formObject[0].sections[i].columns[j].properties[k].name;
        this.activityform.patchValue({ deadline: newDate });
    }
}

interface FileReaderEvent extends Event {
    target: FileReaderEventTarget;
    getMessage(): string;
}

interface FileReaderEventTarget extends EventTarget {
    result: string;
}
