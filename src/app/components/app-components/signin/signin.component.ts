import { UserService } from '../../../service/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { environment } from '../../../../environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  public form: FormGroup;
  errorMessage = false;
  signIn: boolean;
  private user: any = localStorage.getItem('user');
  public capsLock = false;
  public capsOn;
  public isLogin = true;
  public registerForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private userservice: UserService, private tr: ToastrService) {

  }

  ngOnInit() {
    if (localStorage.getItem('token') === undefined || localStorage.getItem('token') === null) {
      this.router.navigateByUrl('/');
    } else {
      this.router.navigateByUrl('/services');
    }
    this.signIn = false;
    this.form = this.fb.group({
      uname: [null, Validators.compose([Validators.required])],
      password: [null, Validators.compose([Validators.required])]
    });
    this.registerForm = this.fb.group({
      name: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')])],
      mobile: [null, Validators.compose([Validators.required, Validators.min(1000000000), Validators.max(9999999999)])]
    });
    if (environment.sso) {
      this.userservice.authenticateUser('test', 'test', 1).subscribe(data => this.singleSignOn(data), error => {
        document.getElementById('spinner').style.display = 'none';
      });
    } else {
      document.getElementById('spinner').style.display = 'none';
      // this.cs.getDeploymentType().subscribe(data => this.getDeploymentType(data));
    }
  }
  getDeploymentType(data) {
    if (data._body === 'CLOUD') {
      const emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    }
  }

  singleSignOn(data) {
    this.userservice.getUserDetails('qw').subscribe(datares => this.signInUser(datares));
  }

  onSubmit(form) {
    document.getElementById('spinner').style.display = 'block';
    const username = btoa(form.controls.uname.value);
    const password = btoa(form.controls.password.value);
    localStorage.clear();
    this.userservice.authenticateUser(username, password, 1).subscribe(data => {
      this.login(username, data);
    }, error => {
      this.signIn = true;
      document.getElementById('spinner').style.display = 'none';
      localStorage.removeItem('token');
    });
  }
  login(username, datares) {
    this.userservice.getUserDetails(username).subscribe(
      data => this.signInUser(data), error => {
        this.errorMessage = true;
        document.getElementById('spinner').style.display = 'none';
        localStorage.removeItem('token');
      });
  }
  onRegisterSubmit(registerform) {
    document.getElementById('spinner').style.display = 'block';
    const TUser = {
      userLogin: this.registerForm.value.email,
      fulName: this.registerForm.value.name,
      mail: this.registerForm.value.email,
      phoneNo: this.registerForm.value.mobile
    };
    // this.exa.isUserExisting(this.registerForm.value.email).subscribe(result => {
    //   this.compareResult(result, TUser);
    // }, error => {
    // document.getElementById('spinner').style.display ='none'
    // });
    console.log(TUser);
    document.getElementById('spinner').style.display = 'none';
  }
  compareResult(result, TUser) {
    if (result._body === 'YES') {
      this.tr.warning('', 'This email is already registered, you can signin now');
      document.getElementById('spinner').style.display = 'none';
      this.registerForm.reset();
    } else {
      // this.exa.saveUser(TUser).subscribe(data => {
      //   this.tr.success('Success', 'Welcome to Flametree');
      //   this.router.navigateByUrl('/hone');
      //   this.registerForm.reset();
      //   document.getElementById('spinner').style.display = 'none';
      // }, error => {
      //   this.registerForm.reset();
      //   document.getElementById('spinner').style.display = 'none';
      // });
    }
  }
  signTyped() {
    this.signIn = false;
  }

  signInUser(data) {
    if (data._body !== '') {
      this.user = JSON.parse(data._body);
      localStorage.setItem('user', JSON.stringify(this.user));
      if (this.user.settings !== undefined) {
        for (let index = 0; index < this.user.settings.length; index++) {
          localStorage.setItem(this.user.settings[index].key, this.user.settings[index].val);
        }
      }
      this.router.navigateByUrl(`/services`);
    } else {
      this.signIn = true;
      document.getElementById('spinner').style.display = 'none';
    }
  }


  detectCapsLock(event) {
    if (event.getModifierState('CapsLock')) {
      this.capsLock = true;
    } else {
      this.capsLock = false;
    }
  }
  clikcedOnRegister() {
    this.isLogin = false;
  }
  clickedOnSignin() {
    this.isLogin = true;
  }
}
