import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AdminLayoutComponent } from '../layout/admin-layout/admin-layout.component';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { Router } from '@angular/router';
import { WorkService } from '../../../service/work.service';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css']
})
export class ServicesComponent implements OnInit {
  public services = [];
  public historyItems = [];
  constructor(public al: AdminLayoutComponent, private changed: ChangeDetectorRef, private breadCrumb: BreadcrumbService,
    private ws: WorkService, private router: Router, private modalService: NgxSmartModalService, private spinner: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      this.changed.detectChanges();
      this.al.selected = 'services';
    }, 0);
    this.ws.getUserWorkItems('ALL', 'ALL').subscribe(data => { this.dynamicItems(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
    this.breadCrumb.setItems([{
      label: 'My Tickets'
    }]);
  }
  dynamicItems(data) {
    this.services = [];
    if (data._body) {
      this.services = JSON.parse(data._body);
    }
  }
  openCreate(service) {
    this.router.navigate(['/create'], { queryParams: { 'id': service.initActivityType, 'typeId': service.id, 'multtiFromSupport': service.multiFormSupport, 'subjectProps': service.subjectProps } });

  }
  lookupRowStyleClass() {
  }
  editTickets(event) {
    this.spinner.show();
    this.ws.getAcitvityHistory(event.data.id).subscribe(data => { this.showHistory(data); this.spinner.hide(); }, error => { this.spinner.hide(); });
  }
  showHistory(data) {
    this.modalService.getModal('historyAlert').open();
    this.historyItems = JSON.parse(data._body);
  }
}
