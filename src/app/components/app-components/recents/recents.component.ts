import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { AdminLayoutComponent } from '../layout/admin-layout/admin-layout.component';
import { ContentService } from '../../../service/content.service';
import { DocumentService } from '../../../service/document.service';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-recents',
  templateUrl: './recents.component.html',
  styleUrls: ['./recents.component.css']
})
export class RecentsComponent implements OnInit {
public recentItems = [];
  constructor(private breadCrumb: BreadcrumbService, private changed: ChangeDetectorRef, public al: AdminLayoutComponent,
    private cs: ContentService, private ds: DocumentService) { }

  ngOnInit() {
    this.breadCrumb.setItems([{
      label: 'My Documents'
  }]);
  this.cs.getRecent(1).subscribe(data => this.recentResult(data));
  setTimeout(() => {
    this.al.selected = 'recents';
  }, 100);
  }
  recentResult(data) {
    this.recentItems = [];
    if (data._body) {
      this.recentItems = JSON.parse(data._body);
    }
  }
  download(id) {
    this.ds.downloadDocument(id).subscribe(data => this.downloadCurrentDocument(data) );
  }
  downloadCurrentDocument(data) {
    let filename = '';
      const disposition = data.headers.get('Content-Disposition');
      const filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        const matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
        saveAs(data._body, filename);
  }
}
