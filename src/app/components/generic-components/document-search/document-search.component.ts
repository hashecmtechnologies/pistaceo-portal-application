import { Component, OnInit, OnChanges, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '../../../../../node_modules/@angular/forms';
import { ActivatedRoute, Router } from '../../../../../node_modules/@angular/router';
import { ContentService } from '../../../service/content.service';
import { DocumentService } from '../../../service/document.service';
import { UserService } from '../../../service/user.service';
import { SchemaService } from '../../../service/schema.service';
import { WorkService } from '../../../service/work.service';
import { TranslateService } from '../../../../../node_modules/@ngx-translate/core';
import * as operators from '../../../operators.variables';

@Component({
  selector: 'app-document-search',
  templateUrl: './document-search.component.html',
  styleUrls: ['./document-search.component.css']
})
export class DocumentSearchComponent implements OnInit, OnChanges {
  @ViewChild('paccordion') paccordion: ElementRef;
@Output() fileList = new EventEmitter();
public isDocumentCollapse = true;
public documentsearch: FormGroup;
public isDocumentResult = false;
public documentClassToshow = [];
public documentClass = [];
public docClassSymName:  any;
public docClassName: any;
public documentClassProp = [];
public searchedDocument = [];
public searchResultDiv = true;
public dateDropdown = [];
public stringDropdown = [];
public numberDropdown = [];
public downloadDocList = [];
public documentChecked;
public  propertyChecked = false;
public isCheckOut = false;
public isCheckIn = false;
public ddmmyy = true;
  constructor(private fb: FormBuilder, private route: ActivatedRoute, private cs: ContentService,
    private ds: DocumentService, private us: UserService, private ss: SchemaService, private ws: WorkService,
     private router: Router, private translate: TranslateService) {
      if(localStorage.getItem('Date Format') === 'DD/MM/YYYY'){
        this.ddmmyy = true;
    }else{
        this.ddmmyy = false;
    }
    this.ddmmyy = true;
      const browserLang: string = translate.getBrowserLang();
      translate.use('en');
  translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
     }

  ngOnInit() {
    this.dateDropdown = operators.dateOperator;
    this.stringDropdown = operators.stringOperator;
    this.numberDropdown = operators.stringOperator;
    this.documentsearch = this.fb.group({
      documentClass: [null],
      documentname: [],
      documentid: [null],
      documenttitle: [null],
      documentcreateddate: [null],
      documentcreatedby: [null]
    });
    this.cs.getDocumentClasses().subscribe(data => this.documentClasses(data));
  }
  ngOnChanges() {
    this.documentChecked = false;
  }
  documentClasses(data) {
    if (data._body !== '') {
      this.documentClassToshow = [];
      this.documentClass = JSON.parse(data._body);
    if (this.documentClass.length > 0) {
      for (let doc = 0 ; doc < this.documentClass.length; doc++) {
        let value;
        value = {
          label: this.documentClass[doc].name,
          value: this.documentClass[doc].symName
        };
        this.documentClassToshow.push(value);
     }
        this.documentsearch.patchValue({
          documentname:  this.documentClass[0].id
        });
        this.docClassSymName = this.documentClass[0].symName;
        this.docClassName = this.documentClass[0].name;
       this.documentClassProp =  this.documentClass[0].props;
       for (let index = 0; index < this.documentClassProp.length; index++) {
          const control: FormControl = new FormControl(null, Validators.required);
          this.documentsearch.addControl(this.documentClassProp[index].symName, control);
     }
      }
    }

  }
  documentClassChanged(event) {
    for (const prop of this.documentClass) {
      if (prop.symName === event.value.value) {
        this.documentClassProp = prop.props;
        this.docClassSymName = event.value.value;
        this.docClassName = prop.name;
      }
    }
    for (let index = 0; index < this.documentClassProp.length; index++) {
      const control: FormControl = new FormControl(null, Validators.required);
      this.documentsearch.addControl(this.documentClassProp[index].symName, control);
 }
  }
  documentSubmit(event) {
    let name = '';
    let symName = '';
    for (const selectField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
      if (selectField.id !== undefined && selectField.id === 'docClass') {
         symName = this.docClassSymName;
         name = this.docClassName;
      }
    }
    const search = {
      'name': name, 'symName': symName, 'type': 'DOCUMENT', 'props': [],
      'contentSearch': {'name': 'Content', 'symName': 'CONTENT', 'dtype': 'STRING', 'mvalues': [], 'oper': ''}
    };
     for (const inputField of [].slice.call(event.target)) {
       for (const selectField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
          if (inputField.getAttribute('type') !== 'file') {
            for (const docClass of this.documentClass) {
               for (const prop of docClass.props) {
                 if (symName === docClass.symName) {
                    if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined && selectField.id === prop.symName) {
                      const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.value], 'oper': selectField.children['0'].children[1].innerText };
                         search.props.push(property);
                    }
                 }
               }
            }
          }
       }
    }

    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
       for (const selectField of [].slice.call(event.target.getElementsByTagName('p-dropdown'))) {
          if (inputField.getAttribute('type') !== 'file') {
            for (const docClass of this.documentClass) {
               for (const prop of docClass.props) {
                 if (symName === docClass.symName) {
                    if (inputField.id !== undefined && inputField.id === prop.symName && selectField.id !== undefined && selectField.id === prop.symName) {
                      const property = {'symName': prop.symName, 'dtype': prop.dtype, 'mvalues': [inputField.children['0'].children['0'].value], 'oper': selectField.children['0'].children[1].innerText };
                         search.props.push(property);
                    }
                 }
               }
            }
          }
       }
    }

   this.searchedDocument = [];

    this.cs.searchDocuments(search).subscribe(data => this.getsearchedDocuments(data) );
    this.isDocumentCollapse = false;
  }
  resetDocument(document) {
    this.documentsearch.reset();
    this.documentClassProp = [];
    this.docClassSymName = this.documentClass[0].symName;
    this.docClassName = this.documentClass[0].name;
   this.documentClassProp =  this.documentClass[0].props;
   for (let index = 0; index < this.documentClassProp.length; index++) {
    const control: FormControl = new FormControl(null, Validators.required);
    this.documentsearch.addControl(this.documentClassProp[index].symName, control);
}
  }
  getsearchedDocuments(data) {
    this.searchedDocument = data;
//   for (let doc = 0; doc < value.length; doc++) {
// this.searchedDocument.push(value[doc].props[0]);
//   }
 // this.searchedDocument = value;

  this.searchResultDiv = false;
  this.isDocumentCollapse = false;
  this.isDocumentResult = true;
}
documentCheck(event , doc) {
  const val = {
    docId: doc.id,
    name: doc.props['0'].mvalues['0'],
    format: doc.format,
    isReserved: doc.isReserved
  };
  if (event) {
    this.downloadDocList.push(val);
   if ( this.downloadDocList.length > 0) {
    this.documentChecked = true;
   }

// property
   if (this.downloadDocList.length > 1) {
    this.propertyChecked = true;
  }else {
    this.propertyChecked = false;
  }

 let count1 = 0;
 let count2 = 0;
    for (let i = 0 ; i <  this.downloadDocList.length ; i++) {
      if (this.downloadDocList[i].isReserved === true) {
       count1++;
      }else {
        count2++;
      }
    }

    if (count1 > 0 && count2 > 0) {
      this.isCheckIn = false;
      this.isCheckOut = false;
    }else if (count1 > 0 && count2 === 0) {
      this.isCheckIn = true;
      this.isCheckOut = false;
    }else if (count1 === 0 && count2 > 0) {
      this.isCheckOut = true;
      this.isCheckIn = false;
    }else {
    }
  } else {
   for (let i = 0 ; i < this.downloadDocList.length ; i++) {
    if (this.downloadDocList[i].docId === doc.id) {
      this.downloadDocList.splice(i , 1);
    }
   }
    if ( this.downloadDocList.length > 0) {
      this.documentChecked = true;
     }else {
      this.documentChecked = false;
     }
    // property
         if (this.downloadDocList.length > 1) {
          this.propertyChecked = true;
        }else {
          this.propertyChecked = false;
        }
       let count1 = 0;
       let count2 = 0;
          for (let i = 0 ; i <  this.downloadDocList.length ; i++) {
            if (this.downloadDocList[i].isReserved === true) {
             count1++;
            }else {
              count2++;
            }
          }
          if (count1 > 0 && count2 > 0) {
            this.isCheckIn = false;
            this.isCheckOut = false;
          }else if (count1 > 0 && count2 === 0) {
            this.isCheckIn = true;
            this.isCheckOut = false;
          }else if (count1 === 0 && count2 > 0) {
            this.isCheckOut = true;
            this.isCheckIn = false;
          }else {
          }
        }

        this.fileList.emit(this.downloadDocList);
 }

 accordionClose(event) {
   this.isDocumentCollapse = true;
 }
}
