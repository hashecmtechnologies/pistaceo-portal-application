import { Component, OnInit, Input, OnChanges, DoCheck, Output, EventEmitter, AfterViewInit, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { IntegrationService } from '../../../service/integration.service';
import { UserService } from '../../../service/user.service';
import { findIndex } from 'rxjs/operator/findIndex';
import { Row } from 'primeng/primeng';
import { DummyTable } from '../../../models/dummy-table.model';
import { FileUploader, FileItem, FileLikeObject } from 'ng2-file-upload';
import { ContentService } from '../../../service/content.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { DocumentService } from '../../../service/document.service';
import { ToastrService } from 'ngx-toastr';
// declare var require: any;
@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.css']
})
export class DynamicFormComponent implements OnChanges, OnInit, DoCheck {

  @ViewChild('addDocument') addDoc: ElementRef;
  @Input() dynamicFormJSON: any;
  @Input() createformtitle: any;
  @Input() activitytype;
  @Input() readOnly;
  @Input() activitytypeName;
  @Output() formJSONOutput = new EventEmitter();
  @Output() formCancel = new EventEmitter();
  @Output() tempAttachmentsArray = new EventEmitter();

  public formObject;
  public dynamicform: FormGroup;
  public formObjectSubmit;
  public ddmmyy = true;
  public submitButton = false;
  public tableRowValue = 0;
  public footercalled = false;
  public isRtl = false;
  public tempFileobject = new FileUploader({});
  public globelFileUploder = new FileUploader({});
  public recentDocumentList = [];
  public documentsSelected = [];
  public websocketNotOpen = false;
  public title: 'ScanedImage';
  public msgs = [];
  public tempAttachments = [];
  public selectedObject: any;
  public formData;
  public attachfilestabshow = false;
  public selectedTab = 1;
  public systemDate = new Date();
  public documentClass;
  public documentClassProp;
  public documentAdd: FormGroup;
  public docClassSymName;
  public docClassName;
  public attachmentfileName = null;
  public attachmentfile;
  public selectedClassItem;
  public docProperty;
  public formsButtonsList;
  public duplicateActivity;
  constructor(private fb: FormBuilder, private integrationservice: IntegrationService, private us: UserService,
    private detectChanged: ChangeDetectorRef, private cs: ContentService, private ngxSmartModalService: NgxSmartModalService,
    private spinner: Ng4LoadingSpinnerService, private ds: DocumentService, private tr: ToastrService, private changeDetectRef: ChangeDetectorRef) {
    if (localStorage.getItem('Date Format') === 'DD/MM/YYYY') {
      this.ddmmyy = true;
    } else {
      this.ddmmyy = false;
    }
    this.ddmmyy = true;
  }


  ngOnInit() {
    this.footercalled = false;
    this.tableRowValue = 0;
  }
  ngOnChanges() {
    // const data = require('./../../../../assets/others/test.json');
    // this.dynamicFormJSON = JSON.stringify(data);
    if (localStorage.getItem('Default Language') === 'ar') {
      this.isRtl = true;
    } else {
      this.isRtl = false;
    }
    this.dynamicform = this.fb.group({});
    this.formObjectSubmit = [];
    this.duplicateActivity = this.activitytype;
    if (JSON.parse(this.dynamicFormJSON.length) > 0) {
      const formData = [];
      formData.push(JSON.parse(this.dynamicFormJSON));
      this.formObjectSubmit.push(JSON.parse(this.dynamicFormJSON));
      for (let index = 0; index < this.formObjectSubmit.length; index++) {
        for (let i = 0; i < this.formObjectSubmit[index].sections.length; i++) {
          if (this.formObjectSubmit[index].sections[i].type === 'FORM') {
            for (let j = 0; j < this.formObjectSubmit[index].sections[i].columns.length; j++) {
              for (let k = 0; k < this.formObjectSubmit[index].sections[i].columns[j].properties.length; k++) {
                if (formData[index].sections[i].columns[j].properties[k].uitype.uitype === 'STEXTTA' || formData[index].sections[i].columns[j].properties[k].uitype.uitype === 'MTEXTTA' || formData[index].sections[i].columns[j].properties[k].uitype.uitype === 'DBLOOKUP') {
                  formData[index].sections[i].columns[j].properties[k].dbValue = [];
                  formData[index].sections[i].columns[j].properties[k].dbDummyValue = [];
                }
                if (formData[index].sections[i].columns[j].properties[k].uitype.uitype === 'DBLOOKUP') {
                  formData[index].sections[i].columns[j].properties[k].lookupOptions = [];
                  this.integrationservice.getDBLookup(formData[index].sections[i].columns[j].properties[k].uitype.dblookup).subscribe(res => this.assignDblookupvalue(res, index, i, j, k));
                }
                if (formData[index].sections[i].columns[j].properties[k].uitype.uitype === 'LOOKUP') {
                  formData[index].sections[i].columns[j].properties[k].lookupOptions = [];
                  if (formData[index].sections[i].columns[j].properties[k].value.length === 0) {
                    formData[index].sections[i].columns[j].properties[k].lookupOptions = [];
                    const select = {
                      name: formData[index].sections[i].columns[j].properties[k].uitype.lookups[0].name,
                      value: formData[index].sections[i].columns[j].properties[k].uitype.lookups[0].value
                    };
                    formData[index].sections[i].columns[j].properties[k].value.push(select);
                  }
                }
                if (formData[index].sections[i].columns[j].properties[k].uitype.uitype === 'LOOKUPCONDITION') {
                  formData[index].sections[i].columns[j].properties[k].lookupOptions = [];
                  if (formData[index].sections[i].columns[j].properties[k].value.length === 0) {
                    // formData[index].sections[i].columns[j].properties[k].lookupOptions = [];
                    const select = {
                      name: formData[index].sections[i].columns[j].properties[k].uitype.lookups[0].name,
                      value: formData[index].sections[i].columns[j].properties[k].uitype.lookups[0].value
                    };
                    formData[index].sections[i].columns[j].properties[k].value.push(select);
                  }
                }

                if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].uitype.uitype === 'MTEXTTA') {
                  formData[index].sections[i].columns[j].properties[k].dbDummyValue = [];
                  formData[index].sections[i].columns[j].properties[k].dbValue = [];
                  if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].value.length === 0) {
                    formData[index].sections[i].columns[j].properties[k].value = [];
                  } else {
                    for (let l = 0; l < this.formObjectSubmit[index].sections[i].columns[j].properties[k].value.length; l++) {
                      const element = {
                        'value': this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[l].value,
                        'name': this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[l].name
                      };
                      formData[index].sections[i].columns[j].properties[k].dbDummyValue.push(element);
                    }
                  }

                } else if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].uitype.uitype === 'STEXTTA') {
                  formData[index].sections[i].columns[j].properties[k].dbDummyValue = [];
                  formData[index].sections[i].columns[j].properties[k].dbValue = [];
                  if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].value.length === 0) {
                    formData[index].sections[i].columns[j].properties[k].value = [];
                    if (this.createformtitle === 'Memo' || this.createformtitle === 'Outgoing Correspondence' || this.createformtitle === 'Outing Correspondence') {
                      if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].name === 'from') {
                        const element = {
                          'value': this.us.getCurrentUser().roles[0].id,
                          'name': this.us.getCurrentUser().roles[0].name
                        };
                        formData[index].sections[i].columns[j].properties[k].dbDummyValue = element;
                        formData[index].sections[i].columns[j].properties[k].value.push(element);
                      }
                    }
                  } else {
                    for (let l = 0; l < this.formObjectSubmit[index].sections[i].columns[j].properties[k].value.length; l++) {
                      const element = {
                        'value': this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[l].value,
                        'name': this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[l].name
                      };
                      formData[index].sections[i].columns[j].properties[k].dbDummyValue = element;
                    }
                  }
                } else if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].uitype.uitype === 'DBLOOKUP') {
                  formData[index].sections[i].columns[j].properties[k].dbDummyValue = [];
                  formData[index].sections[i].columns[j].properties[k].dbValue = [];
                  if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].value === '') {
                    formData[index].sections[i].columns[j].properties[k].value = [];
                  } else {
                    for (let l = 0; l < this.formObjectSubmit[index].sections[i].columns[j].properties[k].value.length; l++) {
                      const element = {
                        'id': this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[l].value,
                        'itemName': this.formObjectSubmit[index].sections[i].columns[j].properties[k].value[l].name
                      };
                      formData[index].sections[i].columns[j].properties[k].dbDummyValue.push(element);
                    }
                  }
                } else if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].uitype.uitype === 'LOOKUP') {
                  this.formObjectSubmit[index].sections[i].columns[j].properties[k].lookupOptions = [];
                  for (const options of this.formObjectSubmit[index].sections[i].columns[j].properties[k].uitype.lookups) {
                    const lookup = {
                      label: options.name,
                      value: options.value
                    };
                    formData[index].sections[i].columns[j].properties[k].lookupOptions.push(lookup);
                  }
                } else if (this.formObjectSubmit[index].sections[i].columns[j].properties[k].uitype.uitype === 'LOOKUPCONDITION') {
                  this.formObjectSubmit[index].sections[i].columns[j].properties[k].lookupOptions = [];
                  for (const options of this.formObjectSubmit[index].sections[i].columns[j].properties[k].uitype.lookups) {
                    const lookup = {
                      label: options.name,
                      value: options.value
                    };
                    formData[index].sections[i].columns[j].properties[k].lookupOptions.push(lookup);
                  }
                }

              }
            }
          } else if (formData[index].sections[i].type === 'TABLE') {
            for (let tj = 0; tj < formData[index].sections[i].rows.length; tj++) {
              // this.tableRowValue = this.tableRowValue + formData[index].sections[i].rows.length;
              // formData[index].sections[i].rows[tj].row = this.tableRowValue;
              for (let k = 0; k < formData[index].sections[i].rows[tj].items.length; k++) {
                formData[index].sections[i].rows[tj].items[k].rOnly = formData[index].sections[i].rowheader[k].rOnly;
                formData[index].sections[i].rows[tj].items[k].req = formData[index].sections[i].rowheader[k].req;
                formData[index].sections[i].rows[tj].items[k].name = formData[index].sections[i].rowheader[k].name;
                formData[index].sections[i].rows[tj].items[k].label = formData[index].sections[i].rowheader[k].label;
                formData[index].sections[i].rows[tj].items[k].label = formData[index].sections[i].rowheader[k].label;
                formData[index].sections[i].rows[tj].items[k].type = formData[index].sections[i].rowheader[k].type;
                formData[index].sections[i].rows[tj].items[k].uitype.uitype = formData[index].sections[i].rowheader[k].uitype.uitype;
                formData[index].sections[i].rows[tj].items[k].length = formData[index].sections[i].rowheader[k].length;
                if (formData[index].sections[i].rowheader[k].uitype.uitype === 'LOOKUP') {
                  formData[index].sections[i].rows[tj].items[k].lookup = formData[index].sections[i].rowheader[k].uitype.lookups;
                  formData[index].sections[i].rows[tj].items[k].lookupOptions = [];
                  if (formData[index].sections[i].rows[tj].items[k].value.length === 0) {
                    formData[index].sections[i].rows[tj].items[k].lookupOptions = [];
                    const select = {
                      name: formData[index].sections[i].rowheader[k].uitype.lookups[0].name,
                      value: formData[index].sections[i].rowheader[k].uitype.lookups[0].value
                    };
                    formData[index].sections[i].rows[tj].items[k].value.push(select);
                    for (const options of formData[index].sections[i].rowheader[k].uitype.lookups) {
                      const lookupValue = {
                        label: options.name,
                        value: options.value
                      };
                      formData[index].sections[i].rows[tj].items[k].lookupOptions.push(lookupValue);
                    }
                  } else {
                    formData[index].sections[i].rows[tj].items[k].lookupOptions = [];
                    const select = {
                      name: formData[index].sections[i].rows[tj].items[k].value[0].name,
                      value: formData[index].sections[i].rows[tj].items[k].value[0].value
                    };
                    formData[index].sections[i].rows[tj].items[k].value = [];
                    formData[index].sections[i].rows[tj].items[k].value.push(select);
                    for (const options of formData[index].sections[i].rowheader[k].uitype.lookups) {
                      const lookupValue = {
                        label: options.name,
                        value: options.value
                      };
                      formData[index].sections[i].rows[tj].items[k].lookupOptions.push(lookupValue);
                    }
                  }
                }
                if (formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'STEXTTA' || formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'MTEXTTA' || formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'DBLOOKUP') {
                  formData[index].sections[i].rows[tj].items[k].dbValue = [];
                  formData[index].sections[i].rows[tj].items[k].dbDummyValue = [];
                }
                if (formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'DBLOOKUP') {
                  formData[index].sections[i].rows[tj].items[k].lookupOptions = [];
                  this.integrationservice.getDBLookup(formData[index].sections[i].rows[tj].items[k].uitype.dblookup).subscribe(res => this.assignDblookupvalueTable(res, index, i, tj, k));
                }
                if (formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'ROWSUM' || formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'MULTIPLICATION' || formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'ROWAVG' ||
                  formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'ROWDATEDIFF' || formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'ROWSUB' || formData[index].sections[i].rows[tj].items[k].uitype.uitype === 'ROWDIV') {
                  formData[index].sections[i].rows[tj].items[k].dbValue = [];
                  formData[index].sections[i].rows[tj].items[k].dbDummyValue = [];

                } if (this.formObjectSubmit[index].sections[i].rows[tj].items[k].uitype.uitype === 'DBLOOKUP') {
                  formData[index].sections[i].rows[tj].items[k].dbDummyValue = [];
                  formData[index].sections[i].rows[tj].items[k].dbValue = [];
                  if (this.formObjectSubmit[index].sections[i].rows[tj].items[k].value === '') {
                    formData[index].sections[i].rows[tj].items[k].value = [];
                  } else {
                    for (let l = 0; l < this.formObjectSubmit[index].sections[i].rows[tj].items[k].value.length; l++) {
                      const element = {
                        'id': this.formObjectSubmit[index].sections[i].rows[tj].items[k].value[l].value,
                        'itemName': this.formObjectSubmit[index].sections[i].rows[tj].items[k].value[l].name
                      };
                      formData[index].sections[i].rows[tj].items[k].dbDummyValue.push(element);
                    }
                  }
                }

                // if (this.formObjectSubmit[index].sections[i].rows[tj].items[k].uitype.uitype === 'LOOKUP') {
                //   this.formObjectSubmit[index].sections[i].rows[tj].items[k].lookupOptions = [];
                //   for (const options of this.formObjectSubmit[index].sections[i].rows[tj].items[k].uitype.lookups) {
                //     const lookup = {
                //       label: options.name,
                //       value: options.value
                //     };
                //     formData[index].sections[i].rows[tj].items[k].lookupOptions.push(lookup);
                //   }
                // }
                if (this.formObjectSubmit[index].sections[i].rows[tj].items[k].uitype.uitype === 'STEXTTA') {
                  formData[index].sections[i].rows[tj].items[k].dbDummyValue = [];
                  formData[index].sections[i].rows[tj].items[k].dbValue = [];
                  if (this.formObjectSubmit[index].sections[i].rows[tj].items[k].value.length === 0) {
                    formData[index].sections[i].rows[tj].items[k].value = [];
                    if (this.createformtitle === 'Memo' || this.createformtitle === 'Outgoing Correspondence' || this.createformtitle === 'Outing Correspondence') {
                      if (this.formObjectSubmit[index].sections[i].rows[tj].items[k].name === 'from') {
                        const element = {
                          'value': this.us.getCurrentUser().roles[0].id,
                          'name': this.us.getCurrentUser().roles[0].name
                        };
                        formData[index].sections[i].rows[tj].items[k].dbDummyValue = element;
                        formData[index].sections[i].rows[tj].items[k].value.push(element);
                      }
                    }
                  } else {
                    for (let l = 0; l < this.formObjectSubmit[index].sections[i].rows[tj].items[k].value.length; l++) {
                      const element = {
                        'value': this.formObjectSubmit[index].sections[i].rows[tj].items[k].value[l].value,
                        'name': this.formObjectSubmit[index].sections[i].rows[tj].items[k].value[l].name
                      };
                      formData[index].sections[i].rows[tj].items[k].dbDummyValue = element;
                    }
                  }
                }
              }
            }
          }
        }
      }
      this.formObject = formData;
      let flag = 0;
      for (const docClass of this.formObject) {
        docClass.sections.forEach((element, formObjectSecIndex) => {
          if (element.type === 'FORM') {
            if (element.visible === 'TRUE' || element.visible === undefined) {
              if (element.rOnly === 'FALSE' || element.rOnly === undefined) {
                for (const cols of element.columns) {
                  for (const props of cols.properties) {
                    if (props.req === 'TRUE') {
                      const control: FormControl = new FormControl(null, Validators.required);
                      this.dynamicform.addControl(props.name, control);
                    } else {
                      const control: FormControl = new FormControl(null);
                      this.dynamicform.addControl(props.name, control);
                    }
                    if (props.rOnly === 'TRUE') {
                      this.dynamicform.controls[props.name].disable();
                    }
                    if (props.visible === 'FALSE') {
                      this.dynamicform.controls[props.name].disable();
                    }
                  }
                }
              } else {
                for (const cols of element.columns) {
                  for (const props of cols.properties) {
                    const control: FormControl = new FormControl(null);
                    this.dynamicform.addControl(props.name, control);
                    this.dynamicform.controls[props.name].disable();

                  }
                }
              }
            } else {
              for (const cols of element.columns) {
                for (const props of cols.properties) {
                  const control: FormControl = new FormControl(null);
                  this.dynamicform.addControl(props.name, control);
                  this.dynamicform.controls[props.name].disable();

                }
              }
            }
          } else if (element.type === 'TABLE') {
            for (let iTable = 0; iTable < element.rowheader.length; iTable++) {
              for (let jTable = 0; jTable < element.rows.length; jTable++) {
                for (let kTable = 0; kTable < element.rows[jTable].items.length; kTable++) {
                  if (element.rowheader[iTable].name === element.rows[jTable].items[kTable].name) {
                    const formControlName = element.rowheader[iTable].name + element.rows[jTable].row;
                    if (element.rowheader[iTable].req === 'TRUE') {
                      const control: FormControl = new FormControl(null, Validators.required);
                      this.dynamicform.addControl(formControlName, control);
                    } else {
                      const control: FormControl = new FormControl(null);
                      this.dynamicform.addControl(formControlName, control);
                    }
                    if (element.rowheader[iTable].rOnly === 'TRUE' || (element.rows[jTable].row === (9090 + formObjectSecIndex))) {
                      this.dynamicform.controls[formControlName].disable();
                    } else {
                      if (element.rOnly === 'TRUE') {
                        this.dynamicform.controls[formControlName].disable();
                      } else {
                      }
                    }
                    if (element.rowheader[iTable].visible === 'FALSE') {
                      this.dynamicform.controls[formControlName].disable();
                    } else {
                    }


                  }
                }
              }
            }
          } else {

          }

        });
      }
      for (let sceindex = 0; sceindex < this.formObjectSubmit[0].sections.length; sceindex++) {
        if (this.formObjectSubmit[0].sections[sceindex].type === 'FORM') {
          if (this.formObjectSubmit[0].sections[sceindex].visible === 'TRUE' || this.formObjectSubmit[0].sections[sceindex].visible === undefined) {
            if (this.formObjectSubmit[0].sections[sceindex].rOnly === 'FALSE' || this.formObjectSubmit[0].sections[sceindex].rOnly === undefined) {
              for (let col = 0; col < this.formObjectSubmit[0].sections[sceindex].columns.length; col++) {
                for (let prop = 0; prop < this.formObjectSubmit[0].sections[sceindex].columns[col].properties.length; prop++) {
                  if (this.formObjectSubmit[0].sections[sceindex].columns[col].properties[prop].visible === 'TRUE' || this.formObjectSubmit[0].sections[sceindex].columns[col].properties[prop].visible === undefined) {
                    if (this.formObjectSubmit[0].sections[sceindex].columns[col].properties[prop].rOnly === 'TRUE') {
                      // readOnlyProps++;
                      flag = 0;
                    } else {
                      flag = 1;
                      break;
                    }
                    // overAllProps++;
                  }
                }
              }
              if (flag === 1) {
                break;
              }
            }
          }
        } else {
          for (let iTable = 0; iTable < this.formObjectSubmit[0].sections[sceindex].rowheader.length; iTable++) {
            for (let jTable = 0; jTable < this.formObjectSubmit[0].sections[sceindex].rows.length; jTable++) {
              for (let kTable = 0; kTable < this.formObjectSubmit[0].sections[sceindex].rows[jTable].items.length; kTable++) {
                if (this.formObjectSubmit[0].sections[sceindex].rowheader[iTable].name === this.formObjectSubmit[0].sections[sceindex].rows[jTable].items[kTable].name) {
                  if (this.formObjectSubmit[0].sections[sceindex].rOnly === 'FALSE') {
                    if (this.formObjectSubmit[0].sections[sceindex].rowheader[iTable].rOnly === 'TRUE') {
                      flag = 0;
                    } else {
                      flag = 1;
                      break;
                    }
                    if (this.formObjectSubmit[0].sections[sceindex].rowheader[iTable].visible === 'FALSE' || this.formObjectSubmit[0].sections[sceindex].rowheader[iTable].visible === undefined) {
                      flag = 0;
                    } else {
                      flag = 1;
                      break;
                    }
                  } else {
                    flag = 1;
                    break;
                  }

                }
              }
            }
            if (flag === 1) {
              break;
            }
          }
        }
      }
      if (flag === 1) {
        this.submitButton = true;
      } else {
        this.submitButton = false;

      }

      for (const docClass of this.formObject) {
        docClass.sections.forEach((element, sectionIndex) => {
          if (element.type === 'FORM') {
            for (const cols of element.columns) {
              for (const props of cols.properties) {
                if (props.uitype.uitype === 'TEXT' && props.name) {
                  // console.log(props.name);
                  if (props.value.length !== 0) {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  } else {
                    props.value = [];
                    if (props.req !== 'TRUE') {
                      const control: FormControl = new FormControl(null);
                      this.dynamicform.addControl(props.name, control);
                    } else {
                      const control: FormControl = new FormControl(null, Validators.required);
                      this.dynamicform.addControl(props.name, control);
                    }
                    this.dynamicform.controls[props.name].patchValue(props.value);
                  }
                } else if (props.uitype.uitype === 'ROWDATEDIFF') {
                  if (props.value.length === 0) {
                    this.dynamicform.controls[props.name].patchValue(0);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  }
                } else if (props.uitype.uitype === 'TEXTAF') {
                  if (props.value.length !== 0) {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.value);
                  }
                } else if (props.uitype.uitype === 'LOOKUP') {
                  if (props.value.length !== 0) {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.uitype.lookups[0].value);
                  }

                } else if (props.uitype.uitype === 'LOOKUPCONDITION') {
                  if (props.value.length !== 0) {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.uitype.lookups[0].value);
                  }
                  if (props.uitype.condition !== undefined) {
                    for (let index = 0; index < props.uitype.condition.length; index++) {
                      if (props.uitype.condition[index].val === this.dynamicform.controls[props.name].value) {
                        if (props.uitype.condition[index].show !== undefined) {
                          for (let show = 0; show < props.uitype.condition[index].show.length; show++) {
                            this.showPropsOnScreen(props.uitype.condition[index].show[show]);
                          }
                        }
                        if (props.uitype.condition[index].hide !== undefined) {
                          for (let hide = 0; hide < props.uitype.condition[index].hide.length; hide++) {
                            this.hidePropsOnScreen(props.uitype.condition[index].hide[hide]);
                          }
                        }
                      }
                    }
                  }
                } else if (props.uitype.uitype === 'ROWSUM' || props.uitype.uitype === 'MULTIPLICATION' || props.uitype.uitype === 'ROWAVG' ||
                  props.uitype.uitype === 'ROWSUB' || props.uitype.uitype === 'ROWDIV') {
                  if (props.value.length === 0) {
                    this.dynamicform.controls[props.name].patchValue(null);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  }
                } else if (props.uitype.uitype === 'TIME') {
                  if (props.value.length === 0) {
                    // const date = new Date();
                    // let hours = date.getHours();
                    // let minutes = date.getMinutes();
                    // let seconds = date.getSeconds();
                    // const todays = date.getTime();
                    // this.dynamicform.controls[props.name].patchValue('06:00:00');
                  } else {
                    this.dynamicform.controls[props.name].patchValue(new Date());
                  }
                } else if (props.uitype.uitype === 'DATE') {
                  if (props.value.length === 0) {
                    const today = new Date(); // needs to be resolved helplinks
                    // https://github.com/kekeh/mydatepicker/blob/master/sampleapp/sample-date-picker-access-modifier/sample-date-picker-access-modifier.html
                    const dd = today.getDate();
                    const mm = (today.getMonth() + 1); // January is 0!
                    let date1;
                    let date2;
                    const yyyy = today.getFullYear();
                    if ((dd.toString()).length < 2) {
                      date1 = '0' + dd;
                    } else {
                      date1 = dd;
                    }
                    if ((mm.toString()).length < 2) {
                      date2 = '0' + mm;
                    } else {
                      date2 = mm;
                    }
                    let todays;
                    if (this.ddmmyy) {
                      todays = date1 + '/' + date2 + '/' + yyyy;
                    } else {
                      todays = date2 + '/' + date1 + '/' + yyyy;
                    }
                    // console.log(props);
                    // if (props.req === 'TRUE') {
                    this.dynamicform.controls[props.name].patchValue(todays);
                    // }
                  } else {
                    // const date = new Date(props.value[0].value.split('/')[2], props.value[0].value.split('/')[1] - 1, props.value[0].value.split('/')[0]);
                    // let dd = date.getDate();
                    // let mm = date.getMonth() + 1;
                    // const yyyy = date.getFullYear();
                    // if (dd < 10) {
                    //   dd = 0 + dd;
                    // }
                    // if (mm < 10) {
                    //   mm = 0 + mm;
                    // }
                    // const todays = dd + '/' + mm + '/' + yyyy;
                    if (props.value[0].value === 'NaN/NaN/NaN' || props.value[0].value.includes('Select')) {

                    } else {
                      // if (props.req === 'TRUE') {
                      this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                      // }
                    }
                  }
                } else if (props.uitype.uitype === 'DATETIME') {
                  if (props.value.length === 0) {
                    const d = new Date();
                    let newDate;
                    let date1;
                    let date2;
                    if (((d.getDate()).toString()).length < 2) {
                      date1 = '0' + (d.getDate());
                    } else {
                      date1 = (d.getDate());
                    }
                    if (((d.getMonth() + 1).toString()).length < 2) {
                      date2 = '0' + (d.getMonth() + 1);
                    } else {
                      date2 = (d.getMonth() + 1);
                    }
                    if (this.ddmmyy) {
                      newDate = `${date1}/${date2}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
                    } else {
                      newDate = `${date2}/${date1}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
                    }
                    // if (props.req === 'TRUE') {
                    this.dynamicform.controls[props.name].patchValue(newDate);
                    // }
                  } else {
                    if (props.value[0].value === 'NaN/NaN/NaN' || props.value[0].value.includes('Select')) {

                    } else {
                      this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                    }
                  }
                } else if (props.uitype.uitype === 'ROWEXPIRYDATE') {
                  if (props.value.length === 0) {
                  } else {
                    if (props.value[0].value === 'NaN/NaN/NaN' || props.value[0].value.includes('Select')) {

                    } else {
                      // if (props.req === 'TRUE') {
                      this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                      // }
                    }
                  }
                } else if (props.uitype.uitype === 'NUMBER') {
                  if (props.value.length !== 0) {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.value);
                  }
                } else if (props.uitype.uitype === 'TEXTAREA') {
                  if (props.value.length !== 0) {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.value);
                  }
                } else if (props.uitype.uitype === 'DATERANGE') {
                  if (props.value.length === 0) {
                    this.dynamicform.controls[props.name].patchValue(null);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(props.value[0].value);
                  }
                } else if (props.uitype.uitype === 'CHECKBOX') {
                  if (props.value.length === 0) {
                    this.dynamicform.controls[props.name].patchValue(null);
                  } else {
                    this.dynamicform.controls[props.name].patchValue(true);
                  }
                } else {
                } if (props.setVisible) {
                  if (props.value.length > 0) {
                    this.calculateVisibility(props);
                  }
                }
              }
            }
          } else if (element.type === 'TABLE') {
            for (let iTable = 0; iTable < element.rows.length; iTable++) {
              for (let jTable = 0; jTable < element.rows[iTable].items.length; jTable++) {
                for (let kTable = 0; kTable < element.rowheader.length; kTable++) {
                  if (element.rowheader[kTable].name === element.rows[iTable].items[jTable].name) {
                    const formControlName = element.rowheader[jTable].name + element.rows[iTable].row;
                    if (element.rows[iTable].items[jTable].uitype.uitype === 'NUMBER') {
                      if (element.rows[iTable].items[jTable].value.length === 0) {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value);
                      } else {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'DATE') {
                      if (element.rows[iTable].items[jTable].value === undefined || element.rows[iTable].items[jTable].value.length === 0) {
                        element.rows[iTable].items[jTable].value = [];
                        const today = new Date(); // needs to be resolved helplinks https://github.com/kekeh/mydatepicker/blob/master/sampleapp/sample-date-picker-access-modifier/sample-date-picker-access-modifier.html
                        const dd = today.getDate();
                        const mm = (today.getMonth() + 1); // January is 0!
                        let date1;
                        let date2;
                        const yyyy = today.getFullYear();
                        if ((dd.toString()).length < 2) {
                          date1 = '0' + dd;
                        } else {
                          date1 = dd;
                        }
                        if ((mm.toString()).length < 2) {
                          date2 = '0' + mm;
                        } else {
                          date2 = mm;
                        }
                        let todays;
                        if (this.ddmmyy) {
                          todays = date1 + '/' + date2 + '/' + yyyy;
                        } else {
                          todays = date2 + '/' + date1 + '/' + yyyy;
                        }
                        this.dynamicform.controls[formControlName].patchValue(null);
                      } else {
                        // const date = new Date(element.rows[iTable].items[jTable].value[0].value.split('/')[2], element.rows[iTable].items[jTable].value[0].value.split('/')[1] - 1, element.rows[iTable].items[jTable].value[0].value.split('/')[0]);
                        // let dd = date.getDate();
                        // let mm = date.getMonth() + 1;
                        // const yyyy = date.getFullYear();
                        // if (dd < 10) {
                        //   dd = 0 + dd;
                        // }
                        // if (mm < 10) {
                        //   mm = 0 + mm;
                        // }
                        // const todays = dd + '/' + mm + '/' + yyyy;
                        if (element.rows[iTable].items[jTable].value[0].value === 'NaN/NaN/NaN' || element.rows[iTable].items[jTable].value[0].value.includes('Select')) {

                        } else {
                          this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                        }
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'ROWEXPIRYDATE') {
                      if (element.rows[iTable].items[jTable].value === undefined || element.rows[iTable].items[jTable].value.length === 0) {
                        this.dynamicform.controls[formControlName].patchValue(null);
                      } else {
                        if (element.rows[iTable].items[jTable].value[0].value === 'NaN/NaN/NaN' || element.rows[iTable].items[jTable].value[0].value.includes('Select')) {

                        } else {
                          this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                        }
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'TEXT') {
                      if (element.rows[iTable].items[jTable].value.length === 0) {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value);
                      } else {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'LOOKUP') {
                      if (element.rows[iTable].items[jTable].value.length === 0) {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].uitype.lookups[0].value);
                      } else {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'CHECKBOX') {
                      if (element.rows[iTable].items[jTable].value.length === 0) {
                        this.dynamicform.controls[formControlName].patchValue(null);
                      } else {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'AUTONUMBER') {
                      if (element.rows[iTable].items[jTable].value.length === 0 && element.rows[iTable].row !== (9090 + sectionIndex)) {
                        this.dynamicform.controls[formControlName].patchValue(iTable + 1);
                      } else {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'ROWSUM' || element.rows[iTable].items[jTable].uitype.uitype === 'MULTIPLICATION' || element.rows[iTable].items[jTable].uitype.uitype === 'ROWAVG' ||
                      element.rows[iTable].items[jTable].uitype.uitype === 'ROWSUB' || element.rows[iTable].items[jTable].uitype.uitype === 'ROWDIV') {
                      if (element.rows[iTable].items[jTable].value.length === 0) {
                        this.dynamicform.controls[formControlName].patchValue(null);
                      } else {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      }
                      //   let colCalcFlag = 0;
                      //     if (element.rows[iTable].items[jTable].colcalc.type === 'SUM' || element.rows[iTable].items[jTable].colcalc.type === 'AVERAGE') {
                      //       // let colCalcFlag = 0;
                      //      console.log(element.rows[iTable].row);
                      //      console.log(9090 + sectionIndex);
                      //      if ( iTable === sectionIndex) {
                      //       if (element.rows[iTable].row === (9090 + sectionIndex)) {
                      //         colCalcFlag = 0;
                      //         break;
                      //       } else {
                      //         colCalcFlag = 1;
                      //         setTimeout(() => {
                      //           // if (this.footercalled === false) {
                      //           this.addNewTableRow(sectionIndex, 'footer');
                      //           // this.footercalled = true;
                      //           // }
                      //         }, 0);
                      //     }
                      //   }
                      // }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'ROWDATEDIFF') {
                      if (element.rows[iTable].items[jTable].value.length === 0) {
                        this.dynamicform.controls[formControlName].patchValue(0);
                      } else {
                        this.dynamicform.controls[formControlName].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      }
                    } else if (element.rows[iTable].items[jTable].uitype.uitype === 'TEXTAREA') {
                      if (element.rows[iTable].items[jTable].value.length !== 0) {
                        this.dynamicform.controls[element.rows[iTable].items[jTable].name + element.rows[iTable].row].patchValue(element.rows[iTable].items[jTable].value[0].value);
                      } else {
                        this.dynamicform.controls[element.rows[iTable].items[jTable].name + element.rows[iTable].row].patchValue(element.rows[iTable].items[jTable].value);
                      }
                    }
                  }
                }
              }
            }
          }
        });

      }


      if (this.readOnly === 'true') {
        this.dynamicform.disable();
      }
      setTimeout(() => {
        if (this.formObjectSubmit && this.formObjectSubmit.length > 0) {
          for (let sl = 0; sl < this.formObjectSubmit[0].sections.length; sl++) {
            if (this.formObjectSubmit[0].sections[sl].type === 'TABLE') {
              if (this.formObjectSubmit[0].sections[sl].rows[this.formObjectSubmit[0].sections[sl].rows.length - 1].row !== (9090 + sl)) {
                if (this.tableRowValue < this.formObjectSubmit[0].sections[sl].rows[this.formObjectSubmit[0].sections[sl].rows.length - 1].row) {
                  this.tableRowValue = this.formObjectSubmit[0].sections[sl].rows[this.formObjectSubmit[0].sections[sl].rows.length - 1].row;
                }
              } else {
                if (this.tableRowValue < this.formObjectSubmit[0].sections[sl].rows[this.formObjectSubmit[0].sections[sl].rows.length - 2].row) {
                  this.tableRowValue = this.formObjectSubmit[0].sections[sl].rows[this.formObjectSubmit[0].sections[sl].rows.length - 2].row;
                }
              }
              let colCalcFlag = 0;
              let itemColCalcFlag = 0;
              for (let rw = 0; rw < this.formObjectSubmit[0].sections[sl].rows.length; rw++) {
                if (this.formObjectSubmit[0].sections[sl].rows[rw].row === (9090 + sl)) {
                  colCalcFlag = 0;
                  break;
                } else {
                  colCalcFlag = 1;

                }

              }
              if (colCalcFlag === 1) {
                for (let ro = 0; ro < this.formObjectSubmit[0].sections[sl].rows.length; ro++) {
                  for (let it = 0; it < this.formObjectSubmit[0].sections[sl].rows[ro].items.length; it++) {
                    if (this.formObjectSubmit[0].sections[sl].rows[ro].items[it].uitype.colcalc) {
                      if (this.formObjectSubmit[0].sections[sl].rows[ro].items[it].uitype.colcalc.type === 'SUM' || this.formObjectSubmit[0].sections[sl].rows[ro].items[it].uitype.colcalc.type === 'AVERAGE') {
                        itemColCalcFlag = 1;
                        break;
                      } else {
                        itemColCalcFlag = 0;
                      }
                    }
                  }
                }
              }
              if (colCalcFlag === 1 && itemColCalcFlag === 1) {
                this.addNewTableRow(sl, 'footer');
              }
            }
          }
        }
      }, 0);
    }
    // console.log(this.dynamicform);
  }
  addFooterRow(sectionIndex, dummyrow) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].type === 'TABLE' && i === sectionIndex) {
          //  rowheaderdummy.rows = this.formObject[index].sections[i].rowheader;
          const footerheadernew = dummyrow;
          let footerheader: any;
          const rowItem = {
            row: (9090 + i),
            items: []
          };
          // console.log(rowheadernew);
          for (let j = 0; j < footerheadernew.length; j++) {
            footerheader = footerheadernew[j];
            const itemFormName = footerheader.name + rowItem.row;
            let sel: any;
            if (footerheadernew[j].uitype.uitype === 'LOOKUP') {
              footerheader.lookup = footerheadernew[j].uitype.lookups;
              footerheader.lookupOptions = [];
              footerheader.value = [];
              const select = {
                name: footerheader.uitype.lookups[0].name,
                value: footerheader.uitype.lookups[0].value
              };
              sel = select;
              footerheader.value.push(select);
              for (const options of footerheader.uitype.lookups) {
                const lookupValue = {
                  label: options.name,
                  value: options.value
                };
                footerheader.lookupOptions.push(lookupValue);
              }
            }

            const control: FormControl = new FormControl(sel);
            this.dynamicform.addControl(itemFormName, control);
            // if (rowheader.rOnly === 'TRUE') {
            this.dynamicform.controls[itemFormName].disable();
            // } else {

            // }

            footerheader.value = [];
            if (footerheadernew[j].uitype.uitype === 'AUTONUMBER') {
              footerheader.value = {
                value: '' // rowItem.row
              };
              this.dynamicform.controls[itemFormName].patchValue(''); // rowItem.row
            }
            if (j === 0) {
              footerheader.value = [{
                value: 'Total'
              }];
              this.dynamicform.controls[itemFormName].patchValue('Total');
            }
            rowItem.items.push(footerheader);
          }
          // console.log(rowItem);
          this.formObject[index].sections[i].rows.push(rowItem);
          // this.detectChanged.detectChanges();
          // console.log(this.formObject);
          this.formObjectSubmit[index].sections[i].rows.push(rowItem);
          // console.log(this.formObjectSubmit);
          // this.tableRowValue = this.tableRowValue + 1;
        }
      }
    }
  }
  ngDoCheck() {
    // if (this.dynamicform && this.formObject) {
    //   for (const docClass of this.formObject) {
    //     docClass.sections.forEach(element => {
    //             if (element.type === 'TABLE') {
    //                 for (let iTable = 0; iTable < element.rows.length; iTable++) {
    //                     for (let jTable = 0; jTable < element.rows[iTable].items.length; jTable++) {
    //                         for (let kTable = 0; kTable < element.rowheader.length; kTable++) {
    //                             if (element.rowheader[kTable].name === element.rows[iTable].items[jTable].name) {
    //                                 const formControlName = element.rowheader[jTable].name + element.rows[iTable].row;
    //                                 if (element.rows[iTable].items[jTable].uitype.uitype === 'ROWSUM' ||
    // element.rows[iTable].items[jTable].uitype.uitype === 'MULTIPLICATION' ||
    // element.rows[iTable].items[jTable].uitype.uitype === 'ROWAVG' ||
    // element.rows[iTable].items[jTable].uitype.uitype === 'ROWDATEDIFF') {
    //                             for (let index = 0; index < element.rows[iTable].items[jTable].uitype.uitype.calc.length; index++) {
    //                               const str = element.rows[iTable].items[jTable].uitype.calc[index] + element.rows[iTable].row;
    //                               this.dynamicform.controls[str].valueChanges.subscribe(val => {
    //                                 if ( iTable < element.rows.length) { // (element.rows[iTable].row !== undefined || element.rows[iTable].row) &&
    //                                 this.ROWSUMCalculate(element.rows[iTable].row, iTable);
    //                                 }
    //                               });
    //                             }
    //                           }
    //                         }
    //                         }
    //                     }
    //                 }
    //             }
    //           });
    //     }
    // }
  }
  eventCalculated(event, row, rowItem, rowpos, itemind, secionIndex, isDleted) {
    if (isDleted === 'false') {
      for (let it = 0; it < row.items.length; it++) {
        if (row.items[it].uitype.uitype === 'MULTIPLICATION' || row.items[it].uitype.uitype === 'ROWAVG' || row.items[it].uitype.uitype === 'ROWSUM') {
          for (let t = 0; t < row.items[it].uitype.calc.length; t++) {
            if (this.converToUppercase(rowItem.name) === this.converToUppercase(row.items[it].uitype.calc[t]) || rowItem.label === row.items[it].uitype.calc[t]) {
              this.RowMulCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
              break;
            }
          }
        } else if (row.items[it].uitype.uitype === 'ROWSUB' || row.items[it].uitype.uitype === 'ROWDIV') {
          for (let a = 0; a < row.items[it].uitype.calc.length; a++) {
            if (this.converToUppercase(rowItem.name) === this.converToUppercase(row.items[it].uitype.calc[a]) || rowItem.label === row.items[it].uitype.calc[a]) {
              this.rowSubCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
              break;
            }
          }
        } else if (row.items[it].uitype.uitype === 'ROWDATEDIFF') {
          this.rowDateDifferenceCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
        }
      }
    } else { }
    if (rowItem.uitype.colcalc) {
      if (rowItem.uitype.colcalc.type === 'SUM') {
        this.colSumCalculate(secionIndex, itemind, rowpos, row, rowItem);
      } else if (rowItem.uitype.colcalc.type === 'MUL') {
        this.colMulCalculate(secionIndex, itemind, rowpos, row, rowItem);
      } else if (rowItem.uitype.colcalc.type === 'AVERAGE') {
        this.colAvgCalculate(secionIndex, itemind, rowpos, row, rowItem);
      }
    }
    // setTimeout(() => {
    //   this.colCalcFooter(secionIndex, itemind, rowpos, row, rowItem);
    // }, 0);
  }
  eventFormCalculated(event, section, columns, prop, ind, i, j, k) {
    for (let a = 0; a < this.formObject[0].sections.length; a++) {
      if (this.formObject[0].sections[a].type === 'FORM') {
        for (let b = 0; b < this.formObject[0].sections[a].columns.length; b++) {
          for (let d = 0; d < this.formObject[0].sections[a].columns[b].properties.length; d++) {
            if (this.formObject[0].sections[a].columns[b].properties[d].uitype.uitype === 'ROWDATEDIFF') {
              for (let g = 0; g < this.formObject[0].sections[a].columns[b].properties[d].uitype.calc.length; g++) {
                if (this.converToUppercase(this.formObject[0].sections[i].columns[j].properties[k].name) === this.converToUppercase(this.formObject[0].sections[a].columns[b].properties[d].uitype.calc[g])
                  || this.formObject[0].sections[i].columns[j].properties[k].label === this.formObject[0].sections[a].columns[b].properties[d].uitype.calc[g]) {
                  this.calculateFormDateDiff(this.formObject[0].sections[a].columns[b].properties[d], a, b, d);
                }
              }
            }
          }
        }
      }
    }
  }
  eventCalculateForForms(event, section, columns, prop, ind, i, j, k) {
    for (let a = 0; a < this.formObject[0].sections.length; a++) {
      if (this.formObject[0].sections[a].type === 'FORM') {
        for (let b = 0; b < this.formObject[0].sections[a].columns.length; b++) {
          for (let d = 0; d < this.formObject[0].sections[a].columns[b].properties.length; d++) {
            if (this.formObject[0].sections[a].columns[b].properties[d].uitype.uitype === 'MULTIPLICATION' || this.formObject[0].sections[a].columns[b].properties[d].uitype.uitype === 'ROWSUM' || this.formObject[0].sections[a].columns[b].properties[d].uitype.uitype === 'ROWAVG') {
              for (let g = 0; g < this.formObject[0].sections[a].columns[b].properties[d].uitype.calc.length; g++) {
                if (this.converToUppercase(this.formObject[0].sections[i].columns[j].properties[k].name) === this.converToUppercase(this.formObject[0].sections[a].columns[b].properties[d].uitype.calc[g])) {
                  this.calculateFormMultiplications(this.formObject[0].sections[a].columns[b].properties[d], a, b, d);
                }
              }
            } else if (this.formObject[0].sections[a].columns[b].properties[d].uitype.uitype === 'ROWSUB' || this.formObject[0].sections[a].columns[b].properties[d].uitype.uitype === 'ROWDIV') {
              for (let g = 0; g < this.formObject[0].sections[a].columns[b].properties[d].uitype.calc.length; g++) {
                if (this.converToUppercase(this.formObject[0].sections[i].columns[j].properties[k].name) === this.converToUppercase(this.formObject[0].sections[a].columns[b].properties[d].uitype.calc[g])) {
                  this.calculateFormSumations(this.formObject[0].sections[a].columns[b].properties[d], a, b, d);
                }
              }
            }
          }
        }
      }
    }

  }
  calculateFormSumations(subPlace, a, b, d) {
    let value1 = 0;
    let value2 = 0;
    let valueResult = 0;
    for (let g = 0; g < this.formObject[0].sections.length; g++) {
      if (this.formObject[0].sections[g].type === 'FORM') {
        for (let k = 0; k < this.formObject[0].sections[g].columns.length; k++) {
          for (let l = 0; l < this.formObject[0].sections[g].columns[k].properties.length; l++) {
            if (this.converToUppercase(this.formObject[0].sections[g].columns[k].properties[l].name) === this.converToUppercase(subPlace.uitype.calc[0]) || this.formObject[0].sections[g].columns[k].properties[l].label === subPlace.uitype.calc[0]) {
              const formControl = this.formObject[0].sections[g].columns[k].properties[l].name;
              value1 = this.dynamicform.controls[formControl].value;
            } if (this.converToUppercase(this.formObject[0].sections[g].columns[k].properties[l].name) === this.converToUppercase(subPlace.uitype.calc[1]) || this.formObject[0].sections[g].columns[k].properties[l].label === subPlace.uitype.calc[1]) {
              const formControl = this.formObject[0].sections[g].columns[k].properties[l].name;
              value2 = this.dynamicform.controls[formControl].value;
            }
          }
        }
      }
    }
    if (subPlace.uitype.uitype === 'ROWSUB') {
      valueResult = value2 - value1;
    } else if (subPlace.uitype.uitype === 'ROWDIV') {
      if (value1 !== 0) {
        valueResult = value2 / value1;
      } else {
        valueResult = 0;
      }
    }
    this.dynamicform.controls[subPlace.name].patchValue(valueResult);
    if (subPlace.actions && subPlace.actions.length > 0) {
      this.actionAutofillDisabled(valueResult, subPlace);
    }
  }
  calculateFormMultiplications(subPlace, a, b, d) {
    let value = 0;
    let PatchValue;
    for (let o = 0; o < subPlace.uitype.calc.length; o++) {
      for (let g = 0; g < this.formObject[0].sections.length; g++) {
        if (this.formObject[0].sections[g].type === 'FORM') {
          for (let k = 0; k < this.formObject[0].sections[g].columns.length; k++) {
            for (let l = 0; l < this.formObject[0].sections[g].columns[k].properties.length; l++) {
              if (this.converToUppercase(this.formObject[0].sections[g].columns[k].properties[l].name) === this.converToUppercase(subPlace.uitype.calc[o]) || this.formObject[0].sections[g].columns[k].properties[l].label === subPlace.uitype.calc[o]) {
                const formControl = this.formObject[0].sections[g].columns[k].properties[l].name;
                if (subPlace.uitype.uitype === 'MULTIPLICATION') {
                  value = value * (+this.dynamicform.controls[formControl].value);
                } else if (subPlace.uitype.uitype === 'ROWSUM' || subPlace.uitype.uitype === 'ROWAVG') {
                  value = value + (+this.dynamicform.controls[formControl].value);
                }
              }
            }
          }
        }
      }
    }
    if (subPlace.uitype.uitype === 'ROWAVG') {
      PatchValue = value / subPlace.uitype.calc.length;
      this.dynamicform.controls[subPlace.name].patchValue(PatchValue);
    } else {
      this.dynamicform.controls[subPlace.name].patchValue(value);
    }
    if (subPlace.actions && subPlace.actions.length > 0) {
      if (subPlace.uitype.uitype === 'ROWAVG') {
        this.actionAutofillDisabled(PatchValue, subPlace);
      } else {
        this.actionAutofillDisabled(value, subPlace);
      }
    }
  }
  colCalcFooter(secionIndex, itemind, rowpos, row, rowItem) {
    if (rowItem.uitype.colcalc) {
      for (let it = 0; it < row.items.length; it++) {
        if (row.items[it].uitype.uitype === 'ROWSUM' || row.items[it].uitype.uitype === 'MULTIPLICATION' || row.items[it].uitype.uitype === 'ROWAVG') {
          if (row.items[it].uitype.colcalc.type === 'SUM') { // row.items[(row.items.length) - 1].type
            this.colSumCalculate(secionIndex, itemind, rowpos, row, rowItem); // ((row.items.length) - 1)
          }
        } else if (row.items[it].uitype.colcalc.type === 'MULTIPLICATION') {
          this.colMulCalculate(secionIndex, itemind, rowpos, row, rowItem);
        } else if (row.items[it].uitype.colcalc.type === 'AVERAGE') {
          // row.items[it].uitype.colcalc.type === 'AVERAGE' &&
          this.colAvgCalculate(secionIndex, itemind, rowpos, row, rowItem);
        }
      }
      // if ((row.items[(row.items.length) - 1].type === 'ROWSUM' || row.items[(row.items.length) - 1].type === 'MULTIPLICATION' || row.items[(row.items.length) - 1].type === 'ROWAVG') && row.items[(row.items.length) - 1].colcalc) {
      //   if (row.items[(row.items.length) - 1].colcalc.type === 'SUM' ) {
      //   this.colSumCalculate(secionIndex, ((row.items.length) - 1), rowpos, row, rowItem);
      //   }
      //   } else if (row.items[(row.items.length) - 1].colcalc.type === 'MUL') {
      //     this.colMulCalculate(secionIndex, ((row.items.length) - 1), rowpos, row, rowItem);
      //   } else if (row.items[(row.items.length) - 1].colcalc.type === 'AVERAGE') {
      //     this.colAvgCalculate(secionIndex, ((row.items.length) - 1), rowpos, row, rowItem);
      //   }
    }
  }
  rowSubCalculate(row, rowItem, rowpos, itemind, secionIndex, subPlace, type) {
    let value = 0;
    let first = 0;
    let second = 0;
    for (let a = 0; a < this.formObject[0].sections[secionIndex].rows[rowpos].items.length; a++) {
      if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[0]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[0]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        first = (+this.dynamicform.controls[formControl].value);
      } if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[1]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[1]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        second = (+this.dynamicform.controls[formControl].value);
      }
    }
    if (second > first || first > second || first === second) {
      if (type === 'ROWSUB') {
        value = second - first;
      } else if (type === 'ROWDIV' && first !== 0) {
        value = second / first;
      }
    } else {
      value = 0;
    }
    const valueControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
    this.dynamicform.controls[valueControl].patchValue(value);
    for (let it = 0; it < row.items.length; it++) {
      if (row.items[it].uitype.uitype === 'MULTIPLICATION' || row.items[it].uitype.uitype === 'ROWAVG' || row.items[it].uitype.uitype === 'ROWSUM') {
        for (let t = 0; t < row.items[it].uitype.calc.length; t++) {
          if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name) === this.converToUppercase(row.items[it].uitype.calc[t]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].label === row.items[it].uitype.calc[t]) {
            this.RowDisabledMulCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
            break;
          }
        }
      } else if (row.items[it].uitype.uitype === 'ROWSUB' || row.items[it].uitype.uitype === 'ROWDIV') {
        for (let a = 0; a < row.items[it].uitype.calc.length; a++) {
          if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name) === this.converToUppercase(row.items[it].uitype.calc[a]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].label === row.items[it].uitype.calc[a]) {
            this.rowDisabledSubCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
            break;
          }
        }
      }
    }
    if (subPlace.uitype.colcalc) {
      if (subPlace.uitype.colcalc.type === 'SUM') {
        this.colSumCalculate(secionIndex, itemind, rowpos, row, subPlace);
      } else if (subPlace.uitype.colcalc.type === 'AVERAGE') {
        this.colAvgCalculate(secionIndex, itemind, rowpos, row, subPlace);
      }
    }
    if (subPlace.actions && subPlace.actions.length > 0) {
      this.actionAutofillDisabled(value, subPlace);
    }
    //  break;
  }
  RowMulCalculate(row, rowItem, rowpos, itemind, secionIndex, subPlace, type) {
    let value;
    let calValue;
    if (type === 'MULTIPLICATION') {
      value = 1;
    } else {
      value = 0;
    }
    for (let t = 0; t < subPlace.uitype.calc.length; t++) {
      for (let a = 0; a < this.formObject[0].sections[secionIndex].rows[rowpos].items.length; a++) {
        if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[t]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[t]) {
          const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
          if (type === 'ROWSUM' || type === 'ROWAVG') {
            value = value + (+this.dynamicform.controls[formControl].value);
          } else if (type === 'MULTIPLICATION') {
            value = value * (+this.dynamicform.controls[formControl].value);
          } else { }
        }
      }
    }
    const valueControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
    if (type === 'ROWAVG') {
      calValue = (value / (+subPlace.uitype.calc.length));
      this.dynamicform.controls[valueControl].patchValue(calValue);
    } else {
      this.dynamicform.controls[valueControl].patchValue(value);
    }
    for (let it = 0; it < row.items.length; it++) {
      if (row.items[it].uitype.uitype === 'MULTIPLICATION' || row.items[it].uitype.uitype === 'ROWAVG' || row.items[it].uitype.uitype === 'ROWSUM') {
        for (let t = 0; t < row.items[it].uitype.calc.length; t++) {
          // if (row.items[it].uitype.colcalc && row.items[it].uitype.colcalc.copyto) {
          //   const fcname = row.items[it].name + (9090 + secionIndex);
          //   this.patchCopytoValue(this.dynamicform.controls[fcname].value, row.items[it]);
          // }
          if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name) === this.converToUppercase(row.items[it].uitype.calc[t]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].label === row.items[it].uitype.calc[t]) {
            this.RowDisabledMulCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
            break;
          }
        }
      } else if (row.items[it].uitype.uitype === 'ROWSUB' || row.items[it].uitype.uitype === 'ROWDIV') {
        for (let a = 0; a < row.items[it].uitype.calc.length; a++) {
          if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name) === this.converToUppercase(row.items[it].uitype.calc[a]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].label === row.items[it].uitype.calc[a]) {
            this.rowDisabledSubCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
            break;
          }
        }
      }
    }
    if (subPlace.uitype.colcalc) {
      if (subPlace.uitype.colcalc.type === 'SUM') {
        this.colSumCalculate(secionIndex, itemind, rowpos, row, subPlace);
      } else if (subPlace.uitype.colcalc.type === 'AVERAGE') {
        this.colAvgCalculate(secionIndex, itemind, rowpos, row, subPlace);
      }
    }
    if (subPlace.actions && subPlace.actions.length > 0) {
      if (type === 'ROWAVG') {
        this.actionAutofillDisabled(calValue, subPlace);
      } else {
        this.actionAutofillDisabled(value, subPlace);
      }
    }
  }
  rowDateDifferenceCalculate(row, rowItem, rowpos, itemind, secionIndex, subPlace, type) {
    let value = 0;
    let first = 0;
    let second = 0;
    for (let a = 0; a < this.formObject[0].sections[secionIndex].rows[rowpos].items.length; a++) {
      if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[0]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[0]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        if (typeof (this.dynamicform.controls[formControl].value) === 'string') {
          const dateString = this.dynamicform.controls[formControl].value;
          let two;
          if (this.ddmmyy) {
            const convertFormat = dateString.split('/');
            two = new Date([convertFormat[1], convertFormat[0], convertFormat[2]].join('/'));
          } else {
            two = new Date(dateString);
          }
          // value2 = this.dynamicform.controls[formControl].value.setHours(0, 0, 0, 0);
          first = (+two.setHours(0, 0, 0, 0));
        } else {
          first = (+ this.dynamicform.controls[formControl].value);
        }
      } if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[1]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[1]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        if (typeof (this.dynamicform.controls[formControl].value) === 'string') {
          const dateString = this.dynamicform.controls[formControl].value;
          let two;
          if (this.ddmmyy) {
            const convertFormat = dateString.split('/');
            two = new Date([convertFormat[1], convertFormat[0], convertFormat[2]].join('/'));
          } else {
            two = new Date(dateString);
          }
          // value2 = this.dynamicform.controls[formControl].value.setHours(0, 0, 0, 0);
          second = (+two.setHours(0, 0, 0, 0));
        } else {
          second = (+ this.dynamicform.controls[formControl].value);
        }
      }
    }
    // console.log(value);
    if (second > first || first > second || first === second) {
      const diffInMs: number = (second) - (first);
      value = diffInMs / (24 * 1000 * 3600);
    } else {
      value = 0;
    }
    const valueControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
    this.dynamicform.controls[valueControl].patchValue(value);
    for (let it = 0; it < row.items.length; it++) {
      if (row.items[it].uitype.uitype === 'ROWDATEDIFF') {
        for (let a = 0; a < row.items[it].uitype.calc.length; a++) {
          if (row.items[it].uitype.calc[a] === this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name) {
            this.rowDisabledDateDifferenceCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].type);
          }
        }
      }
    }
    if (subPlace.actions && subPlace.actions.length > 0) {
      this.actionAutofillDisabled(value, subPlace);
    }
    if (subPlace.uitype.colcalc) {
      if (subPlace.uitype.colcalc.type === 'SUM') {
        this.colSumCalculate(secionIndex, itemind, rowpos, row, rowItem);
      } else if (subPlace.uitype.colcalc.type === 'AVERAGE') {
        this.colAvgCalculate(secionIndex, itemind, rowpos, row, rowItem);
      }
    }

    // break;
  }
  calculateFormDateDiff(patchDiff, a, b, d) {
    let value1 = 0;
    let value2 = 0;
    let valueDiff = 0;
    for (let g = 0; g < this.formObject[0].sections.length; g++) {
      if (this.formObject[0].sections[g].type === 'FORM') {
        for (let k = 0; k < this.formObject[0].sections[g].columns.length; k++) {
          for (let l = 0; l < this.formObject[0].sections[g].columns[k].properties.length; l++) {
            if (this.converToUppercase(this.formObject[0].sections[g].columns[k].properties[l].name) === this.converToUppercase(patchDiff.uitype.calc[0]) || this.formObject[0].sections[g].columns[k].properties[l].label === patchDiff.uitype.calc[0]) {
              const formControl = this.formObject[0].sections[g].columns[k].properties[l].name;
              if (typeof (this.dynamicform.controls[formControl].value) === 'string') {
                const dateString = this.dynamicform.controls[formControl].value;
                let two;
                if (this.ddmmyy) {
                  const convertFormat = dateString.split('/');
                  two = new Date([convertFormat[1], convertFormat[0], convertFormat[2]].join('/'));
                } else {
                  two = new Date(dateString);
                }
                // value2 = this.dynamicform.controls[formControl].value.setHours(0, 0, 0, 0);
                value1 = (+two.setHours(0, 0, 0, 0));
              } else {
                value1 = (+ this.dynamicform.controls[formControl].value);
              }
              if (this.formObject[0].sections[g].columns[k].properties[l].actions) {
                this.actionAutofillDisabled(value1, this.formObject[0].sections[g].columns[k].properties[l]);
              }
            } if (this.converToUppercase(this.formObject[0].sections[g].columns[k].properties[l].name) === this.converToUppercase(patchDiff.uitype.calc[1]) || this.formObject[0].sections[g].columns[k].properties[l].label === patchDiff.uitype.calc[1]) {
              const formControl = this.formObject[0].sections[g].columns[k].properties[l].name;
              if (typeof (this.dynamicform.controls[formControl].value) === 'string') {
                const dateString = this.dynamicform.controls[formControl].value;
                let two;
                if (this.ddmmyy) {
                  const convertFormat = dateString.split('/');
                  two = new Date([convertFormat[1], convertFormat[0], convertFormat[2]].join('/'));
                } else {
                  two = new Date(dateString);
                }
                // value2 = this.dynamicform.controls[formControl].value.setHours(0, 0, 0, 0);
                value2 = (+two.setHours(0, 0, 0, 0));
              } else {
                value2 = (+ this.dynamicform.controls[formControl].value);
              }
              if (this.formObject[0].sections[g].columns[k].properties[l].actions) {
                this.actionAutofillDisabled(value2, this.formObject[0].sections[g].columns[k].properties[l]);
              }
            }
          }
        }
      }
    }
    // const date1 = new Date();
    // if (isNaN(value1)) {
    //   value1 = date1.setHours(0, 0, 0, 0);
    // } if (isNaN(value2)) {
    //   value2 = date1.setHours(0, 0, 0, 0);
    // }
    const diffInMs: number = (value2) - (value1);
    valueDiff = diffInMs / (24 * 1000 * 3600);
    const valueControl = this.formObject[0].sections[a].columns[b].properties[d].name;
    this.dynamicform.controls[valueControl].patchValue(valueDiff);
    if (this.formObject[0].sections[a].columns[b].properties[d].actions && this.formObject[0].sections[a].columns[b].properties[d].actions.length > 0) {
      this.actionAutofillDisabled(valueDiff, this.formObject[0].sections[a].columns[b].properties[d]);
    }
  }
  RowDisabledMulCalculate(row, rowItem, rowpos, itemind, secionIndex, subPlace, type) {
    let value = 0;
    let calValue;
    for (let t = 0; t < subPlace.uitype.calc.length; t++) {
      for (let a = 0; a < this.formObject[0].sections[secionIndex].rows[rowpos].items.length; a++) {
        if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[t]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[t]) {
          const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
          if (type === 'ROWSUM' || type === 'ROWAVG') {
            value = value + (+this.dynamicform.controls[formControl].value);
            // this.patchCopytoValue(value, subPlace);
          } else if (type === 'MULTIPLICATION') {
            value = value * (+this.dynamicform.controls[formControl].value);
            // this.patchCopytoValue(value, subPlace);
          } else { }
        }
      }
    }
    const valueControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
    if (type === 'ROWAVG') {
      calValue = (value / (+subPlace.uitype.calc.length));
      this.dynamicform.controls[valueControl].patchValue(calValue);
    } else {
      this.dynamicform.controls[valueControl].patchValue(value);
    }
    if (subPlace.uitype.colcalc) {
      if (subPlace.uitype.colcalc.type === 'SUM') {
        this.colSumCalculate(secionIndex, itemind, rowpos, row, subPlace);
      } else if (subPlace.uitype.colcalc.type === 'AVERAGE') {
        this.colAvgCalculate(secionIndex, itemind, rowpos, row, subPlace);
      }
    }
    if (subPlace.actions && subPlace.actions.length > 0) {
      if (type === 'ROWAVG') {
        this.actionAutofillDisabled(calValue, subPlace);
      } else {
        this.actionAutofillDisabled(value, subPlace);
      }
    }
  }
  rowDisabledSubCalculate(row, rowItem, rowpos, itemind, secionIndex, subPlace, type) {
    let value = 0;
    let first = 0;
    let second = 0;
    for (let a = 0; a < this.formObject[0].sections[secionIndex].rows[rowpos].items.length; a++) {
      if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[0]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[0]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        first = (+this.dynamicform.controls[formControl].value);
      } if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[1]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[1]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        second = (+this.dynamicform.controls[formControl].value);
      }
    }
    if (second > first || first > second || first === second) {
      if (type === 'ROWSUB') {
        value = second - first;
      } else if (type === 'ROWDIV' && first !== 0) {
        value = second / first;
      }
    } else {
      value = 0;
    }
    const valueControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
    this.dynamicform.controls[valueControl].patchValue(value);
    if (subPlace.uitype.colcalc) {
      if (subPlace.uitype.colcalc.type === 'SUM') {
        this.colSumCalculate(secionIndex, itemind, rowpos, row, subPlace);
      } else if (subPlace.uitype.colcalc.type === 'AVERAGE') {
        this.colAvgCalculate(secionIndex, itemind, rowpos, row, subPlace);
      }
    }
    if (subPlace.actions && subPlace.actions.length > 0) {
      this.actionAutofillDisabled(value, subPlace);
    }

  }
  rowDisabledDateDifferenceCalculate(row, rowItem, rowpos, itemind, secionIndex, subPlace, type) {
    let value = 0;
    let first = 0;
    let second = 0;
    for (let a = 0; a < this.formObject[0].sections[secionIndex].rows[rowpos].items.length; a++) {
      if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[0]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[0]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        if (typeof (this.dynamicform.controls[formControl].value) === 'string') {
          const dateString = this.dynamicform.controls[formControl].value;
          let two;
          if (this.ddmmyy) {
            const convertFormat = dateString.split('/');
            two = new Date([convertFormat[1], convertFormat[0], convertFormat[2]].join('/'));
          } else {
            two = new Date(dateString);
          }
          // value2 = this.dynamicform.controls[formControl].value.setHours(0, 0, 0, 0);
          first = (+two.setHours(0, 0, 0, 0));
        } else {
          first = (+ this.dynamicform.controls[formControl].value);
        }
      } if (this.converToUppercase(this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name) === this.converToUppercase(subPlace.uitype.calc[1]) || this.formObject[0].sections[secionIndex].rows[rowpos].items[a].label === subPlace.uitype.calc[1]) {
        const formControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[a].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
        if (typeof (this.dynamicform.controls[formControl].value) === 'string') {
          const dateString = this.dynamicform.controls[formControl].value;
          let two;
          if (this.ddmmyy) {
            const convertFormat = dateString.split('/');
            two = new Date([convertFormat[1], convertFormat[0], convertFormat[2]].join('/'));
          } else {
            two = new Date(dateString);
          }
          // value2 = this.dynamicform.controls[formControl].value.setHours(0, 0, 0, 0);
          second = (+two.setHours(0, 0, 0, 0));
        } else {
          second = (+ this.dynamicform.controls[formControl].value);
        }
      }
    }
    // console.log(value);
    if (second > first || first > second || first === second) {
      const diffInMs: number = (second) - (first);
      value = diffInMs / (24 * 1000 * 3600);
    } else {
      value = 0;
    }
    const valueControl = this.formObject[0].sections[secionIndex].rows[rowpos].items[itemind].name + this.formObject[0].sections[secionIndex].rows[rowpos].row;
    this.dynamicform.controls[valueControl].patchValue(value);
    if (subPlace.uitype.colcalc) {
      if (subPlace.uitype.colcalc.type === 'SUM') {
        this.colSumCalculate(secionIndex, itemind, rowpos, row, rowItem);
      } else if (subPlace.uitype.colcalc.type === 'AVERAGE') {
        this.colAvgCalculate(secionIndex, itemind, rowpos, row, rowItem);
      }
    }
    if (subPlace.actions && subPlace.actions.length > 0) {
      this.actionAutofillDisabled(value, subPlace);
    }
  }
  colSumCalculate(si, itmind, rowpos, row, rowItem) {
    let colValue = 0;
    for (let ri = 0; ri < this.formObject[0].sections[si].rows.length; ri++) {
      if (ri < (this.formObject[0].sections[si].rows.length - 1)) {
        const formctrl = this.formObject[0].sections[si].rows[ri].items[itmind].name + this.formObject[0].sections[si].rows[ri].row;
        colValue = colValue + (+this.dynamicform.controls[formctrl].value);
      } else if (ri === (this.formObject[0].sections[si].rows.length - 1) && itmind !== 0) {
        const formcrl = this.formObject[0].sections[si].rows[ri].items[itmind].name + this.formObject[0].sections[si].rows[ri].row;
        this.dynamicform.controls[formcrl].patchValue(colValue);
        this.patchCopytoValue(colValue, rowItem);
      } else { }

    }
    // this.colCalcFooter(si, itmind, rowpos, row, rowItem);
  }
  patchCopytoValue(colValue, rowItem) {
    if (rowItem.uitype.colcalc.copyto) {
      for (let a = 0; a < this.formObject[0].sections.length; a++) {
        if (this.formObject[0].sections[a].type === 'FORM') {
          for (let b = 0; b < this.formObject[0].sections[a].columns.length; b++) {
            for (let c = 0; c < this.formObject[0].sections[a].columns[b].properties.length; c++) {
              if (this.converToUppercase(this.formObject[0].sections[a].columns[b].properties[c].name) === this.converToUppercase(rowItem.uitype.colcalc.copyto) || this.formObject[0].sections[a].columns[b].properties[c].label === rowItem.uitype.colcalc.copyto) {
                const formCName = this.formObject[0].sections[a].columns[b].properties[c].name;
                this.dynamicform.controls[formCName].patchValue(colValue);
                this.eventCalculateForForms('event', 'section', 'columns', 'prop', 'ind', a, b, c);
                break;
              }
            }
          }
        }
      }
    }
  }
  colMulCalculate(si, itmind, rowpos, row, rowItem) {
    let colValue = 1;
    for (let ri = 0; ri < this.formObject[0].sections[si].rows.length; ri++) {
      for (let rt = 0; rt < this.formObject[0].sections[si].rows[ri].items.length; rt++) {
        if (this.formObject[0].sections[si].rows[ri].items[rt].uitype.colcalc && ri < (this.formObject[0].sections[si].rows.length - 1) && rt === itmind) {
          const formctrl = this.formObject[0].sections[si].rows[ri].items[rt].name + this.formObject[0].sections[si].rows[ri].row;
          // if (this.dynamicform.controls[formctrl].value) {
          colValue = colValue * (+this.dynamicform.controls[formctrl].value);
          // }
        } else if (this.formObject[0].sections[si].rows[ri].items[rt].uitype.colcalc && ri === (this.formObject[0].sections[si].rows.length - 1) && rt === itmind) {
          const formcrl = this.formObject[0].sections[si].rows[ri].items[rt].name + this.formObject[0].sections[si].rows[ri].row;
          this.dynamicform.controls[formcrl].patchValue(colValue);
        } else { }
      }
    }
  }
  colAvgCalculate(si, itmind, rowpos, row, rowItem) {
    let colValue = 0;
    for (let ri = 0; ri < this.formObject[0].sections[si].rows.length; ri++) {
      for (let rt = 0; rt < this.formObject[0].sections[si].rows[ri].items.length; rt++) {
        if (this.formObject[0].sections[si].rows[ri].items[rt].uitype.colcalc && ri < (this.formObject[0].sections[si].rows.length - 1) && rt === itmind) {
          const formctrl = this.formObject[0].sections[si].rows[ri].items[rt].name + this.formObject[0].sections[si].rows[ri].row;
          if (this.dynamicform.controls[formctrl].value) {
            colValue = (colValue + (+this.dynamicform.controls[formctrl].value));
          }
        } else if (this.formObject[0].sections[si].rows[ri].items[rt].uitype.colcalc && ri === (this.formObject[0].sections[si].rows.length - 1) && rt === itmind && itmind !== 0) {
          const formcrl = this.formObject[0].sections[si].rows[ri].items[rt].name + this.formObject[0].sections[si].rows[ri].row;
          this.dynamicform.controls[formcrl].patchValue(colValue / (this.formObject[0].sections[si].rows.length - 1));
          const avg = colValue / (this.formObject[0].sections[si].rows.length - 1);
          this.patchCopytoValue(avg, rowItem);
        } else { }
      }
    }
  }
  eventForDisabled() {
    // console.log('summed');
  }
  selectSingle(event, ind, sec, col, prop) {
    this.formObject[ind].sections[sec].columns[col].properties[prop].value = [];
    this.formObject[ind].sections[sec].columns[col].properties[prop].value.push(event);
  }
  selectSingleTable(event, ind, sec, col, prop) {
    this.formObject[0].sections[sec].rows[col].items[prop].value = [];
    this.formObject[0].sections[sec].rows[col].items[prop].value.push(event);
  }

  assignDblookupvalue(res, index, i, j, k) {
    const value = JSON.parse(res._body);
    for (let val = 0; val < value.length; val++) {
      const elementval = {
        label: value[val].label,
        value: value[val].value
      };
      this.formObject[index].sections[i].columns[j].properties[k].lookupOptions.push(elementval);
    }
    if (this.formObject[index].sections[i].columns[j].properties[k].value.length === 0) {
      this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].patchValue(value[0].value);
      this.formObject[index].sections[i].columns[j].properties[k].value.push(this.formObject[index].sections[i].columns[j].properties[k].lookupOptions[0]);
    } else if (this.formObject[index].sections[i].columns[j].properties[k].value.length > 0) {
      this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].patchValue(this.formObject[index].sections[i].columns[j].properties[k].value[0].value);
    }
  }
  assignDblookupvalueTable(res, index, i, tj, k) {
    const value = JSON.parse(res._body);
    for (let val = 0; val < value.length; val++) {
      const elementval = {
        label: value[val].label,
        value: value[val].value
      };
      this.formObject[index].sections[i].rows[tj].items[k].lookupOptions.push(elementval);
    }
    if (this.formObject[index].sections[i].rows[tj].items[k].value.length === 0) {
      this.dynamicform.controls[(this.formObject[index].sections[i].rows[tj].items[k].name + this.formObject[index].sections[i].rows[tj].row)].patchValue(value[0].value);
      this.formObject[index].sections[i].rows[tj].items[k].value.push(this.formObject[index].sections[i].rows[tj].items[k].lookupOptions[0]);
    } else if (this.formObject[index].sections[i].rows[tj].items[k].value.length > 0) {
      this.dynamicform.controls[(this.formObject[index].sections[i].rows[tj].items[k].name + this.formObject[index].sections[i].rows[tj].row)].patchValue(this.formObject[index].sections[i].rows[tj].items[k].value[0].value);
    }
  }
  assignDblookupvalueTableNewRow(res, index, i, tj, k) {
    const value = JSON.parse(res._body);
    for (let val = 0; val < value.length; val++) {
      const elementval = {
        label: value[val].label,
        value: value[val].value
      };
      this.formObject[index].sections[i].rows[tj].items[k].lookupOptions.push(elementval);
    }
    this.dynamicform.controls[(this.formObject[index].sections[i].rows[tj].items[k].name + this.formObject[index].sections[i].rows[tj].row)].patchValue(value[0].value);
    this.formObject[index].sections[i].rows[tj].items[k].value.push(this.formObject[index].sections[i].rows[tj].items[k].lookupOptions[0]);
  }


  converToUppercase(string) {
    if (string) {
      return string.toUpperCase();
    }
  }

  onSearch(evt, dBlookUp, sectionValue, columnvalue, propvalue) {
    dBlookUp.filter = evt.target.value;
    let keyUp: String;
    keyUp = evt.target.value;
    this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbValue = [];
    if (keyUp.length > 2) {
      const newRole = {
        'label': '',
        'itemName': evt.target.value
      };
      this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbValue.push(newRole);
      this.integrationservice.getDBTypeAhead(dBlookUp).subscribe(res => this.assinDBTypeAhead(res, sectionValue, columnvalue, propvalue));
    }
  }

  selctedMultiple(event, ind, sec, col, prop) {
    if (this.formObject[ind].sections[sec].columns[col].properties[prop].value.length > 0) {
      let flag = 0;
      for (const value of this.formObject[ind].sections[sec].columns[col].properties[prop].value) {
        if (value.value === event.value) {
          flag = 1;
          break;
        }
      }
      if (flag === 0) {
        this.formObject[ind].sections[sec].columns[col].properties[prop].value.push(event);
      }
    } else {
      this.formObject[ind].sections[sec].columns[col].properties[prop].value.push(event);
    }
  }


  assinDBTypeAhead(res, sectionValue, columnvalue, propvalue) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
          for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
            if (sectionValue === i && columnvalue === j && propvalue === k) {
              //  this.formObject[index].sections[i].columns[j].properties[k].dbValue = JSON.parse(res._body);
              let value;
              value = JSON.parse(res._body);
              for (let val = 0; val < value.length; val++) {
                const element = {
                  'id': value[val].value,
                  'itemName': value[val].label
                };
                this.formObject[index].sections[i].columns[j].properties[k].dbValue.push(element);

                // this.dropdownList.push(element);
              }
            }
          }
        }
      }
    }
  }


  unselectMultiple(event, ind, sec, col, prop) {
    if (this.formObject[ind].sections[sec].columns[col].properties[prop].value.length > 0) {
      for (
        let a = 0;
        a < this.formObject[ind].sections[sec].columns[col].properties[prop].value.length;
        a++
      ) {
        if (this.converToUppercase(this.formObject[ind].sections[sec].columns[col].properties[prop].value[a].name) === this.converToUppercase(event.name) || this.formObject[ind].sections[sec].columns[col].properties[prop].value[a].label === event.name) {
          this
            .formObject[ind]
            .sections[sec]
            .columns[col]
            .properties[prop]
            .value
            .splice(a, 1);
        }
      }
    }
  }




  focusElement(event) {
    window.setTimeout(function () {
      document.getElementById(event.target.id).focus();
    }, 0);
  }

  dbLookupDropdownCondition(event, ind, i, j, k, prop) {
    let selected;
    this.formObject[ind].sections[i].columns[j].properties[k].value = [];
    for (const select of this.formObject[0].sections[i].columns[j].properties[k].lookupOptions) {
      if (event.value === select.value) {
        selected = {
          name: select.label,
          value: select.value
        };
        this.formObject[ind].sections[i].columns[j].properties[k].value.push(selected);
        break;
      }
    }
    if (prop.uitype.condition !== undefined) {
      for (let index = 0; index < prop.uitype.condition.length; index++) {
        if (prop.uitype.condition[index].val === selected.value && selected.value !== undefined) {
          if (prop.uitype.condition[index].show !== undefined) {
            for (let show = 0; show < prop.uitype.condition[index].show.length; show++) {
              this.showPropsOnScreen(prop.uitype.condition[index].show[show]);
            }
          }
          if (prop.uitype.condition[index].hide !== undefined) {
            for (let hide = 0; hide < prop.uitype.condition[index].hide.length; hide++) {
              this.hidePropsOnScreen(prop.uitype.condition[index].hide[hide]);
            }
          }
        }
      }
    }
  }

  showPropsOnScreen(showProp: any) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].type === 'FORM') {
          for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
            for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
              if (this.formObject[index].sections[i].columns[j].properties[k].label === showProp || this.formObject[index].sections[i].columns[j].properties[k].name === showProp) {
                if (this.formObject[index].sections[i].columns[j].properties[k].req === 'TRUE') {
                  const control: FormControl = new FormControl(null, Validators.required);
                  this.dynamicform.addControl(this.formObject[index].sections[i].columns[j].properties[k].name, control);
                  if (this.formObject[index].sections[i].columns[j].properties[k].value.length === 0) {
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(null);
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([Validators.required]);
                  } else {
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(this.formObject[index].sections[i].columns[j].properties[k].value[0].value);
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([Validators.required]);
                  }
                } else {
                  const control: FormControl = new FormControl(null);
                  this.dynamicform.addControl(this.formObject[index].sections[i].columns[j].properties[k].name, control);
                  if (this.formObject[index].sections[i].columns[j].properties[k].value.length === 0) {
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(null);
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([]);
                  } else {
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(this.formObject[index].sections[i].columns[j].properties[k].value[0].value);
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([]);
                  }
                }
                this.formObject[index].sections[i].columns[j].properties[k].visible = 'TRUE';
              }
            }
          }
        }
      }
    }
    // for (let index = 0; index < this.formObject.length; index++) {
    //   for (let i = 0; i < this.formObject[index].sections.length; i++) {
    //     if (this.formObject[index].sections[i].type === 'FORM') {
    //       for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
    //         for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
    //           if (this.formObject[index].sections[i].columns[j].properties[k].name === showProp) {
    //             if (showProp.req === 'TRUE') {
    //               if (this.formObject[index].sections[i].columns[j].properties[k].value.length === 0) {
    //                 const control: FormControl = new FormControl(null,  Validators.required);
    //                 this.dynamicform.addControl(showProp.name, control);
    //               }else {
    //                 const control: FormControl = new FormControl(this.formObject[index].sections[i].columns[j].properties[k].value[0].value,  Validators.required);
    //                 this.dynamicform.addControl(showProp.name, control);
    //               }
    //           } else {
    //             if (this.formObject[index].sections[i].columns[j].properties[k].value.length === 0) {
    //               const control: FormControl = new FormControl(null);
    //               this.dynamicform.addControl(showProp.name, control);
    //             }else {
    //               const control: FormControl = new FormControl(this.formObject[index].sections[i].columns[j].properties[k].value[0].value);
    //               this.dynamicform.addControl(showProp.name, control);
    //             }
    //           }
    //           }
    //         }
    //       }
    //     }
    //   }
    // }
  }

  hidePropsOnScreen(showProp) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].type === 'FORM') {
          for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
            for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
              if (this.formObject[index].sections[i].columns[j].properties[k].label === showProp || this.converToUppercase(this.formObject[index].sections[i].columns[j].properties[k].name) === this.converToUppercase(showProp)) {
                // this.dynamicform.removeControl(showProp);
                this.formObject[index].sections[i].columns[j].properties[k].visible = 'FALSE';
                this.formObject[index].sections[i].columns[j].properties[k].value = [];
                if (this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name)) {
                  this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).clearValidators();
                  this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                  this.dynamicform.removeControl(showProp);
                  this.dynamicform.addControl(showProp, new FormControl(''));
                  // this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                }
              }
            }
          }
        }
      }
    }
  }

  focusOutFunction(event, prop) {
    if (event.target.value.length > 0) {
      const autoFillComplete = prop.uitype.autofill;
      autoFillComplete.fillparams = [];
      autoFillComplete.fillparams.push(event.target.value);
      // this.integrationservice.getDBAutoFill(autoFillComplete).subscribe(data => this.getDBAutoFill(data, prop), error => this.resetDBAutoFill(prop));
    }
  }
  calculateVisibility(prop) {
    if (this.converToUppercase(prop.setVisible.val) === this.converToUppercase(prop.value[0].value)) {
      if (prop.setVisible.hide.length > 0) {
        const countHide = 0;
        this.propHide(prop.setVisible.hide, countHide);
      }
      if (prop.setVisible.show.length > 0) {
        const countShow = 0;
        this.propShow(prop.setVisible.show, countShow);
      }
      if (prop.setVisible.ronly.length > 0) {
        const countRonly = 0;
        this.propRonly(prop.setVisible.ronly, countRonly);
      }
      if (prop.setVisible.hidesec.length > 0) {
        const countHidesec = 0;
        this.hideSec(prop.setVisible.hidesec, countHidesec);
      }
      if (prop.setVisible.ronlysec.length > 0) {
        const countRonlysec = 0;
        this.ronlySec(prop.setVisible, countRonlysec);
      }
    }
  }
  setVisibleEvent(prop, value) {
    if (this.converToUppercase(value) === this.converToUppercase(prop.val)) {
      if (prop.hide.length > 0) {
        const countHide = 0;
        this.propHide(prop.hide, countHide);
      }
      if (prop.show.length > 0) {
        const countShow = 0;
        this.propShow(prop.show, countShow);
      }
      if (prop.ronly.length > 0) {
        const countRonly = 0;
        this.propRonly(prop.ronly, countRonly);
      }
      if (prop.hidesec.length > 0) {
        const countHidesec = 0;
        this.hideSec(prop.hidesec, countHidesec);
      }
      if (prop.ronlysec.length > 0) {
        const countRonlysec = 0;
        this.ronlySec(prop, countRonlysec);
      }
    }
  }
  ronlySec(ronly, count) {
    if (count < ronly.ronlysec.length) {
      for (let index = 0; index < this.formObject.length; index++) {
        for (let i = 0; i < this.formObject[index].sections.length; i++) {
          if (this.formObject[index].sections[i].type === 'FORM') {
            if (this.formObject[index].sections[i].heading === ronly.ronlysec[count]) {
              for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
                for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                  this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].disable();
                  if (this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'TEXT' || this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'NUMBER') {
                    this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].patchValue(this.formObject[index].sections[i].columns[j].properties[k].value[0].value);
                  }
                }
              }
            }
          }
        }
      }
      count = count + 1;
      this.ronlySec(ronly, count);
    }
  }
  hideSec(hide, count) {
    if (count < hide.length) {
      for (let index = 0; index < this.formObject.length; index++) {
        for (let i = 0; i < this.formObject[index].sections.length; i++) {
          if (this.formObject[index].sections[i].type === 'FORM') {
            if (this.formObject[index].sections[i].heading === hide[count]) {
              this.formObject[index].sections[i].visible = 'FALSE';
              for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
                for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                  // this.dynamicform.removeControl(showProp);
                  this.formObject[index].sections[i].columns[j].properties[k].visible = 'FALSE';
                  this.formObject[index].sections[i].columns[j].properties[k].value = [];
                  if (this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name)) {
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).clearValidators();
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                    this.dynamicform.removeControl(hide[count]);
                    this.dynamicform.addControl(hide[count], new FormControl(''));
                    // this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                  }
                }
              }
            }
          }
        }
      }
      count = count + 1;
      this.hideSec(hide, count);
    }
  }
  propRonly(ronly, count) {
    if (count < ronly.length) {
      for (let index = 0; index < this.formObject.length; index++) {
        for (let i = 0; i < this.formObject[index].sections.length; i++) {
          if (this.formObject[index].sections[i].type === 'FORM') {
            for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
              for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                if (this.formObject[index].sections[i].columns[j].properties[k].label === ronly[count] || this.formObject[index].sections[i].columns[j].properties[k].name === ronly[count]) {
                  const valueP = this.dynamicform.value[this.formObject[index].sections[i].columns[j].properties[k].name];
                  this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].disable();
                  this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].patchValue(valueP);
                  // this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                }
              }
            }
          }
        }
      }

      count = count + 1;
      this.propRonly(ronly, count);
    }
  }
  propShow(show, count) {
    if (count < show.length) {
      for (let index = 0; index < this.formObject.length; index++) {
        for (let i = 0; i < this.formObject[index].sections.length; i++) {
          if (this.formObject[index].sections[i].type === 'FORM') {
            for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
              for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                if (this.formObject[index].sections[i].columns[j].properties[k].label === show[count] || this.formObject[index].sections[i].columns[j].properties[k].name === show[count]) {
                  const valueP = this.dynamicform.value[this.formObject[index].sections[i].columns[j].properties[k].name];
                  console.log(this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype);
                  if (this.formObject[index].sections[i].columns[j].properties[k].req === 'TRUE') {
                    const control: FormControl = new FormControl(null, Validators.required);
                    this.dynamicform.addControl(this.formObject[index].sections[i].columns[j].properties[k].name, control);
                    if (this.formObject[index].sections[i].columns[j].properties[k].value.length === 0) {
                      this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(null);
                      this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([Validators.required]);
                    } else {
                      if (this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'LOOKUP' || this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'DBLOOKUP' || this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'STEXTTA' ||
                        this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'MTEXTTA') {
                        this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(this.formObject[index].sections[i].columns[j].properties[k].value);
                      } else {
                        this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(this.formObject[index].sections[i].columns[j].properties[k].value[0].value);
                      }
                      this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([Validators.required]);
                    }
                  } else {
                    const control: FormControl = new FormControl(null);
                    this.dynamicform.addControl(this.formObject[index].sections[i].columns[j].properties[k].name, control);
                    if (this.formObject[index].sections[i].columns[j].properties[k].value.length === 0) {
                      this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(null);
                      this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([]);
                    } else {
                      if (this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'LOOKUP' || this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'DBLOOKUP' || this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'STEXTTA' ||
                        this.formObject[index].sections[i].columns[j].properties[k].uitype.uitype === 'MTEXTTA') {
                        this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(this.formObject[index].sections[i].columns[j].properties[k].value);
                      } else {
                        this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValue(this.formObject[index].sections[i].columns[j].properties[k].value[0].value);
                      }
                      this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).setValidators([]);
                    }
                  }
                  // this.formObject[index].sections[i].columns[j].properties[k].visible = 'FALSE';
                  this.formObject[index].sections[i].columns[j].properties[k].visible = 'TRUE';
                  // this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                }
              }
            }
          }
        }
      }
      count = count + 1;
      this.propShow(show, count);
    }
  }
  propHide(hide, count) {
    if (count < hide.length) {
      for (let index = 0; index < this.formObject.length; index++) {
        for (let i = 0; i < this.formObject[index].sections.length; i++) {
          if (this.formObject[index].sections[i].type === 'FORM') {
            for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
              for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                if (this.formObject[index].sections[i].columns[j].properties[k].label === hide[count] || this.converToUppercase(this.formObject[index].sections[i].columns[j].properties[k].name) === this.converToUppercase(hide[count])) {
                  // this.dynamicform.removeControl(showProp);
                  this.formObject[index].sections[i].columns[j].properties[k].visible = 'FALSE';
                  this.formObject[index].sections[i].columns[j].properties[k].value = [];
                  if (this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name)) {
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).clearValidators();
                    this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                    this.dynamicform.removeControl(hide[count]);
                    this.dynamicform.addControl(hide[count], new FormControl(''));
                    // this.dynamicform.get(this.formObject[index].sections[i].columns[j].properties[k].name).updateValueAndValidity();
                  }
                }
              }
            }
          }
        }
      }
      count = count + 1;
      this.propHide(hide, count);
    }
  }
  actionAutoFill(event, prop) {
    if (prop.actions && event.target.value.length > 0 && prop.actions.length > 0) {
      const count = 0;
      this.callAutoFillForActions(event.target.value, prop, count);
    }
    if (prop.setVisible && event.target.value.length > 0) {
      // console.log(prop.setVisible);
      prop.value = [];
      prop.value.push({ value: event.target.value });
      this.setVisibleEvent(prop.setVisible, event.target.value);
    }
  }
  actionAutofillDisabled(event, prop) {
    if (prop.actions && event && prop.actions.length > 0) {
      const count = 0;
      this.callAutoFillForActions(event, prop, count);
    }
    if (prop.setVisible && event) {
      // console.log(prop.setVisible);
      // prop.value.push(event);
      this.setVisibleEvent(prop.setVisible, event);

    }
  }
  actionAutoFillLookup(event, prop) {
    if (prop.actions && event.value.length > 0 && prop.actions.length > 0) {
      const count = 0;
      this.callAutoFillForActions(event.value, prop, count);
    }
    if (prop.setVisible && event.value.length > 0) {
      // console.log(prop.setVisible);
      // prop.value.push(event.value);
      this.setVisibleEvent(prop.setVisible, event.value);

    }
  }
  actionAutoFillSTEXTTA(event, prop) {
    if (prop.actions && event.name.length > 0 && prop.actions.length > 0) {
      const count = 0;
      this.callAutoFillForActions(event.name, prop, count);
    }
    if (prop.setVisible && event.name.value.length > 0) {
      // console.log(prop.setVisible);
      // prop.value.push(event.name.value);
      this.setVisibleEvent(prop.setVisible, event.name.value);

    }
  }
  callAutoFillForActions(event, prop, count) {
    if (count < prop.actions.length) {
      if (event) {
        const autoFillComplete = prop.actions[count].autofill;
        autoFillComplete.fillparams = [];
        // autoFillComplete.fillparams.push(event.target.value);
        const value = prop.actions[count].autofill.sql.split('{$');
        const subArray = [];
        for (let i = 0; i < value.length; i++) {
          if (value[i].includes('}')) {
            subArray.push(value[i].split('}')[0]);
          }
        }
        for (let arr = 0; arr < subArray.length; arr++) {
          for (let index = 0; index < this.formObject.length; index++) {
            for (let i = 0; i < this.formObject[index].sections.length; i++) {
              if (this.formObject[index].sections[i].type === 'FORM') {
                for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
                  for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                    if (this.formObject[index].sections[i].columns[j].properties[k].name === subArray[arr]) {
                      const formControlName = this.formObject[index].sections[i].columns[j].properties[k].name;
                      let fillParamObjct;
                      if (this.dynamicform.controls[formControlName].value.length === 0) {
                        fillParamObjct = {
                          key: this.formObject[index].sections[i].columns[j].properties[k].name,
                          value: ''
                        };
                      } else {
                        fillParamObjct = {
                          key: this.formObject[index].sections[i].columns[j].properties[k].name,
                          value: this.dynamicform.controls[formControlName].value
                        };
                      }
                      autoFillComplete.fillparams.push(fillParamObjct);
                    }
                  }
                }
              }
            }
          }
        }
        this.integrationservice.getDBAutoFill(autoFillComplete).subscribe(data => this.getActionDBAutoFill(data, prop, ++count, event), error => { if (prop.type !== 'DATE') { this.resetDBAutoFill(prop.actions[count]); } });
      }
    }
  }
  getActionDBAutoFill(data, prop, count, event) {
    const keyValuePair = JSON.parse(data._body);
    if (keyValuePair.length > 0) {
      for (let keyindex = 0; keyindex < keyValuePair.length; keyindex++) {
        for (let index = 0; index < this.formObject.length; index++) {
          for (let i = 0; i < this.formObject[index].sections.length; i++) {
            if (this.formObject[index].sections[i].type === 'FORM') {
              for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
                for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                  if (this.converToUppercase(this.formObject[index].sections[i].columns[j].properties[k].name) === this.converToUppercase(keyValuePair[keyindex].key)) {
                    this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].patchValue(keyValuePair[keyindex].value);
                    this.formObject[index].sections[i].columns[j].properties[k].value = [];
                    this.formObject[index].sections[i].columns[j].properties[k].value.push({ 'value': keyValuePair[keyindex].value });
                  }
                }
              }
            } else {
              for (let a = 0; a < this.formObject[index].sections[i].rows.length; a++) {
                for (let it = 0; it < this.formObject[index].sections[i].rows[a].items.length; it++) {
                  if (this.converToUppercase(this.formObject[index].sections[i].rows[a].items[it].name) === this.converToUppercase(keyValuePair[keyindex].key)) {
                    this.dynamicform.controls[this.formObject[index].sections[i].rows[a].items[it].name + this.formObject[index].sections[i].rows[a].row].patchValue(keyValuePair[keyindex].value);
                    this.formObject[index].sections[i].rows[a].items[it].value = [];
                    this.formObject[index].sections[i].rows[a].items[it].value.push({ 'value': keyValuePair[keyindex].value });
                  }
                }
              }
            }
          }
        }
      }
      this.callAutoFillForActions(event, prop, count);
    } else {
      this.callAutoFillForActions(event, prop, count);
    }
  }
  getDBAutoFill(data, event, prop, count) {
    const keyValuePair = JSON.parse(data._body);
    if (keyValuePair.length > 0) {
      for (let keyindex = 0; keyindex < keyValuePair.length; keyindex++) {
        for (let index = 0; index < this.formObject.length; index++) {
          for (let i = 0; i < this.formObject[index].sections.length; i++) {
            if (this.formObject[index].sections[i].type === 'FORM') {
              for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
                for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                  if (this.converToUppercase(this.formObject[index].sections[i].columns[j].properties[k].name) === this.converToUppercase(keyValuePair[keyindex].key)) {
                    this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].patchValue(keyValuePair[keyindex].value);
                    this.formObject[index].sections[i].columns[j].properties[k].value = [];
                    this.formObject[index].sections[i].columns[j].properties[k].value.push({ 'value': keyValuePair[keyindex].value });
                  }
                }
              }
            } else {
              for (let a = 0; a < this.formObject[index].sections[i].rows.length; a++) {
                for (let it = 0; it < this.formObject[index].sections[i].rows[a].items.length; it++) {
                  if (this.converToUppercase(this.formObject[index].sections[i].rows[a].items[it].name) === this.converToUppercase(keyValuePair[keyindex].key)) {
                    this.dynamicform.controls[this.formObject[index].sections[i].rows[a].items[it].name + this.formObject[index].sections[i].rows[a].row].patchValue(keyValuePair[keyindex].value);
                    this.formObject[index].sections[i].rows[a].items[it].value = [];
                    this.formObject[index].sections[i].rows[a].items[it].value.push({ 'value': keyValuePair[keyindex].value });
                  }
                }
              }
            }
          }
        }
      }
    } else {
      this.resetDBAutoFill(prop);
    }
  }
  resetDBAutoFill(props) {
    for (let keyindex = 0; keyindex < props.autofill.fillprops.length; keyindex++) {
      for (let index = 0; index < this.formObject.length; index++) {
        for (let i = 0; i < this.formObject[index].sections.length; i++) {
          if (this.formObject[index].sections[i].type === 'FORM') {
            for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
              for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
                if (this.formObject[index].sections[i].columns[j].properties[k].name === props.autofill.fillprops[keyindex]) {
                  this.dynamicform.controls[this.formObject[index].sections[i].columns[j].properties[k].name].patchValue(null);
                  this.formObject[index].sections[i].columns[j].properties[k].value = [];
                }
              }
            }
          }
        }
      }
    }
  }

  dbLookupDropdown(event, ind, i, j, k) {
    let selected;
    this.formObject[ind].sections[i].columns[j].properties[k].value = [];
    for (const select of this.formObject[0].sections[i].columns[j].properties[k].lookupOptions) {
      if (event.value === select.value) {
        selected = {
          name: select.label,
          value: select.value
        };
        this.formObject[ind].sections[i].columns[j].properties[k].value.push(selected);
        break;
      }
    }
    if (this.formObject[0].sections[i].columns[j].properties[k].name === 'category') {
      for (let p = 0; p < this.formObject.length; p++) {
        for (let q = 0; q < this.formObject[p].sections.length; q++) {
          for (let r = 0; r < this.formObject[p].sections[q].columns.length; r++) {
            for (let s = 0; s < this.formObject[p].sections[q].columns[r].properties.length; s++) {
              if (selected.name === 'CEO') {
                if (this.createformtitle === 'Incoming Correspondence' && this.formObject[p].sections[q].columns[r].properties[s].type === 'MTEXTTA') {
                  if (this.formObject[p].sections[q].columns[r].properties[s].name === 'to') {
                    this.formObject[p].sections[q].columns[r].properties[s].value = [];
                    const element = {
                      'value': '100',
                      'name': 'CEO Office'
                    };
                    this.formObject[p].sections[q].columns[r].properties[s].dbDummyValue.push(element);
                    this.formObject[p].sections[q].columns[r].properties[s].dbValue.push(element);
                  }
                }
              }
              if (this.formObject[p].sections[q].columns[r].properties[s].name === 'privacy') {
                if (selected.name === 'CEO' || selected.name === 'QCB') {
                  this.dynamicform.patchValue({ privacy: 3 });
                  this.formObject[p].sections[q].columns[r].properties[s].value = [];
                  this.formObject[p].sections[q].columns[r].properties[s].value.push({
                    name: 'Restricted',
                    value: 3
                  });
                  //   this.formObject[p].sections[q].columns[r].properties[s].value.push(3);
                } else if (selected.name === 'Technical' || selected.name === 'Legal' || selected.name === 'Government') {
                  this.dynamicform.patchValue({ privacy: 2 });
                  this.formObject[p].sections[q].columns[r].properties[s].value = [];
                  this.formObject[p].sections[q].columns[r].properties[s].value.push({
                    name: 'Restricted',
                    value: 2
                  });
                } else {
                  this.dynamicform.patchValue({ privacy: 1 });
                  this.formObject[p].sections[q].columns[r].properties[s].value = [];
                  this.formObject[p].sections[q].columns[r].properties[s].value.push({
                    name: 'Public',
                    value: 1
                  });
                }
              }
              if (this.formObject[p].sections[q].columns[r].properties[s].name === 'priority') {
                if (selected.name === 'CEO' || selected.name === 'QCB' || selected.name === 'Legal') {
                  this.dynamicform.patchValue({ priority: 3 });
                  this.formObject[p].sections[q].columns[r].properties[s].value = [];
                  this.formObject[p].sections[q].columns[r].properties[s].value.push({
                    name: '',
                    value: 3
                  });
                } else {
                  this.dynamicform.patchValue({ priority: 1 });
                  this.formObject[p].sections[q].columns[r].properties[s].value = [];
                  this.formObject[p].sections[q].columns[r].properties[s].value.push({
                    name: '',
                    value: 1
                  });
                }
              }
            }
          }
        }
      }
    }
  }

  onSearchAutoStextTA(event, dBlookUp, sectionValue, columnvalue, propvalue) {
    dBlookUp.filter = event.query;
    let keyUp: String;
    let newRole = {};
    keyUp = event.query;
    if (keyUp.length > 2) {
      this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbValue = [];
      newRole = {
        'value': '',
        'name': event.query
      };
      this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbValue.push(newRole);
      setTimeout(() => {
        this.integrationservice.getDBTypeAhead(dBlookUp).subscribe(res => this.assinDBTypeAheadAuto(res, sectionValue, columnvalue, propvalue));
      }, 100);
      this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].value = [];
      this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].value.push({ 'name': event.query, 'value': '' });
    }
  }
  onSearchAutoStextTATable(event, dBlookUp, sectionValue, columnvalue, propvalue) {
    dBlookUp.filter = event.query;
    let keyUp: String;
    let newRole = {};
    keyUp = event.query;
    if (keyUp.length > 2) {
      this.formObject[0].sections[sectionValue].rows[columnvalue].items[propvalue].dbValue = [];
      newRole = {
        'value': '',
        'name': event.query
      };
      this.formObject[0].sections[sectionValue].rows[columnvalue].items[propvalue].dbValue.push(newRole);
      setTimeout(() => {
        this.integrationservice.getDBTypeAhead(dBlookUp).subscribe(res => this.assinDBTypeAheadAutoTable(res, sectionValue, columnvalue, propvalue));
      }, 100);
      this.formObject[0].sections[sectionValue].rows[columnvalue].items[propvalue].value = [];
      this.formObject[0].sections[sectionValue].rows[columnvalue].items[propvalue].value.push({ 'name': event.query, 'value': '' });
    }
  }
  assinDBTypeAheadAuto(res, sectionValue, columnvalue, propvalue) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].columns) {
          for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
            for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
              if (sectionValue === i && columnvalue === j && propvalue === k) {
                //  this.formObject[index].sections[i].columns[j].properties[k].dbValue = JSON.parse(res._body);
                if ((this.createformtitle === 'Incoming Correspondence' && (this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].name === 'to' || this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].name === 'cc'))
                  || (this.createformtitle === 'Outgoing Correspondence' && this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].name === 'from') || (this.createformtitle === 'Memo')) {
                  this.formObject[0].sections[i].columns[j].properties[k].dbValue = [];
                }
                let value;
                value = JSON.parse(res._body);
                for (let val = 0; val < value.length; val++) {
                  const element = {
                    'value': value[val].value,
                    'name': value[val].label
                  };
                  this.formObject[index].sections[i].columns[j].properties[k].dbValue.push(element);
                  // this.dropdownList.push(element);
                }
              }
            }
          }
        }
      }
    }
  }
  assinDBTypeAheadAutoTable(res, sectionValue, columnvalue, propvalue) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].rows) {
          for (let j = 0; j < this.formObject[index].sections[i].rows.length; j++) {
            for (let k = 0; k < this.formObject[index].sections[i].rows[j].items.length; k++) {
              if (sectionValue === i && columnvalue === j && propvalue === k) {
                //  this.formObject[index].sections[i].columns[j].properties[k].dbValue = JSON.parse(res._body);
                if ((this.createformtitle === 'Incoming Correspondence' && (this.formObject[0].sections[sectionValue].rows[columnvalue].items[propvalue].name === 'to' || this.formObject[0].sections[sectionValue].rows[columnvalue].items[propvalue].name === 'cc'))
                  || (this.createformtitle === 'Outgoing Correspondence' && this.formObject[0].sections[sectionValue].rows[columnvalue].items[propvalue].name === 'from') || (this.createformtitle === 'Memo')) {
                  this.formObject[0].sections[i].rows[j].items[k].dbValue = [];
                }
                let value;
                value = JSON.parse(res._body);
                for (let val = 0; val < value.length; val++) {
                  const element = {
                    'value': value[val].value,
                    'name': value[val].label
                  };
                  this.formObject[index].sections[i].rows[j].items[k].dbValue.push(element);
                  // this.dropdownList.push(element);
                }
              }
            }
          }
        }
      }
    }
  }

  onSearchAuto(event, dBlookUp, sectionValue, columnvalue, propvalue) {
    dBlookUp.filter = event.query;
    let keyUp: String;
    let newRole = {};
    keyUp = event.query;
    if (keyUp.length > 2) {
      this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbValue = [];
      newRole = {
        'value': '',
        'name': event.query
      };
      this.formObject[0].sections[sectionValue].columns[columnvalue].properties[propvalue].dbValue.push(newRole);
      setTimeout(() => {
        this.integrationservice.getDBTypeAhead(dBlookUp).subscribe(res => this.assinDBTypeAheadAuto(res, sectionValue, columnvalue, propvalue));
      }, 100);
    }
  }

  cancelForm() {
    const date = new Date();
    this.formCancel.emit(date.getTime());
  }


  formDynamicSubmit(event) {
    // console.log(this.formObject);

    for (const inputField of [].slice.call(event.target)) {
      for (const docClass of this.formObject) {
        docClass.sections.forEach(element => {
          if (element.type === 'FORM') {
            for (const cols of element.columns) {
              for (const props of cols.properties) {
                if (inputField.id === props.name) {
                  props.value = [{
                    'value': inputField.value
                  }];
                }
                delete props.dbValue;
                delete props.dbDummyValue;
                delete props.lookupOptions;
                for (const value of props.value) {
                  delete value._$visited;
                }
              }
            }
          } else if (element.type === 'TABLE' && element.rows.length > 0) {
            //
            for (let iTable = 0; iTable < element.rows.length; iTable++) {
              // if () {
              for (let jTable = 0; jTable < element.rows[iTable].items.length; jTable++) {
                if (inputField.id === (element.rows[iTable].items[jTable].name + (element.rows[iTable].row))) {
                  element.rows[iTable].items[jTable].value = [{
                    'value': inputField.value
                  }];
                  // console.log(element.rows[iTable].items[jTable].value);
                  //  console.log(this.formObject);
                  // break;
                }

                delete element.rows[iTable].items[jTable].dbValue;
                delete element.rows[iTable].items[jTable].dbDummyValue;
                delete element.rows[iTable].items[jTable].lookupOptions;
                delete element.rows[iTable].items[jTable].rOnly;
                delete element.rows[iTable].items[jTable].req;
                delete element.rows[iTable].items[jTable].label;
                delete element.rows[iTable].items[jTable].type;
                delete element.rows[iTable].items[jTable].length;
                delete element.rows[iTable].items[jTable].lookup;
              }
              //  console.log(iTable);
              //        console.log( element.rows[iTable]);
            }
            // }
          } else {

          }
        });
      }
    }

    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-checkbox'))) {
      for (const docClass of this.formObject) {
        docClass.sections.forEach(element => {
          if (element.type === 'FORM') {
            for (const cols of element.columns) {
              for (const props of cols.properties) {
                if (inputField.id === props.name) {
                  props.value = [{
                    'value': inputField.children['0'].children['0'].children['0'].checked
                  }];
                }
              }
            }
          } else if (element.type === 'TABLE') {
            for (let iTable = 0; iTable < element.rows.length; iTable++) {
              for (let jTable = 0; jTable < element.rows[iTable].items.length; jTable++) {
                if (inputField.id === (element.rows[iTable].items[jTable].name + (element.rows[iTable].row))) { // iTable + 1
                  element.rows[iTable].items[jTable].value = [{
                    'value': inputField.children['0'].children['0'].children['0'].checked
                  }];
                }
              }
            }
          }
        });
      }
    }


    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-calendar'))) {
      for (const docClass of this.formObject) {
        docClass.sections.forEach(element => {
          if (element.type === 'FORM') {
            for (const cols of element.columns) {
              for (const props of cols.properties) {
                if (inputField.id === props.name) {
                  props.value = [{
                    'value': inputField.children['0'].children['0'].value
                  }];
                }
              }
            }
          } else if (element.type === 'TABLE') {
            for (let iTable = 0; iTable < element.rows.length; iTable++) {
              for (let jTable = 0; jTable < element.rows[iTable].items.length; jTable++) {
                if (inputField.id === (element.rows[iTable].items[jTable].name + (element.rows[iTable].row))) {
                  element.rows[iTable].items[jTable].value = [{
                    'value': inputField.children['0'].children['0'].value
                  }];
                }
              }
            }
          }
        });
      }
    }
    for (const inputField of [].slice.call(event.target.getElementsByTagName('p-editor'))) {
      for (const docClass of this.formObject) {
        docClass.sections.forEach(element => {
          if (element.type === 'FORM') {
            for (const cols of element.columns) {
              for (const props of cols.properties) {
                if (inputField.id === props.name) {
                  props.value = [{
                    'value': inputField.children[0].children[1].children[0].innerHTML
                  }];
                }
              }
            }
          } else if (element.type === 'TABLE') {
            for (let iTable = 0; iTable < element.rows.length; iTable++) {
              for (let jTable = 0; jTable < element.rows[iTable].items.length; jTable++) {
                if (inputField.id === (element.rows[iTable].items[jTable].name + (element.rows[iTable].row))) {
                  element.rows[iTable].items[jTable].value = [{
                    'value': inputField.children[0].children[1].children[0].innerHTML
                  }];
                }
              }
            }
          }
        });
      }
    }
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].type === 'FORM') {
          for (let j = 0; j < this.formObject[index].sections[i].columns.length; j++) {
            for (let k = 0; k < this.formObject[index].sections[i].columns[j].properties.length; k++) {
              this.formObjectSubmit[index].sections[i].columns[j].properties[k].value = this.formObject[index].sections[i].columns[j].properties[k].value;
            }
          }
        } else if (this.formObject[index].sections[i].type === 'TABLE') {
          // console.log(this.formObject);
          //  for (let j = 0; j < this.formObject[index].sections[i].rows.length; j++) {
          //    console.log(this.formObject[index].sections[i].rows[j]);
          //    for (let k = 0; k < this.formObject[index].sections[i].rows[j].items.length; k++) {

          //      this.formObjectSubmit[index].sections[i].rows[j].items[k].value = this.formObject[index].sections[i].rows[j].items[k].value;
          //    }
          //    console.log(this.formObjectSubmit[index].sections[i].rows[j]);
          //  }
          this.formObjectSubmit[index].sections[i] = this.formObject[index].sections[i];
        } else {

        }
      }
    }
    for (const docClass of this.formObjectSubmit) {
      docClass.sections.forEach(element => {
        if (element.type === 'FORM') {
          for (const cols of element.columns) {
            for (const props of cols.properties) {
              delete props.dbValue;
              delete props.dbDummyValue;
              delete props.lookupOptions;
              for (const value of props.value) {
                delete value._$visited;
              }
            }
          }
        } else if (element.type === 'TABLE') {
          for (const row of element.rows) {
            for (const item of row.items) {
              delete item.rOnly;
              delete item.req;
              delete item.label;
              delete item.type;
              delete item.length;
              delete item.lookup;
            }
          }
        }
      });
    }
    let act = 0;
    let att = 1;
    for (let i = 0; i < this.activitytype.docTypes.length; i++) {
      if (this.activitytype.docTypes[i].type === 'DOCUMENT' && this.activitytype.docTypes[i].req === 1) {
        if (this.tempAttachments.length > 0) {
          for (let k = 0; k < this.tempAttachments.length; k++) {
            if (this.tempAttachments[k].docType.id === this.activitytype.docTypes[i].id) {
              att = 1;
            } else {
              att = 0;
            }
          }
        } else {
          act = 1;
          break;
        }
      } else {
        act = 0;
      }
    }
    if (act === 1 || att === 0) {
      this.tr.warning('', 'Please attach required attchments');
    } else {
      this.formJSONOutput.emit(this.formObjectSubmit);
      this.tempAttachmentsArray.emit(this.tempAttachments);
    }
  }

  removeExistingTableRow(sectionIndex, row, pos) {
    for (let index = 0; index < row.items.length; index++) {
      this.dynamicform.removeControl(row.items[index].name + row.row);
    }
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].type === 'TABLE' && i === sectionIndex) {
          this.formObject[index].sections[i].rows.splice(pos, 1);
          this.formObjectSubmit[index].sections[i].rows.splice(pos, 1);
        }
        if (this.formObject[index].sections[i].rows && i === sectionIndex) {
          for (let rowsindex = 0; rowsindex < this.formObject[index].sections[i].rows.length; rowsindex++) {
            for (let rid = 0; rid < this.formObject[index].sections[i].rows[rowsindex].items.length; rid++) {
              const itemFormControlName = this.formObject[index].sections[i].rows[rowsindex].items[rid].name + this.formObject[index].sections[i].rows[rowsindex].row;
              if (this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.uitype === 'AUTONUMBER' && this.formObject[index].sections[i].rows[rowsindex].row !== (9090 + sectionIndex)) {
                // console.log( this.dynamicform.value.itemFormControlName);
                this.dynamicform.controls[itemFormControlName].patchValue(rowsindex + 1);
              } else { }
              // console.log();
              if ((this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.uitype === 'ROWSUM' || this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.uitype === 'MULTIPLICATION'
                || this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.uitype === 'ROWAVG') && this.formObject[index].sections[i].rows[rowsindex].items[rid].colcalc) {
                // for (let t = 0; t < this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.calc.length; t++) {
                //   if (rowItem.name === row.items[it].uitype.calc[t]) {
                //     this.RowMulCalculate(row, rowItem, rowpos, it, secionIndex, row.items[it], row.items[it].uitype.uitype);
                //     break;
                //   }
                // }
                if (this.formObject[index].sections[i].rows[rowsindex].items[rid].colcalc.uitype.uitype === 'SUM') {
                  this.colSumCalculate(i, rid, rowsindex, this.formObject[index].sections[i].rows, this.formObject[index].sections[i].rows[rowsindex].items);
                } else if (this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.colcalc.type === 'MUL') {
                  this.colMulCalculate(i, rid, rowsindex, this.formObject[index].sections[i].rows, this.formObject[index].sections[i].rows[rowsindex].items);
                } else if (this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.colcalc.type === 'AVERAGE') {
                  this.colAvgCalculate(i, rid, rowsindex, this.formObject[index].sections[i].rows, this.formObject[index].sections[i].rows[rowsindex].items);
                }
              } else { }
              if (this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.colcalc) {
                if (this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.colcalc.type === 'SUM') {
                  setTimeout(() => {
                    this.colSumCalculate(i, rid, rowsindex, this.formObject[index].sections[i].rows, this.formObject[index].sections[i].rows[rowsindex].items);
                  }, 0);
                } else if (this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.colcalc.type === 'MUL') {
                  setTimeout(() => {
                    this.colMulCalculate(i, rid, rowsindex, this.formObject[index].sections[i].rows, this.formObject[index].sections[i].rows[rowsindex].items);
                  }, 0);
                } else if (this.formObject[index].sections[i].rows[rowsindex].items[rid].uitype.colcalc.type === 'AVERAGE') {
                  setTimeout(() => {
                    this.colAvgCalculate(i, rid, rowsindex, this.formObject[index].sections[i].rows, this.formObject[index].sections[i].rows[rowsindex].items);
                  }, 0);
                }
              }

            }
          }
        }
      }
    }
  }

  addNewTableRow(sectionIndex, isFooter) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].type === 'TABLE' && i === sectionIndex) {
          let flag = 0;
          let rowheader: any;
          let rowheaderdummy = new DummyTable().rows;
          for (let d = 0; d < this.formObject[index].sections[i].rowheader.length; d++) {
            // let properties  = new Props();
            const properties = Object.assign({}, this.formObject[index].sections[i].rowheader[d]);
            // properties.assign({}, this.formObject[index].sections[i].rowheader[d]);
            // properties = this.formObject[index].sections[i].rowheader[d];
            rowheaderdummy.push(properties);
          }
          // rowheaderdummy.rows = this.formObject[index].sections[i].rowheader;
          if (isFooter === 'footer') {
            this.addFooterRow(sectionIndex, rowheaderdummy);
          } else {
            let rowheadernew = rowheaderdummy;
            const rowItem = {
              row: this.tableRowValue + 1,
              items: []
            };
            // console.log(this.formObject[index].sections[i].rowheader);
            // console.log(rowheadernew);
            for (let j = 0; j < rowheadernew.length; j++) {
              rowheader = rowheadernew[j];
              const itemFormName = rowheadernew[j].name + rowItem.row;
              let sel: any;
              if (rowheadernew[j].uitype.uitype === 'LOOKUP') {
                rowheader.lookup = rowheadernew[j].lookups;
                rowheader.lookupOptions = [];
                rowheader.value = [];
                const select = {
                  name: rowheader.uitype.lookups[0].name,
                  value: rowheader.uitype.lookups[0].value
                };
                sel = select;
                rowheader.value.push(select);
                for (const options of rowheader.uitype.lookups) {
                  const lookupValue = {
                    label: options.name,
                    value: options.value
                  };
                  rowheader.lookupOptions.push(lookupValue);
                }
              }

              if (rowheader.req === 'TRUE') {
                const control: FormControl = new FormControl(sel, Validators.required);
                this.dynamicform.addControl(itemFormName, control);
                if (rowheader.rOnly === 'TRUE') {
                  this.dynamicform.controls[itemFormName].disable();
                } else {

                }
              } else {
                const control: FormControl = new FormControl(sel);
                this.dynamicform.addControl(itemFormName, control);
                if (rowheader.rOnly === 'TRUE') {
                  this.dynamicform.controls[itemFormName].disable();
                } else {

                }
              }
              rowheader.value = [];
              if (rowheadernew[j].uitype.uitype === 'AUTONUMBER') {
                rowheader.value = {
                  value: '' // rowItem.row
                };
                this.dynamicform.controls[itemFormName].patchValue(rowItem.row); // rowItem.row
              }
              if (rowheadernew[j].uitype.colcalc) {
                if (rowheadernew[j].uitype.colcalc.type !== 'NONE' && rowheadernew[j].uitype.colcalc.type !== '') {
                  flag = 1;
                }
              } else {
              }
              if (rowheadernew[j].uitype.uitype === 'DBLOOKUP') {
                rowheader.lookup = rowheadernew[j].lookups;
                rowheader.lookupOptions = [];
                rowheader.value = [];
                this.integrationservice.getDBLookup(rowheadernew[j].uitype.dblookup).subscribe(res => {
                  if (flag === 1) {
                    this.assignDblookupvalueTableNewRow(res, index, i, (this.formObject[index].sections[i].rows.length - 1), j);
                  } else {
                    this.assignDblookupvalueTableNewRow(res, index, i, (this.formObject[index].sections[i].rows.length), j);
                  }
                }, error => { });
              }
              rowItem.items.push(rowheader);
            }
            // console.log(rowItem);
            if (flag === 1) {
              this.formObject[index].sections[i].rows.splice((this.formObject[index].sections[i].rows.length - 1), 0, rowItem);
              this.formObjectSubmit[index].sections[i].rows.splice((this.formObject[index].sections[i].rows.length - 1), 0, rowItem);
              // console.log(this.formObject);
            } else {

              this.formObject[index].sections[i].rows.push(rowItem);
              this.formObjectSubmit[index].sections[i].rows.push(rowItem);
            }
          }
        }
      }

      this.tableRowValue = this.tableRowValue + 1;
      // console.log(this.formObject);
      // for (let foindex = 0; foindex  < this.formObject.length; foindex++) {
      for (let secindex = 0; secindex < this.formObject[0].sections.length; secindex++) {
        if (this.formObject[0].sections[secindex].type === 'TABLE') {
          for (let rowsindex = 0; rowsindex < this.formObject[0].sections[secindex].rows.length; rowsindex++) {
            for (let rid = 0; rid < this.formObject[0].sections[secindex].rows[rowsindex].items.length; rid++) {
              const itemFormControlName = this.formObject[0].sections[secindex].rows[rowsindex].items[rid].name + this.formObject[0].sections[secindex].rows[rowsindex].row;
              if (this.formObject[0].sections[secindex].rows[rowsindex].items[rid].uitype.uitype === 'AUTONUMBER' && this.formObject[0].sections[secindex].rows[rowsindex].row !== ((9090) + secindex)) {
                this.dynamicform.controls[itemFormControlName].patchValue(rowsindex + 1);
                this.formObject[0].sections[secindex].rows[rowsindex].items[rid].value = [];
                this.formObject[0].sections[secindex].rows[rowsindex].items[rid].value = [{ value: rowsindex + 1 }];
                // console.log(this.formObject[0].sections[secindex].rows[rowsindex].items[rid].value);
                // console.log(this.formObject);
              }
            }
          }
        }
      }
      // }
    }
  }

  dbLookupDropdownTable(event, pos, ind, title) {
    for (let index = 0; index < this.formObject.length; index++) {
      for (let i = 0; i < this.formObject[index].sections.length; i++) {
        if (this.formObject[index].sections[i].type === 'TABLE' && this.formObject[index].sections[i].heading === title) {
          for (const selected of this.formObject[index].sections[i].rows[pos].items[ind].lookupOptions) {
            if (event.value === selected.value && title === this.formObject[index].sections[i].heading) {
              const select = {
                name: selected.label,
                value: selected.value
              };
              this.formObject[index].sections[i].rows[pos].items[ind].value = [];
              this.formObject[index].sections[i].rows[pos].items[ind].value.push(select);
              break;
            }
          }
        }
      }
    }
  }
  actuallFormat(event, i, j, k, isDD, prop) {
    const d = new Date(Date.parse(event));
    let newDate;
    let date1;
    let date2;
    if (((d.getDate()).toString()).length < 2) {
      date1 = '0' + (d.getDate());
    } else {
      date1 = (d.getDate());
    }
    if (((d.getMonth() + 1).toString()).length < 2) {
      date2 = '0' + (d.getMonth() + 1);
    } else {
      date2 = (d.getMonth() + 1);
    }
    if (isDD === 'ddmmyy') {
      newDate = `${date1}/${date2}/${d.getFullYear()}`;
    } else {
      newDate = `${date2}/${date1}/${d.getFullYear()}`;
    }
    const formControlName = this.formObject[0].sections[i].columns[j].properties[k].name;
    this.dynamicform.controls[formControlName].patchValue(newDate);
    const eventValue = {
      target: {
        value: newDate
      }
    };
    this.actionAutoFill(eventValue, prop);
  }
  dateAndTime(event, i, j, k, isDD, prop) {
    const d = new Date(Date.parse(event));
    let newDate;
    let date1;
    let date2;
    if (((d.getDate()).toString()).length < 2) {
      date1 = '0' + (d.getDate());
    } else {
      date1 = (d.getDate());
    }
    if (((d.getMonth() + 1).toString()).length < 2) {
      date2 = '0' + (d.getMonth() + 1);
    } else {
      date2 = (d.getMonth() + 1);
    }
    if (isDD === 'ddmmyy') {
      newDate = `${date1}/${date2}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
    } else {
      newDate = `${date2}/${date1}/${d.getFullYear()} ${d.getHours()}:${d.getMinutes()}`;
    }
    const formControlName = this.formObject[0].sections[i].columns[j].properties[k].name;
    this.dynamicform.controls[formControlName].patchValue(newDate);
  }

  actuallFormatTable(event, row, rowItem, pos, ind, i, isDD) {
    const d = new Date(Date.parse(event));
    let newDate;
    let date1;
    let date2;
    if (((d.getDate()).toString()).length < 2) {
      date1 = '0' + (d.getDate());
    } else {
      date1 = (d.getDate());
    }
    if (((d.getMonth() + 1).toString()).length < 2) {
      date2 = '0' + (d.getMonth() + 1);
    } else {
      date2 = (d.getMonth() + 1);
    }
    if (isDD === 'ddmmyy') {
      newDate = `${date1}/${date2}/${d.getFullYear()}`;
    } else {
      newDate = `${date2}/${date1}/${d.getFullYear()}`;
    }
    // if (isDD === 'ddmmyy') {
    //   newDate = `${d.getDate()}/${d.getMonth() + 1}/${d.getFullYear()}`;
    // } else {
    //   newDate = `${d.getMonth() + 1}/${d.getDate()}/${d.getFullYear()}`;
    // }
    const formControlName = this.formObject[0].sections[i].rows[pos].items[ind].name + this.formObject[0].sections[i].rows[pos].row;
    this.dynamicform.controls[formControlName].patchValue(newDate);
  }
  calculateExpiryDate(event, prop) {
    let value1;
    let value2;
    for (let i = 0; i < this.formObject[0].sections.length; i++) {
      if (this.formObject[0].sections[i].type === 'FORM') {
        for (let a = 0; a < this.formObject[0].sections[i].columns.length; a++) {
          for (let b = 0; b < this.formObject[0].sections[i].columns[a].properties.length; b++) {
            if (this.formObject[0].sections[i].columns[a].properties[b].uitype.uitype === 'ROWEXPIRYDATE') {
              for (let e = 0; e < this.formObject[0].sections[i].columns[a].properties[b].uitype.calc.length; e++) {
                if (this.formObject[0].sections[i].columns[a].properties[b].uitype.calc[e] === prop.name) {
                  if (this.dynamicform.controls[prop.name].value) {
                    value1 = prop;
                  } else {
                    value1 = undefined;
                  }
                }
              }
              for (let aa = 0; aa < this.formObject[0].sections[i].columns.length; aa++) {
                for (let bb = 0; bb < this.formObject[0].sections[i].columns[aa].properties.length; bb++) {
                  if (this.formObject[0].sections[i].columns[aa].properties[bb].uitype.uitype === 'DATE' || this.formObject[0].sections[i].columns[aa].properties[bb].uitype.uitype === 'NUMBER') {
                    for (let ee = 0; ee < this.formObject[0].sections[i].columns[a].properties[b].uitype.calc.length; ee++) {
                      if (this.formObject[0].sections[i].columns[aa].properties[bb].name === this.formObject[0].sections[i].columns[a].properties[b].uitype.calc[ee] && prop.name !== this.formObject[0].sections[i].columns[a].properties[b].uitype.calc[ee]) {
                        if (this.dynamicform.controls[this.formObject[0].sections[i].columns[aa].properties[bb].name].value) {
                          value2 = this.formObject[0].sections[i].columns[aa].properties[bb];
                        } else {
                          value2 = undefined;
                        }
                      }
                    }
                  }
                }
              }
              if (value1 && value2) {
                if (value1.type === 'DATE') {
                  const thisvalue = this.dynamicform.controls[value1.name].value;
                  const dateFormat = thisvalue.split('/')[1] + '/' + thisvalue.split('/')[0] + '/' + thisvalue.split('/')[2];
                  const date = new Date(dateFormat);
                  date.setDate(date.getDate() + (+this.dynamicform.controls[value2.name].value));
                  let date1;
                  let date2;
                  if ((date.getDate()).toString().length < 2) {
                    date1 = '0' + (date.getDate());
                  }
                  if ((date.getMonth() + 1).toString().length < 2) {
                    date2 = '0' + (date.getMonth() + 1);
                  }
                  const dateFormated = date1 + '/' + date2 + '/' + date.getFullYear();
                  const formControlName = this.formObject[0].sections[i].columns[a].properties[b].name;
                  this.dynamicform.controls[formControlName].patchValue(dateFormated);
                } else {
                  const thisvalue = this.dynamicform.controls[value2.name].value;
                  const dateFormat = thisvalue.split('/')[1] + '/' + thisvalue.split('/')[0] + '/' + thisvalue.split('/')[2];
                  const date = new Date(dateFormat);
                  date.setDate(date.getDate() + (+this.dynamicform.controls[value1.name].value));
                  let date1;
                  let date2;
                  if ((date.getDate()).toString().length < 2) {
                    date1 = '0' + (date.getDate());
                  }
                  if ((date.getMonth() + 1).toString().length < 2) {
                    date2 = '0' + (date.getMonth() + 1);
                  }
                  const dateFormated = date1 + '/' + date2 + '/' + date.getFullYear();
                  const formControlName = this.formObject[0].sections[i].columns[a].properties[b].name;
                  this.dynamicform.controls[formControlName].patchValue(dateFormated);
                }
              } else {
                const formControlName = this.formObject[0].sections[i].columns[a].properties[b].name;
                this.dynamicform.controls[formControlName].patchValue(null);
              }
            }
          }
        }
      }
    }
  }

  tempObject(obj, name) {
    this.tempFileobject = obj;
    for (let index = 0; index < this.activitytype.docTypes.length; index++) {
      if (this.activitytype.docTypes[index].type === 'DOCUMENT' && this.activitytype.docTypes[index].name === name) {
        this.selectedObject = this.duplicateActivity.docTypes[index].id;
      }
    }
  }
  getRecent() {
    const date = new Date();
    // this.systemDate = date;
    this.cs.getRecent(1).subscribe(data => this.getRecentDocuments(data));
  }

  getRecentDocuments(data) {
    if (data._body !== '') {
      this.recentDocumentList = JSON.parse(data._body);
      this.attachfilestabshow = true;
      this.selectedTab = 2;
      this.changeDetectRef.detectChanges();
      this.documentsSelected = [];
      this.ngxSmartModalService.getModal('documentAddRepository').open();
    }
  }

  scanImage(id, docClass) {
    const socket = new WebSocket('ws://localhost:8181');
    socket.onopen = () => socket.send('1100');
    socket.onmessage = (e) => this.onMessage(e, docClass);
    socket.onerror = (e) => this.onWebsocketError(e);
  }
  onWebsocketError(e) {
    this.websocketNotOpen = true;
    const socket = new WebSocket('ws://localhost:8181');
    socket.close();
    this.ngxSmartModalService.getModal('hashTwainModel').open();
  }
  onMessage(e, docClass) {
    this.spinner.show();
    this.websocketNotOpen = false;
    const storedFiles = [];
    let count = 0;
    if (typeof e.data === 'string') {
      // IF Received Data is String
    } else if (e.data instanceof ArrayBuffer) {
      // IF Received Data is ArrayBuffer
    } else if (e.data instanceof Blob && count <= 1) {
      const f = e.data;
      count++;
      const reader = new FileReader();
      reader.onload = (f: FileReaderEvent) => {
        const pdfdata = f.target.result;
        const byteCharacters = atob(pdfdata.replace('data:application/octet-stream;base64,', ''));
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], { type: 'application/pdf' });
        // saveAs(blob, this.title);
        let string: String;
        string = this.title;
        let date;
        date = new Date();
        const docInfo = {
          docclass: docClass,
          props: [{
            'name': 'Document Title',
            'symName': 'DocumentTitle',
            'dtype': 'STRING',
            'mvalues': ['ScannedImage'.concat(date.getTime().toString()).concat('.pdf')],
            'mtype': 'N',
            'len': 255,
            'rOnly': 'false',
            'hidden': 'false',
            'req': 'false'
          }],
          accessPolicies: []
        };
        let formData;
        formData = new FormData();
        formData.append('DocInfo', JSON.stringify(docInfo));
        formData.append('document', blob, 'ScannedImage'.concat(date.getTime().toString()).concat('.pdf'));
        this.ds.addDocument(formData).subscribe(data => { this.scannedDocument(data, 'ScannedImage'.concat(date.getTime().toString()).concat('.pdf')); this.spinner.hide(); }, error => this.spinner.hide());
      };
      reader.readAsDataURL(f);
    }
  }
  scannedDocument(data, name) {
    this.uploadFile(name);
    const str = 'Scanned-';
    const attachments = {
      docId: data._body,
      docTitle: name,
      format: 'application/pdf',
      docType: {
        id: this.selectedObject
      }
    };
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Document added', detail: '' });
    this.tr.success('', 'Document added');
    this.tempAttachments.push(attachments);
  }
  documentSearchList(event) {
    this.documentsSelected = event;
    for (let i = 0; i < this.documentsSelected.length; i++) {
      //   if (this.documentsSelected[i].format !== 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
      //     && this.documentsSelected[i].format !== 'application/pdf' && this.documentsSelected[i].format !== 'application/msword' && this.documentsSelected[i].format !== 'image/jpeg' && this.documentsSelected[i].format !== 'image/png') {
      // this.tr.error('', this.documentsSelected[i].name  + ': Not able to attach please select only pdf, doc and image files');
      //  this.documentsSelected.splice(i, 1);
      //   }else {
      //   }
    }
  }
  attachRepDoc() {
    for (let index = 0; index < this.documentsSelected.length; index++) {
      const attachments = {
        docId: this.documentsSelected[index].docId,
        docTitle: this.documentsSelected[index].name,
        format: this.documentsSelected[index].format,
        docType: {
          id: this.selectedObject
        },
        tempid: 1
      };
      this.tr.success('', 'Document added');
      this.tempAttachments.push(attachments);
    }
    for (let index = 0; index < this.documentsSelected.length; index++) {
      this.uploadFile(this.documentsSelected[index].name);
    }
    this.documentsSelected = [];
  }

  uploadFile(desiredFilename: string) {
    let date: number;
    date = new Date().getTime();
    //  let contents: string[];
    // contents must be an array of strings, each representing a line in the new file
    let file;
    file = new File([''], desiredFilename, { type: 'text/plain', lastModified: date });
    let fileItem;
    fileItem = new FileItem(this.tempFileobject, file, {});
    // (Visual Only) adds the new fileItem to the upload queue
    this.tempFileobject.queue.push(fileItem);
  }

  inputfileChanged(id, docClass) {
    for (let i = 0; i < this.globelFileUploder.queue.length; i++) {
      // if (this.globelFileUploder.queue[i]._file.type === 'application/octet-stream'
      // || this.globelFileUploder.queue[i]._file.type === 'application/java-archive'
      // || this.globelFileUploder.queue[i]._file.type === 'application/x-zip-compressed'
      // || this.globelFileUploder.queue[i]._file.type === 'application/x-msdownload' ) {
      //   this
      //   .tr
      //   .error(
      //       '',
      //       this.globelFileUploder.queue[i]._file.name + ': Not able to attach please select only pdf, docs, xls and image files'
      //   );
      //    this.globelFileUploder.removeFromQueue(this.globelFileUploder.queue[i]);
      //   i--;
      // }else {
      //   this.addDocument(id, docClass);
      // }
      this.addDocument(id, docClass);
    }
  }

  addDocument(fileUploader, docClass) {
    this.spinner.show();

    for (let i = 0; i < this.globelFileUploder.queue.length; i++) {
      const docInfo = {
        docclass: docClass,
        props: [{
          'name': 'Document Title',
          'symName': 'DocumentTitle',
          'dtype': 'STRING',
          'mvalues': [this.globelFileUploder.queue[i]._file.name],
          'mtype': 'N',
          'len': 255,
          'rOnly': 'false',
          'hidden': 'false',
          'req': 'false'
        }],
        accessPolicies: []
      };
      this.formData = new FormData();
      this.formData.append('DocInfo', JSON.stringify(docInfo));
      this.formData.append('file' + i, this.globelFileUploder.queue[i]._file);
      this.ds.addDocument(this.formData).subscribe(data => this.addDocTemp(data, this.globelFileUploder.queue[i]._file.name, this.globelFileUploder.queue[i]._file.type), error => { this.spinner.hide(); });
    }
  }
  addDocTemp(data, name, format) {
    let adddocument;
    adddocument = [];
    adddocument.push({
      docid: data,
      docTitle: name,
      format: format
    });
    this.spinner.hide();
    this.saveAttachemtObjects(adddocument);
  }

  saveAttachemtObjects(data) {
    const attachments = {
      docId: data[0].docid._body,
      docTitle: data[0].docTitle,
      format: data[0].format,
      docType: {
        id: this.selectedObject
      }
    };
    this.msgs = [];
    this.msgs.push({ severity: 'success', summary: 'Document added', detail: '' });
    this.tr.success('', 'Document Added');
    this.tempAttachments.push(attachments);
    console.log(this.tempAttachments);
    const date: number = new Date().getTime();
    // contents must be an array of strings, each representing a line in the new file
    // const file = new File([''], data[0].docTitle, {type: data[0].format, lastModified: date});
    // const fileItem = new FileItem(this.tempFileobject, file, {});
    // // (Visual Only) adds the new fileItem to the upload queue
    // this.tempFileobject.queue.push(fileItem);
    this.globelFileUploder.clearQueue();

  }

  parseInt(str) {
    let x = str;
    let y = +x; // y: number
    return y;
  }
  removeAttachments(fileobj) {
    for (let index = 0; index < this.tempAttachments.length; index++) {
      if (fileobj.docTitle === this.tempAttachments[index].docTitle) {
        this.tempAttachments.splice(index, 1);
      }
    }
    this.tr.warning('', 'Document removed');
  }

  showClassProp(item) {
    this.selectedClassItem = item;
    this.cs.getDocumentClasses().subscribe(data => this.documentClasses(data, item));
  }

  showClassPropScan(item) {
    this.selectedClassItem = item;
    this.cs.getDocumentClasses().subscribe(data => this.documentClassesScan(data, item));
  }

  documentClassesScan(data, item) {
    if (data._body !== '') {
      this.documentClass = JSON.parse(data._body);
      for (let index = 0; index < this.documentClass.length; index++) {
        if (this.documentClass[index].symName === item.docClass) {
          this.docProperty = this.documentClass[index];
          this.documentClassProp = this.documentClass[index].props;
          this.docClassSymName = this.documentClass[index].value;
          this.docClassName = this.documentClass[index].name;
        }
      }
      for (let index = 0; index < this.documentClassProp.length; index++) {
        if (this.documentClassProp[index].req === 'true' && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null, Validators.required);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } else {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } if (this.documentClassProp[index].lookup !== undefined && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].lookups[0].value, control);
        }
      }
    }
    this.attachmentfileName = null;
    this.addDoc.nativeElement.value = null;
    this.documentAdd.reset();
    this.ngxSmartModalService.getModal('documentAddScanWithProps').open();
  }

  documentClasses(data, item) {
    if (data._body !== '') {
      this.documentClass = JSON.parse(data._body);
      for (let index = 0; index < this.documentClass.length; index++) {
        if (this.documentClass[index].symName === item.docClass) {
          this.docProperty = this.documentClass[index];
          this.documentClassProp = this.documentClass[index].props;
          this.docClassSymName = this.documentClass[index].value;
          this.docClassName = this.documentClass[index].name;
        }
      }
      for (let index = 0; index < this.documentClassProp.length; index++) {
        if (this.documentClassProp[index].req === 'true' && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null, Validators.required);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } else {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].symName, control);
        } if (this.documentClassProp[index].lookup !== undefined && this.documentClassProp[index].symName !== 'documenttitle') {
          const control: FormControl = new FormControl(null);
          this.documentAdd.addControl(this.documentClassProp[index].lookups[0].value, control);
        }
      }
    }
    this.attachmentfileName = null;
    this.addDoc.nativeElement.value = null;
    this.documentAdd.reset();
    this.ngxSmartModalService.getModal('documentAddWithProps').open();
  }
}


interface FileReaderEventTarget extends EventTarget {
  result: string;
}

interface FileReaderEvent extends Event {
  target: FileReaderEventTarget;
  getMessage(): string;
}

interface FileUploaderCustom extends FileItem {
  filec: FileLikeObjectCustom;
}

interface FileLikeObjectCustom extends FileLikeObject {
  name: string;
  size: number;
  type: string;
}
